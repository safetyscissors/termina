#import <UIKit/UIKit.h>

@interface TMDISCSCharacterViewController : UIViewController

/** User Character Name */
@property (nonatomic, strong) IBOutlet UILabel *userCharacterName;

/** Current Total Locations Controlled Label */
@property (nonatomic, strong) IBOutlet UILabel *locationsControlledLabel;

/** Current Total Locations Controlled Label */
@property (nonatomic, strong) IBOutlet UILabel *averageRentLabel;

/** Current Gem Balance Label */
@property (nonatomic, strong) IBOutlet UILabel *currentGemBalance;

/** Current Maxium Moves Per Fight */
@property (nonatomic, strong) IBOutlet UILabel *maxMovesPerFight;

/** Fight Record Label */
@property (nonatomic, strong) IBOutlet UILabel *fightRecordLabel;

/** Days Played Label */
@property (nonatomic, strong) IBOutlet UILabel *daysPlayedLabel;

/** Refill Energy Button */
@property (nonatomic, strong) IBOutlet UIButton *refillEnergyButton;

@end
