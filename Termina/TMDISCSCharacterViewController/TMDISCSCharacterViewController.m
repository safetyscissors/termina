//
//  TMDISCSCharacterViewController.m
//  Termina
//
//  Created by Jason Lagaac on 27/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMDISCSCharacterViewController.h"
#import "TMCharacterLevelView.h"
#import "TMUserCharacter.h"
#import "TMFightHelpers.h"
#import "TMUser.h"


@interface TMDISCSCharacterViewController ()

/** Character Level View */
@property (nonatomic, strong) TMCharacterLevelView *characterLevelView;
@end

@implementation TMDISCSCharacterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self loadUserCharacterLevelIndicator];
    [self loadUserCharacterDetails];
    
}

- (void)loadUserCharacterLevelIndicator
{
    // Load
    self.characterLevelView = [[TMCharacterLevelView alloc] init];
    [self.characterLevelView.characterImage setImage:[UIImage imageNamed:@"MainCharacterIconSample.png"]];
    [self.view addSubview:self.characterLevelView];
    
    NSString *level = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"level"];
    NSString *levelText = [NSString stringWithFormat:@"Level %@", level];
    self.characterLevelView.levelLabel.text = levelText;
    
    if (IS_IPHONE_5) {
        self.characterLevelView.center = CGPointMake(93.0f, 86.0f);
    } else {
        self.characterLevelView.center = CGPointMake(93.0f, 74.0f);
    }
}

- (void)loadUserCharacterDetails
{
    // Current user character gems
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];

    NSNumber *currentGems = [[TMUser sharedInstance].currentUserDetails objectForKey:@"money"];
    self.currentGemBalance.text = [numberFormatter stringFromNumber:currentGems];
    
    // Number of character moves
    NSInteger maxMoves = [[[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"attributes"] objectForKey:@"attack_points"] integerValue];
    self.maxMovesPerFight.text = [NSString stringWithFormat:@"%d moves per fight", maxMoves];
    
    // Calculate number of days played
    NSString *dateString = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"created"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    DebugLog(@"Create Date: %@", dateString);

    NSDate *createDate = [dateFormat dateFromString:dateString];
    self.daysPlayedLabel.text = [NSString stringWithFormat:@"%d", daysBetween(createDate, [NSDate date])];
    
    // Calculate Fight Record
    self.fightRecordLabel.text = @"0 - 0";
    
    // Controlled Locations
    self.locationsControlledLabel.text = @"0 Locations";
    
    // Average Rent
    self.averageRentLabel.text = @"0 per day";
    
    // Character Name
    self.userCharacterName.text = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"name"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
