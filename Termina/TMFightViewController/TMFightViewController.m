#import "TMFightViewController.h"
#import "TMViewHandlerController.h"
#import "TMProgressIndicator.h"
#import "TMUserToFighterConvert.h"
#import "TMUser.h"
#import "TMFight.h"
#import "TMFightHelpers.h"
#import "TMLocation.h"
#import "TMFightResultViewController.h"
#import "TMFightResultItemView.h"

#import <SVProgressHUD/SVProgressHUD.h>

// Minimum touch area
float const kFightTouchAreaYMin  = 65.0f;

// Maximum 3-inch device touch area
float const k3inchFightTouchAreaYMax  = 325.0f;

// Maximum 4-inch device touch area
float const k4inchFightTouchAreaYMax  = 415.0f;

@interface TMFightViewController ()

/** Opponent health bar */
@property (nonatomic) TMProgressIndicator *opponentHealthBar;

/** Current character health bar */
@property (nonatomic) TMProgressIndicator *playerHealthBar;

/** Allow fight input */
@property (nonatomic) BOOL allowFightInput;

/** Entering fight sequence */
@property (nonatomic) BOOL enteringFightSequence;

/** Current fight state */
@property (nonatomic) FightMode currentFightMode;

/** Current Fight Sequences */
@property (nonatomic, strong) NSMutableArray *fightSequences;

/** Count down to fight sequence input */
@property (nonatomic, strong) NSTimer *countDownTimer;

/** Sequences Update Timer */
@property (nonatomic, strong) NSTimer *sequencesUpdateTimer;

/** Count down value */
@property (nonatomic) NSInteger countDownVal;

/** Player Attack Touches */
@property (nonatomic, strong) NSMutableArray *playerAttackTouches;

/** Player Defense Touches */
@property (nonatomic, strong) NSMutableArray *playerDefenseTouches;

/** Player Defense Touch Sprites */
@property (nonatomic, strong) NSMutableArray *playerDefenseTouchSprites;

/** Opponent Attack Touches */
@property (nonatomic, strong) NSMutableArray *opponentAttackTouches;

/** Opponent Defense Touch Sprites */
@property (nonatomic, strong) NSMutableArray *opponentAttackTouchSprites;

/** Opponent Defense Touches */
@property (nonatomic, strong) NSMutableArray *opponentDefenseTouches;

/** Opponent Attack Count */
@property (nonatomic) NSInteger opponentAttackCount;

/** Opponent Attack Replay Timer */
@property (nonatomic, strong) NSTimer *opponentAttackReplayTimer;

/** Fight Result View Controller */
@property (nonatomic, strong) TMFightResultViewController *fightResultViewController;

@end

@implementation TMFightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.opponentAttackCount = 0;
    self.allowFightInput = NO;
    self.enteringFightSequence = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightSequencesRetrieved:)
                                                 name:kFightSequencesRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightSequencesRetrieveFailed:)
                                                 name:kFightSequencesRetrieveFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightSequenceCreated:)
                                                 name:kFightSequenceCreated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightSequenceCreateFailed:)
                                                 name:kFightSequenceCreateFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightStopped:)
                                                 name:kFightStopped
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightStopFailed:)
                                                 name:kFightStopFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationUpdated:)
                                                 name:kLocationUpdated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationUpdateFailed:)
                                                 name:kLocationUpdateFailed
                                               object:nil];
    
    // Load Fight Won/Lost View Controllers
    [self loadFightResultViewController];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadLocationData];
    [self loadPlayerData];
    [self loadOpponentData];
    [self loadHealthBars];
    [self loadFightSequences];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self resetFightData];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Unload fight won/lost controllers
    [self.fightResultViewController.view removeFromSuperview];
    self.fightResultViewController = nil;
    
    [super viewDidDisappear:animated];
}

- (void)resetFightData
{
    self.playerData = nil;
    self.playerCheckinData = nil;
    
    self.opponentData = nil;
    self.opponentCheckinData = nil;

    self.fightSequences = nil;
    self.fightData = nil;
    
    self.locationData = nil;
    self.indicator.hidden = YES;
    
    self.allowFightInput = NO;
    self.readyButton.alpha = 0.0f;
    self.readyButton.hidden = YES;
    
    [self.playerHealthBar removeFromSuperview];
    self.playerHealthBar = nil;
    
    [self.opponentHealthBar removeFromSuperview];
    self.opponentHealthBar = nil;
    
    [self cleanupAttackDefenseTouchesAndSprites];
}


- (void)cleanupAttackDefenseTouchesAndSprites
{
    for (UIImageView *sprite in self.playerAttackTouches) {
        [sprite removeFromSuperview];
    }
    [self.playerAttackTouches removeAllObjects];
    
    for (UIImageView *sprite in self.playerDefenseTouchSprites) {
        [sprite removeFromSuperview];
    }
    
    [self.playerDefenseTouchSprites removeAllObjects];
    
    for (UIImageView *sprite in self.opponentAttackTouchSprites) {
        [sprite removeFromSuperview];
    }
    
    [self.opponentAttackTouchSprites removeAllObjects];
    
    [self.playerAttackTouches removeAllObjects];
    self.playerAttackTouches = nil;
    
    [self.playerDefenseTouches removeAllObjects];
    self.playerDefenseTouches = nil;
    
    [self.opponentAttackTouches removeAllObjects];
    self.opponentAttackTouches = nil;
}

#pragma mark - Fight Won/Lost Controller Actions
////////////////////////////////////////////////////////////////////////////////

- (void)loadFightResultViewController
{
    NSString *nibName = IS_IPHONE_5 ? @"FightResultViewController-568h" : @"FightResultViewController";
    self.fightResultViewController = [[TMFightResultViewController alloc] initWithNibName:nibName
                                                                             bundle:nil];
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    self.fightResultViewController.view.center = CGPointMake(screenFrame.size.width / 2, screenFrame.size.height/ 2);
    
}

- (void)presentFightWon
{
    NSMutableArray *resultViews = [[NSMutableArray alloc] init];

    if ([[self.fightData objectForKey:@"boss_fight"] boolValue]) {
        TMFightResultItemView *bossResult = [[TMFightResultItemView alloc] initBossOfLocation];
        [resultViews addObject:bossResult];
    }
    
    TMFightResultItemView *attributePointsResult = [[TMFightResultItemView alloc] initAttributePoints:25];
    [resultViews addObject:attributePointsResult];

    [self.fightResultViewController loadWithResultsViews:resultViews];
    [self.fightResultViewController loadWin];
    
    self.fightResultViewController.view.alpha = 0.0f;
    
    [self.view addSubview:self.fightResultViewController.view];
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.fightResultViewController.view.alpha = 1.0f;
                     }];
}


- (void)presentFightLost
{
    NSMutableArray *resultViews = [[NSMutableArray alloc] init];
    
    TMFightResultItemView *attributePointsResult = [[TMFightResultItemView alloc] initAttributePoints:5];
    [resultViews addObject:attributePointsResult];

    
    [self.fightResultViewController loadWithResultsViews:resultViews];
    [self.fightResultViewController loadLose];
    
    self.fightResultViewController.view.alpha = 0.0f;
    [self.view addSubview:self.fightResultViewController.view];
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.fightResultViewController.view.alpha = 1.0f;
                     }];
}

#pragma mark - Load Data Actions
////////////////////////////////////////////////////////////////////////////////

- (void)loadOpponentData
{
    self.opponentNameLabel.text = [[self.opponentData objectForKey:@"name"] uppercaseString];
}

- (void)loadPlayerData
{
    self.playerNameLabel.text = [[self.playerData objectForKey:@"name"] uppercaseString];
}

- (void)loadLocationData
{
    if (self.locationData) {
        NSString *locationName = [[self.locationData objectForKey:@"name"] uppercaseString];
        self.locationLabel.text = locationName;
        [self.locationLabel setHidden:NO];
    } else {
        [self.locationLabel setHidden:YES];
    }
}

- (void)loadHealthBars
{
    if (IS_IPHONE_5) {
        self.opponentHealthBar = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(6.0f, 472.0f, 309.0f, 30.0f)
                                                                   progress:100.0f
                                                                       type:TMProgressBarOpponentHealth];
        
        self.playerHealthBar = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(6.0f, 521.0f, 309.0f, 30.0f)
                                                                 progress:100.0f
                                                                     type:TMProgressBarPlayerHealth];
    } else {
        self.opponentHealthBar = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(6.0f, 387.0f, 309.0f, 30.0f)
                                                                   progress:100.0f
                                                                       type:TMProgressBarOpponentHealth];
        
        self.playerHealthBar = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(6.0f, 429.0f, 309.0f, 30.0f)
                                                                 progress:100.0f
                                                                     type:TMProgressBarPlayerHealth];
    }
    
    self.opponentHealthBar.alpha = 0.8f;
    self.playerHealthBar.alpha = 0.8f;
    
    [self.view insertSubview:self.opponentHealthBar
                belowSubview:self.opponentCharacterOutline];
    
    [self.view insertSubview:self.playerHealthBar
                belowSubview:self.playerCharacterOutline];
}

- (void)loadFightSequences
{
    DebugLog(@"Loading fight sequences");
    [[TMFight sharedInstance] getSequencesForFight:[self.fightData objectForKey:@"_id"]];
}

- (void)loadWaitingIndicators
{
    NSString *playerID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    
    if (checkPlayerIsAttacker(playerID, self.fightData)) {
        self.indicator.image = [UIImage imageNamed:@"WaitingP2.png"];
    } else {
        self.indicator.image = [UIImage imageNamed:@"WaitingP1.png"];
    }
}


- (void)reloadFightState
{
    NSString *activeCharacterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    
    // Set the health for both players
    float currPlayerHealth = healthForCharacterFromSequences(self.playerData, self.opponentData, self.fightSequences);
    float playerHealth = [[[self.playerData objectForKey:@"attributes"] objectForKey:@"health"] floatValue];
    
    float currOpponentHealth = healthForCharacterFromSequences(self.opponentData, self.playerData, self.fightSequences);
    float opponentHealth = [[[self.opponentData objectForKey:@"attributes"] objectForKey:@"health"] floatValue];

    
    // Redraw health bars
    [[self playerHealthBar] reduceBarToProgressLevel:(currPlayerHealth / playerHealth * 100)];
    [[self opponentHealthBar] reduceBarToProgressLevel:(currOpponentHealth / opponentHealth * 100)];

    DebugLog(@"Player: %f Opponent: %f", currPlayerHealth, currOpponentHealth);
    
    if (currOpponentHealth > 0 && currPlayerHealth > 0 && [self.fightSequences count] < 4) {
        switch (checkPlayerState(activeCharacterID, self.fightData, self.fightSequences)) {
            case kFightModeWaiting:
                // Present waiting indicator
                self.fightStateLabel.text = @"WAITING";
                self.allowFightInput = NO;
                self.enteringFightSequence = NO;
                [self presentWaitingIndicator];
                
                // Run NPC actions
                if (checkOpponentIsNPC(self.fightData)) {
                    [self runNPCActionSequence];
                }
                [self reloadFightSequences];
                
                break;
            case kFightModeAttack:
                // Present attack "ready" button
                self.fightStateLabel.text = @"ATTACK";
                [self hideWaitingIndicator];
                [self presentReadyButton];
                [self reloadFightSequencesStop];
                break;
            case kFightModeDefend:
                // Present defend "ready" button
                self.fightStateLabel.text = @"DEFEND";
                [self hideWaitingIndicator];
                [self presentReadyButton];
                [self reloadFightSequencesStop];
                break;
        }
    } else {
        NSString *winnerID = nil;
        NSString *fightID = [self.fightData objectForKey:@"_id"];
        NSString *locationID = [self.fightData objectForKey:@"_location"];
        
        if (currOpponentHealth < currPlayerHealth) {
            DebugLog(@"Player Won");
            winnerID = [self.playerData objectForKey:@"_user_character"];
            
            if (self.bossFight) {
                [[TMLocation sharedInstance] updateLocation:locationID
                                                 parameters:@{ @"_boss" : winnerID,
                                                               @"boss_type" : @"player"}];
            }
            
            [self presentFightWon];
            
        } else {
            DebugLog(@"Opponent Won");
            winnerID = [self.opponentData objectForKey:@"_user_character"];
            
            if (self.bossFight) {
                NSString *playerID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
                NSString *bossType = [opponentForFight(playerID, self.fightData) objectForKey:@"type"];
                
                [[TMLocation sharedInstance] updateLocation:locationID
                                                 parameters:@{ @"_boss" : winnerID,
                                                               @"boss_type" : bossType }];
            }
            
            [self presentFightLost];
        }
        
        [[TMFight sharedInstance] stopFight:fightID
                                 withWinner:winnerID];
    }
}

- (void)reloadFightSequences
{
    DebugLog(@"Opponent is NPC: %d", checkOpponentIsNPC(self.fightData));
    self.sequencesUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f
                                                                 target:self
                                                               selector:@selector(loadFightSequences)
                                                               userInfo:nil
                                                                repeats:NO];
}

- (void)reloadFightSequencesStop
{
    [self.sequencesUpdateTimer invalidate];
}

- (void)runNPCActionSequence
{
    NSString *playerID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    NSDictionary *opponent = opponentForFight(playerID, self.fightData);
    NSString *opponentID = [opponent objectForKey:@"_user_character"];
    
    NSString *fightID = [self.fightData objectForKey:@"_id"];
    
    switch(checkPlayerState(opponentID, self.fightData, self.fightSequences)) {
        case kFightModeAttack:
            // NPC is attacking
            [[TMFight sharedInstance] createSequence:nil
                                            forFight:fightID
                                       withCharacter:opponentID
                                        sequenceType:kSequenceTypeAttack];
            break;
        case kFightModeDefend:
            // NPC is defending
            [[TMFight sharedInstance] createSequence:nil
                                            forFight:fightID
                                       withCharacter:opponentID
                                        sequenceType:kSequenceTypeDefend];
            break;
        case kFightModeWaiting:
            break;
    }
    
}

#pragma mark - Presentation Actions
////////////////////////////////////////////////////////////////////////////////

- (void)presentWaitingIndicator
{
    DebugLog(@"Fight State: Waiting");
    [self loadWaitingIndicators];
    
    if (self.indicator.hidden) {
        self.indicator.alpha = 0.0f;
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.indicator.hidden = NO;
                             self.indicator.alpha = 1.0f;
                         }];
    }
}

- (void)hideWaitingIndicator
{
    if (!self.indicator.hidden) {
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.indicator.alpha = 0.0f;
                         } completion:^(BOOL finished) {
                             self.indicator.hidden = NO;
                         }];
    }
}

- (void)presentReadyButton
{
    if (self.readyButton.hidden && self.allowFightInput == NO && self.enteringFightSequence == NO) {
        self.readyButton.hidden = YES;
        self.allowFightInput = NO;
        self.readyButton.alpha = 0.0f;
        self.readyButton.hidden = NO;
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.readyButton.alpha = 1.0f;
                         }];
    }
}

#pragma mark - Current Fight State
////////////////////////////////////////////////////////////////////////////////

- (void)startFightSequence
{
    self.countDownLabel.alpha = 0.0f;
    self.countDownLabel.hidden = NO;
    self.countDownLabel.text = @"3";
    self.countDownVal = 3;
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.countDownLabel.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                         self.countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                                                target:self
                                                                              selector:@selector(countDownToFight)
                                                                              userInfo:nil
                                                                               repeats:YES];
                     }];
}

- (void)countDownToFight
{
    self.countDownVal--;
    
    if (!self.countDownVal) {
        [self.countDownTimer invalidate];
        
        if (self.currentFightMode == kFightModeAttack) {
            self.countDownLabel.text = @"ATTACK";
        } else if (self.currentFightMode == kFightModeDefend) {
            self.countDownLabel.alpha = 0.0f;
            self.countDownLabel.text = @"DEFEND";
        }
        
        if (self.currentFightMode == kFightModeAttack) {
            [UIView animateWithDuration:1.0f
                             animations:^{
                                 self.countDownLabel.alpha = 0.0f;
                             } completion:^(BOOL finished) {
                                 self.countDownVal = 3;
                                 self.allowFightInput = YES;
                                 self.enteringFightSequence = YES;
                             }];
        } else {
            self.enteringFightSequence = YES;
            [self loadOpponentAttack];
            [self replayOpponentAttack];
        }
        
    } else {
        self.countDownLabel.text = [NSString stringWithFormat:@"%d", self.countDownVal];
    }
}

#pragma mark - Fight Sequence Action Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)fightSequencesRetrieved:(NSNotification *)notification
{
    DebugLog(@"Fight Sequences Retrieved: %@", [notification object]);
    self.fightSequences = [[notification object] mutableCopy];
    [self reloadFightState];
}

- (void)fightSequencesRetrieveFailed:(NSNotification *)notification
{
    
}

- (void)fightSequenceCreated:(NSNotification *)notification
{
    DebugLog(@"Fight Sequence Created: %@", [notification object]);
    
    [self.fightSequences removeAllObjects];
    [self.playerDefenseTouches removeAllObjects];
    [self.playerAttackTouches removeAllObjects];
    [self.opponentAttackTouches removeAllObjects];
    [self.opponentAttackTouchSprites removeAllObjects];
    
    [self reloadFightSequences];
}

- (void)fightSequenceCreateFailed:(NSNotification *)notification
{
    DebugLog(@"Fight Sequence Create Failed: %@", [notification object]);
}


#pragma mark - Fight Stop Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)fightStopped:(NSNotification *)notification
{
    DebugLog(@"Fight Stopped: %@", [notification object]);
}


- (void)fightStopFailed:(NSNotification *)notification
{
    DebugLog(@"Fight Stop Failed: %@", [notification object]);
}

#pragma mark - Location Action Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)locationUpdated:(NSNotification *)notification
{
    DebugLog(@"Location updated: %@", [notification object]);
}


- (void)locationUpdateFailed:(NSNotification *)notification
{
    DebugLog(@"Location update failed: %@", [notification object]);
}


#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (void)backToMain:(id)sender
{
    [[TMViewHandlerController sharedInstance] presentMainView];
}

- (IBAction)readyButtonAction:(id)sender
{
    NSString *activeCharacterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    self.currentFightMode = checkPlayerState(activeCharacterID, self.fightData, self.fightSequences);
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         [self.readyButton setAlpha:0.0f];
                     } completion:^(BOOL finished) {
                         [self startFightSequence];
                         self.readyButton.hidden = YES;
                     }];
}


#pragma mark - Touch Handle Actions
////////////////////////////////////////////////////////////////////////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:touch.view];
    float maxY = 0;
    
    if (IS_IPHONE_5) {
        maxY = k4inchFightTouchAreaYMax;
    } else {
        maxY = k3inchFightTouchAreaYMax;
    }
    
    if (self.allowFightInput && location.y >= kFightTouchAreaYMin && location.y < maxY) {
        
        UITouch *touch = [touches anyObject];
        
        if (self.currentFightMode == kFightModeAttack) {
            // Handle attack touches
            [self attackTouch:touch];
            [self displayAttackTouch:touch];
            [self rumbleOpponentCharacter];
        } else if (self.currentFightMode == kFightModeDefend) {
            // Handle defend touch
            [self defendTouch:touch];
            [self displayDefendTouch:touch];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSString *characterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    NSDictionary *characterAttributes = characterAttributesForFight(characterID, self.fightData);
    NSInteger characterMaxAttackPoints = [[characterAttributes objectForKey:@"attack_points"] integerValue];
    
    if (self.allowFightInput) {
        if (self.currentFightMode == kFightModeAttack) {
            // Handle attack touches
            if ([self.playerAttackTouches count] == characterMaxAttackPoints) {
                [self attackTouchSequenceEnded];
            }
        } else if (self.currentFightMode == kFightModeDefend) {
            if ([self.playerDefenseTouches count] == [self.opponentAttackTouches count]) {
                [self defendTouchSequenceEnded];
            }
        }
    }
}


- (UIImageView *)displayTouch:(UITouch *)touch type:(FightMode)touchState
{
    CGPoint location = [touch locationInView:touch.view];
    
    // Create a point image based on their input
    UIImage *touchPointImage = nil;
    UIImageView *touchPointView = nil;
    
    if (touchState == kFightModeAttack) {
        touchPointImage = [UIImage imageNamed:@"AttackPoint.png"];
    } else if (touchState == kFightModeDefend) {
        touchPointImage = [UIImage imageNamed:@"DefendPoint.png"];
    }
    
    touchPointView = [[UIImageView alloc] initWithImage:touchPointImage];
    
    // Animate the touch
    touchPointView.center = location;
    touchPointView.transform = CGAffineTransformMakeScale(0, 0);
    
    [self.view addSubview:touchPointView];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         touchPointView.transform = CGAffineTransformMakeScale(1, 1);
                     } completion:^(BOOL finished) {
                         touchPointView.hidden = YES;
                     }];
    
    return touchPointView;
}





#pragma mark - Opponent Attack Actions
////////////////////////////////////////////////////////////////////////////////

- (void)loadOpponentAttack
{
    self.opponentAttackTouches = nil;
    self.opponentAttackTouches = [[[self.fightSequences lastObject] objectForKey:@"points"] mutableCopy];
    
    DebugLog(@"Opponent attack touches: %@", self.opponentAttackTouches);
    
    self.opponentAttackTouchSprites = nil;
    self.opponentAttackTouchSprites = [[NSMutableArray alloc] init];
    
    for (NSDictionary *point in self.opponentAttackTouches) {
        UIImageView *attackPoint = sequencePointToTouchPoint(point, kFightModeAttack);
        
        attackPoint.hidden = YES;
        attackPoint.transform = CGAffineTransformMakeScale(0, 0);
        [self.opponentAttackTouchSprites addObject:attackPoint];
        [self.view insertSubview:attackPoint
                    belowSubview:self.indicator];
    }
}

- (void)replayOpponentAttack
{
    if (self.opponentAttackCount < ([self.opponentAttackTouches count])) {
        
        if (self.opponentAttackReplayTimer == nil) {
            // Initialise the timer
            self.opponentAttackReplayTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f
                                                                              target:self
                                                                            selector:@selector(replayOpponentAttack)
                                                                            userInfo:nil
                                                                             repeats:YES];
        } else {
            UIImageView *attackPoint = [self.opponentAttackTouchSprites objectAtIndex:self.opponentAttackCount];

            DebugLog(@"Opponent Attack Point x - %f y - %f", attackPoint.center.x, attackPoint.center.y);
            attackPoint.hidden = NO;
            [UIView animateWithDuration:0.2
                             animations:^{
                                 attackPoint.transform = CGAffineTransformMakeScale(1.0, 1.0);
                             } completion:^(BOOL finished) {
                                 [UIView animateWithDuration:0.2
                                                  animations:^{
                                                      attackPoint.transform = CGAffineTransformMakeScale(0.0, 0.0);
                                                  } completion:^(BOOL finished) {
                                                      attackPoint.hidden = YES;
                                                  }];
                             }];
            
            self.opponentAttackCount++;
        }
    
    } else {
        [self.opponentAttackReplayTimer invalidate];
        self.opponentAttackReplayTimer = nil;
        self.opponentAttackCount = 0;
        self.countDownLabel.alpha = 1.0f;
        
        [UIView animateWithDuration:1.0f
                         animations:^{
                             self.countDownLabel.alpha = 0.0f;
                         } completion:^(BOOL finished) {
                             self.countDownVal = 3;
                             self.allowFightInput = YES;
                         }];
        
    }
}

#pragma mark - Attack Touch Actions
////////////////////////////////////////////////////////////////////////////////

- (void)attackTouch:(UITouch *)touch
{
    NSString *characterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    NSDictionary *characterAttributes = characterAttributesForFight(characterID, self.fightData);
    NSInteger characterMaxAttackPoints = [[characterAttributes objectForKey:@"attack_points"] integerValue];
    
    CGPoint location = [touch locationInView:touch.view];
    
    DebugLog(@"Character Attributes: %@", characterAttributes);
    DebugLog(@"Max attack points: %d", characterMaxAttackPoints);
    
    // Initialise the array if it is empty
    if (self.playerAttackTouches == nil) {
        self.playerAttackTouches = [[NSMutableArray alloc] init];
    }
    
    // Record the player attack touches
    if ([self.playerAttackTouches count] < characterMaxAttackPoints) {
        CGPoint apiAttackPoint = scaleTouchToAPI(location);
        NSDictionary *attackPoint = @{ @"x" : [NSString stringWithFormat:@"%f", apiAttackPoint.x],
                                       @"y" : [NSString stringWithFormat:@"%f", apiAttackPoint.y] };
        [self.playerAttackTouches addObject:attackPoint];
    }
}

- (void)attackTouchSequenceEnded
{
    NSString *characterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    
    [[TMFight sharedInstance] createSequence:self.playerAttackTouches
                                    forFight:[self.fightData objectForKey:@"_id"]
                               withCharacter:characterID
                                sequenceType:kSequenceTypeAttack];
    
    [self presentWaitingIndicator];
    self.allowFightInput = NO;
}

- (void)displayAttackTouch:(UITouch *)touch
{
    NSString *characterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    NSDictionary *characterAttributes = characterAttributesForFight(characterID, self.fightData);
    NSInteger characterMaxAttackPoints = [[characterAttributes objectForKey:@"attack_points"] integerValue];
    
    if ([self.playerAttackTouches count] <= characterMaxAttackPoints) {
        [self displayTouch:touch type:kFightModeAttack];
    }
}


#pragma mark - Defenseive Touch Actions
////////////////////////////////////////////////////////////////////////////////

- (void)defendTouch:(UITouch *)touch
{
    CGPoint defendTouchLocation = [touch locationInView:touch.view];
    
    if (self.playerDefenseTouches == nil) {
        self.playerDefenseTouches = [[NSMutableArray alloc] init];
    }
    
    // Record the defense touches
    if ([self.playerDefenseTouches count] < [self.opponentAttackTouches count]) {
        // Add defense point
        CGPoint apiDefendPoint = scaleTouchToAPI(defendTouchLocation);
        NSDictionary *defendPoint = @{ @"x" : [NSString stringWithFormat:@"%f", apiDefendPoint.x],
                                       @"y" : [NSString stringWithFormat:@"%f", apiDefendPoint.y] };
        
        // Record defensive touch
        [self.playerDefenseTouches addObject:defendPoint];
    }
}

- (void)defendTouchSequenceEnded
{
    NSString *characterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    NSDictionary *defendSequence = @{ @"points" : self.playerDefenseTouches,
                                      @"_user_character" : characterID,
                                      @"type" : @"defend"};
    
    DebugLog(@"Created Defend Sequence: %@", defendSequence);
    [self.fightSequences addObject:defendSequence];
    
    DebugLog(@"Fight Sequence Count: %d", [self.fightSequences count]);
    
    float currPlayerHealth = healthForCharacterFromSequences(self.playerData, self.opponentData, self.fightSequences);
    float playerHealth = [[[self.playerData objectForKey:@"attributes"] objectForKey:@"health"] floatValue];
    
    DebugLog(@"Fight View Current Player Health: %f", currPlayerHealth);
    DebugLog(@"Fight View Player Health: %f", playerHealth);

    // Present defend and attack comparison
    DebugLog(@"Player Health Bar: %f", (currPlayerHealth/ playerHealth * 100));
    [self displayDefendAndAttackTouches];
    [self.playerHealthBar reduceBarToProgressLevel:(currPlayerHealth/ playerHealth * 100)];
    
    self.allowFightInput = NO;
    self.enteringFightSequence = NO;
    
    if (currPlayerHealth > 0) {
        [[TMFight sharedInstance] createSequence:self.playerDefenseTouches
                                        forFight:[self.fightData objectForKey:@"_id"]
                                   withCharacter:characterID
                                    sequenceType:kSequenceTypeDefend];
        
        self.currentFightMode = kFightModeAttack;
    } else {
        NSString *fightID = [self.fightData objectForKey:@"_id"];
        NSString *opponentID = [self.opponentData objectForKey:@"_user_character"];
        [self presentFightLost];
        [[TMFight sharedInstance] stopFight:fightID withWinner:opponentID];
    }
}


- (void)displayDefendTouch:(UITouch *)touch
{
    UIImageView *defensiveTouchSprite = [self displayTouch:touch type:kFightModeDefend];
    
    if (self.playerDefenseTouchSprites == nil) {
        self.playerDefenseTouchSprites = [[NSMutableArray alloc] init];
    }
    
    [self.playerDefenseTouchSprites addObject:defensiveTouchSprite];
}


- (void)displayDefendAndAttackTouches
{
    [self presentFadeInAndOutSprites:self.playerDefenseTouchSprites];
    [self presentFadeInAndOutSprites:self.opponentAttackTouchSprites];
}

- (void)presentFadeInAndOutSprites:(NSMutableArray *)sprites
{
    DebugLog(@"Total Sprites: %d", [sprites count]);
    
    for (UIImageView *touchSprite in sprites) {
        touchSprite.transform = CGAffineTransformMakeScale(1.0, 1.0);
        touchSprite.alpha = 0.0f;
        touchSprite.hidden = NO;
        
        DebugLog(@"Touch Sprite: %f %f", touchSprite.center.x, touchSprite.center.y);
        
        [UIView animateWithDuration:1.0f
                              animations:^{
                                  touchSprite.alpha = 1.0f;
                              } completion:^(BOOL finished) {
                                  [UIView animateWithDuration:1.5f
                                                   animations:^{
                                                       touchSprite.alpha = 0.0f;
                                                   } completion:^(BOOL finished) {
                                                       touchSprite.hidden = YES;
                                                       [self presentReadyButton];
                                                   }];
                              }];
    }
}

#pragma mark - User Character Actions 
////////////////////////////////////////////////////////////////////////////////

- (void)rumbleOpponentCharacter
{
    float low_bound = -50.00;
    float high_bound = 50.00;
    float randX = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    float randY = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    CGPoint characterCenter = self.characterView.center;
    
    DebugLog(@"Rand X: %f, Rand Y: %f", self.characterView.center.x + randX, self.characterView.center.y + randY);
    CGPoint attackPos = CGPointMake(self.characterView.center.x + randX, self.characterView.center.y + randY);
    [UIView animateWithDuration:0.15
                     animations:^{
                         self.characterView.center = attackPos;
                     } completion:^(BOOL finished) {
                         self.characterView.center = characterCenter;
                     }];
}





@end
