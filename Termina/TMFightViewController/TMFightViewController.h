#import <UIKit/UIKit.h>

@interface TMFightViewController : UIViewController

////////////////////////////////////////////////////////////////////////////////
// Data Objects

/** Fight Data */
@property (nonatomic, strong) NSDictionary *fightData;

/** Location Data */
@property (nonatomic, strong) NSDictionary *locationData;

/** Opponent Data */
@property (nonatomic, strong) NSDictionary *opponentData;

/** Opponent Checkin Data */
@property (nonatomic, strong) NSDictionary *opponentCheckinData;

/** Player Data */
@property (nonatomic, strong) NSDictionary *playerData;

/** Opponent Checkin Data */
@property (nonatomic, strong) NSDictionary *playerCheckinData;

/** Boss Fight */
@property (nonatomic) BOOL bossFight;

/** Quest Fight */
@property (nonatomic) BOOL questFight;

////////////////////////////////////////////////////////////////////////////////
// Interface Objects

/** View background image */
@property (nonatomic, strong) IBOutlet UIImageView  *background;

/** Ready Button */
@property (nonatomic, strong) IBOutlet UIButton *readyButton;

/** State indicator - Win, Lose, Waiting */
@property (nonatomic, strong) IBOutlet UIImageView *indicator;

/** Character view */
@property (nonatomic, strong) IBOutlet UIImageView *characterView;

/** Button to show the count down state */
@property (nonatomic, strong) IBOutlet UILabel *countDownLabel;

/** Player Character Outline */
@property (nonatomic, strong) IBOutlet UIImageView *playerCharacterOutline;

/** Opponent Character Outline */
@property (nonatomic, strong) IBOutlet UIImageView *opponentCharacterOutline;

/** Location Label */
@property (nonatomic, strong) IBOutlet UILabel *locationLabel;

/** Opponent Name Label */
@property (nonatomic, strong) IBOutlet UILabel *opponentNameLabel;

/** Player Name Label */
@property (nonatomic, strong) IBOutlet UILabel *playerNameLabel;

/** Fight State Label */
@property (nonatomic, strong) IBOutlet UILabel *fightStateLabel;

/** Move back to main view */
- (IBAction)backToMain:(id)sender;

/** Ready button action */
- (IBAction)readyButtonAction:(id)sender;

@end
