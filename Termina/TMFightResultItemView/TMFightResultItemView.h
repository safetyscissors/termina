//
//  TMFightResultItemView.h
//  Termina
//
//  Created by Jason Lagaac on 5/12/2013.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum FightResult
{
    kFightResultBoss,
    kFightResultAttribute,
    kFightResultItem,
    kFightResultMoney,
    kFightResultBonus
} tFightResult;

@interface TMFightResultItemView : UIView

// View to show that the user is now the boss of the location
- (id)initBossOfLocation;

// View to show that the user obtained attribute points
- (id)initAttributePoints:(NSInteger)addedAttributePoints;

@end
