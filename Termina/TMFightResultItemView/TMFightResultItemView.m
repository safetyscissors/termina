//
//  TMFightResultItemView.m
//  Termina
//
//  Created by Jason Lagaac on 5/12/2013.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMFightResultItemView.h"

@interface TMFightResultItemView ()

/** Result icon image */
@property (nonatomic, strong) UIImageView *icon;

/** Result detail text */
@property (nonatomic, strong) UILabel *resultLabel;

/** Result detail text */
@property (nonatomic, strong) UILabel *resultDetailLabel;

@end


@implementation TMFightResultItemView

- (id)init
{
    if (self = [self initWithFrame:CGRectMake(0.0f, 0.0f, 230.0f, 188.0f)]) {
        self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80.0f, 80.0f)];
        self.icon.center = CGPointMake(115,53);
        [self addSubview:self.icon];
        
        self.resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 219.0f, 41.0f)];
        self.resultLabel.center = CGPointMake(116, 126);
        self.resultLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.resultLabel.numberOfLines = 2;
        self.resultLabel.textAlignment = NSTextAlignmentCenter;
        [self.resultLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0f]];
        self.resultLabel.textColor = [UIColor whiteColor];
        
        [self addSubview:self.resultLabel];
        
        
        self.resultDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 219.0f, 37.0f)];
        self.resultDetailLabel.center = CGPointMake(116, 162);
        self.resultDetailLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.resultDetailLabel.numberOfLines = 2;
        self.resultDetailLabel.textAlignment = NSTextAlignmentCenter;
        [self.resultDetailLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f]];
        self.resultDetailLabel.textColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.8f];

        [self addSubview:self.resultDetailLabel];
    }
    
    return self;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - Custom Initialisation Actions
////////////////////////////////////////////////////////////////////////////////

- (id)initBossOfLocation
{
    if ([self init]) {
        [self.icon setImage:[self loadIcon:kFightResultBoss]];
        [self.resultLabel setText:@"You Are The Boss"];
        [self.resultDetailLabel setText:[self loadDescriptionText:kFightResultBoss]];
    }
    
    return self;
}

- (id)initAttributePoints:(NSInteger)addedAttributePoints
{
    if ([self init]) {
        [self.icon setImage:[self loadIcon:kFightResultAttribute]];
        
        NSString *resultLabel = [NSString stringWithFormat:@"+%d Attribute Points", addedAttributePoints];
        [self.resultLabel setText:resultLabel];
        [self.resultDetailLabel setText:[self loadDescriptionText:kFightResultAttribute]];
    }
    
    return self;
}

#pragma mark - Load Actions
////////////////////////////////////////////////////////////////////////////////

- (UIImage *)loadIcon:(tFightResult)fightResultType
{
    UIImage *resultIcon = nil;
    
    switch (fightResultType) {
        case kFightResultBoss:
            resultIcon = [UIImage imageNamed:@"FightResultBoss.png"];
            break;
        case kFightResultAttribute:
            resultIcon = [UIImage imageNamed:@"FightResultAttribute.png"];
            break;
        case kFightResultBonus:
            resultIcon = [UIImage imageNamed:@"FightResultBonus.png"];
            break;
        case kFightResultItem:
            resultIcon = [UIImage imageNamed:@"FightResultItem.png"];
            break;
        case kFightResultMoney:
            resultIcon = [UIImage imageNamed:@"FightResultMoney.png"];
            break;
        default:
            break;
    }
    
    return resultIcon;
}

- (NSString *)loadDescriptionText:(tFightResult)fightResultType
{
    NSString *resultText = nil;
    
    switch (fightResultType) {
        case kFightResultBoss:
            resultText = @"You Control This Location.\n You Can Now Collect Rent.";
            break;
        case kFightResultAttribute:
            resultText = @"Go to D.I.S.C.S Data to Manage";
            break;
        case kFightResultBonus:
            resultText = @"This Bonus Will Be Used In Your Next Fight";
            break;
        case kFightResultItem:
            resultText = @"Go to D.I.S.C.S Items to Manage";
            break;
        case kFightResultMoney:
            resultText = @"Go to the Store to Buy Items";
            break;
        default:
            break;
    }
    
    return resultText;
}


@end
