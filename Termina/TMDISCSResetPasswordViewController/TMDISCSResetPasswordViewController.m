#import "TMDISCSResetPasswordViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "TMUser.h"

@interface TMDISCSResetPasswordViewController ()

@end

@implementation TMDISCSResetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userUpdated:)
                                                 name:kUserUpdated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userUpdateFailed:)
                                                 name:kUserUpdateFailed
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)changePassword:(id)sender
{
    [self.passwordField resignFirstResponder];
    [self.passwordConfirmationField resignFirstResponder];
    
    if ([self.passwordField.text length] && [self.passwordConfirmationField.text length]) {
        if ([self.passwordConfirmationField.text isEqualToString:self.passwordField.text]) {
            // Change the password
            NSString *username = [TMUser sharedInstance].username;
            [[TMUser sharedInstance] updateUser:username
                                     parameters:@{ @"password" : self.passwordField.text}];
        } else {
            [SVProgressHUD showErrorWithStatus:@"Please ensure that both password fields are the same"];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:@"Please fill both fields"];
    }
}

- (IBAction)backButtonPressed:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"backToDefaultSettingsView"
                                                        object:nil];
}

#pragma mark - User Update Handler Actions
////////////////////////////////////////////////////////////////////////////////

- (void)userUpdated:(NSNotification *)notification
{
    [SVProgressHUD showSuccessWithStatus:@"Password Updated"];
}

- (void)userUpdateFailed:(NSNotification *)notification
{
    [SVProgressHUD showErrorWithStatus:@"Password Update Failed.\n\n Please try again later"];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];

    if (self.passwordField != [touch view] || self.passwordConfirmationField != [touch view]) {
        [self.passwordField resignFirstResponder];
        [self.passwordConfirmationField resignFirstResponder];
        
    }
}

#pragma mark - Textfield Return Delegates
////////////////////////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.passwordField) {
        [self.passwordConfirmationField becomeFirstResponder];
    } else {
        [self changePassword:nil];
    }
    
    return YES;
}



@end
