#import <UIKit/UIKit.h>

@interface TMDISCSResetPasswordViewController : UIViewController <UITextFieldDelegate>

/** Password Field */
@property (nonatomic, strong) IBOutlet UITextField *passwordField;

/** Password Field */
@property (nonatomic, strong) IBOutlet UITextField *passwordConfirmationField;

/** Change password button */
@property (nonatomic, strong) IBOutlet UIButton *changePassword;

/** Change Password Action */
- (IBAction)changePassword:(id)sender;

/** Back Button Action */
- (IBAction)backButtonPressed:(id)sender;

@end
