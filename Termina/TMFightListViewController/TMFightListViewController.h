#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "OCMapView.h"

enum
{
    kFightListLocationMap,
    kFightListLocationList
} typedef MapToggleState;

enum
{
    kFiltersHidden,
    kFiltersShown
} typedef FilterVisibleState;

enum
{
    kFilterGlobal,
    kFilterFriends,
    kFilterNearBy
} typedef FilterState;


@interface TMFightListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate>

/** Location / Friend / Global data table */
@property (nonatomic, strong) IBOutlet UITableView *dataTable;

/** Location Map */
@property (nonatomic, strong) IBOutlet OCMapView *locationMap;

/** List/Map toggle button */
@property (nonatomic, strong) IBOutlet UIButton *listMapToggleButton;

/** Filter show/hide button */
@property (nonatomic, strong) IBOutlet UIButton *filterToggle;

/** Filter button container */
@property (nonatomic, strong) IBOutlet UIView *container;

/** Global filter button */
@property (nonatomic, strong) IBOutlet UIButton *globalButton;

/** Friends filter button */
@property (nonatomic, strong) IBOutlet UIButton *friendsButton;

/** Nearby filter button */
@property (nonatomic, strong) IBOutlet UIButton *nearbyButton;

/** Back to central button action */
- (IBAction)backAction:(id)sender;

/** Toggle filter button action */
- (IBAction)toggleFilterView:(id)sender;

@end