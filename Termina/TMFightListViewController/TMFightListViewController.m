#import "TMFightListViewController.h"
#import "TMMainViewController.h"
#import "TMFightFriendsViewController.h"
#import "TMFightListLocationCell.h"
#import "TMFightListOpponentCell.h"
#import "TMUser.h"
#import "TMLocation.h"
#import "TMViewHandlerController.h"
#import "TMLocationAnnotation.h"
#import <SVProgressHUD/SVProgressHUD.h>

static CGFloat kDEFAULTCLUSTERSIZE = 0.1;


@interface TMFightListViewController ()

/** List friend fighters */
@property (nonatomic, strong) TMFightFriendsViewController *fightFriendsViewController;

/** Map/List toggle state */
@property (nonatomic) MapToggleState toggleButtonState;

/** Filter is visible */
@property (nonatomic) FilterVisibleState filterVisibleState;

/** Current filter state */
@property (nonatomic) FilterState filterState;

/** Friend characters */
@property (nonatomic) NSMutableArray *friendCharacters;

/** Total number of friends */
@property (nonatomic) int friendCount;

/** Selected row */
@property (nonatomic) int tableSelectedRow;

@end

@implementation TMFightListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.locationMap.delegate = self;
    self.locationMap.clusterSize = kDEFAULTCLUSTERSIZE;
    self.locationMap.clusteringEnabled = YES;
    self.locationMap.showsUserLocation = YES;
    self.locationMap.hidden = YES;

    [self.nearbyButton setImage:[UIImage imageNamed:@"NearbyFilterButtonActive.png"]
                  forState:UIControlStateNormal];
    
    MKUserLocation *userLocation = self.locationMap.userLocation;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (userLocation.location.coordinate, 20, 20);
    [self.locationMap setRegion:region animated:NO];
    
    [self loadFriendsViewController];
    
    self.toggleButtonState = kFightListLocationList;
}

- (void)viewDidAppear:(BOOL)animated
{
    if (!self.dataTable.hidden && self.locationMap.hidden) {
        [SVProgressHUD showWithStatus:@"Loading Locations"];
        [[TMLocation sharedInstance] startMonitoringLocation];
    }
    
    
    // Setup the map
    if (self.locationMap.hidden) {
        MKUserLocation *userLocation = self.locationMap.userLocation;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (userLocation.location.coordinate, 20, 20);
        [self.locationMap setRegion:region animated:NO];
    }
    
    self.filterState = kFilterNearBy;
    [self.locationMap doClustering];
    
    // Define NSNotifcations
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationsNearByRetrieved:)
                                                 name:kLocationsWithinRadiusRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationsNearByRetrieveFail:)
                                                 name:kLocationsWithinRadiusRetrieveFailed
                                               object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.dataTable resignFirstResponder];
    [[TMLocation sharedInstance] stopMonitoringLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData
{
    [UIView transitionWithView:self.dataTable
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        [self.dataTable reloadData];
                    } completion:nil];
}

- (void)loadFriendsViewController
{
    NSString *nibName = IS_IPHONE_5 ? @"TMFightFriendsViewController-568h" : @"TMFightFriendsViewController";
    self.fightFriendsViewController = [[TMFightFriendsViewController alloc] initWithNibName:nibName
                                                                                     bundle:nil];
    self.fightFriendsViewController.view.hidden = YES;
    self.fightFriendsViewController.view.frame = self.container.frame;
    [self.view insertSubview:self.fightFriendsViewController.view
                belowSubview:self.locationMap];
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)backAction:(id)sender
{
    [[TMMainViewController sharedInstance] presentCentralView];
}

- (IBAction)toggleListAndMap:(id)sender
{

    if (self.toggleButtonState == kFightListLocationList) {
        self.toggleButtonState = kFightListLocationMap;
        self.locationMap.hidden = NO;
        CGRect locationMapFrame = self.locationMap.frame;
        locationMapFrame.origin = CGPointMake(0, 0);

        [self.listMapToggleButton setImage:[UIImage imageNamed:@"MapButton.png"]
                             forState:UIControlStateNormal];
        [UIView animateWithDuration:0.2f
                         animations:^{
                             self.locationMap.alpha = 1.0f;
                             self.dataTable.alpha = 0.0f;
                         } completion:^(BOOL finished) {
                             self.dataTable.hidden = YES;
                         }];
    } else {
        self.dataTable.hidden = NO;
        self.toggleButtonState = kFightListLocationList;
        [self.listMapToggleButton setImage:[UIImage imageNamed:@"ListButton.png"]
                                  forState:UIControlStateNormal];
        
        [UIView animateWithDuration:0.2f animations:^{
            self.locationMap.alpha = 0.0f;
            self.dataTable.alpha = 1.0f;
        } completion:^(BOOL finished) {
            self.locationMap.hidden = YES;
        }];
    }
}

- (IBAction)toggleFilterView:(id)sender
{
    if (self.filterVisibleState == kFiltersHidden) {
        self.filterVisibleState = kFiltersShown;
        [self.filterToggle setImage:[UIImage imageNamed:@"FilterButtonActive.png"] forState:UIControlStateNormal];
        
        float pos_y = IS_IPHONE_5 ? 324.0f : 274.0f;

        
        if (!self.fightFriendsViewController.view.hidden) {
            [UIView animateWithDuration:0.2f animations:^{
                self.fightFriendsViewController.view.center = CGPointMake(self.container.center.x, pos_y);
                self.container.center = CGPointMake(self.container.center.x, pos_y);
            }];
        } else {
            [UIView animateWithDuration:0.2f animations:^{
                self.container.center = CGPointMake(self.container.center.x, pos_y);
            }];
        }
    } else {
        self.filterVisibleState = kFiltersHidden;
        [self.filterToggle setImage:[UIImage imageNamed:@"FilterButtonInactive.png"] forState:UIControlStateNormal];
        
        float pos_y = IS_IPHONE_5 ? 276.0f : 224.0f;
        
        if (!self.fightFriendsViewController.view.hidden) {
            [UIView animateWithDuration:0.2f animations:^{
                self.fightFriendsViewController.view.center = CGPointMake(self.container.center.x, pos_y);
                self.container.center = CGPointMake(self.container.center.x, pos_y);
            }];
        } else {
            [UIView animateWithDuration:0.2f animations:^{
                self.container.center = CGPointMake(self.container.center.x, pos_y);
            }];
        }
    }
}

- (void)hideFilterPresentFindFriends
{
    [UIView animateWithDuration:0.2f animations:^{
        float pos_y = IS_IPHONE_5 ? 276.0f : 224.0f;
        self.container.center = CGPointMake(self.container.center.x, pos_y);
        
    } completion:^(BOOL finished) {
        float pos_y = IS_IPHONE_5 ? 276.0f : 224.0f;

        self.filterVisibleState = kFiltersHidden;
        [self.filterToggle setImage:[UIImage imageNamed:@"FilterButtonInactive.png"]
                           forState:UIControlStateNormal];

        self.dataTable.hidden = YES;
        self.locationMap.hidden = YES;
        self.fightFriendsViewController.view.hidden = NO;
        
        if (!self.fightFriendsViewController.view.hidden) {
            self.fightFriendsViewController.view.center = CGPointMake(self.container.center.x, pos_y);
        }
    }];
}

- (void)hideFilterPresentNearbyLocations
{
    [UIView animateWithDuration:0.2f animations:^{
        float pos_y = IS_IPHONE_5 ? 276.0f : 224.0f;
        
        self.container.center = CGPointMake(self.container.center.x, pos_y);
        if (!self.fightFriendsViewController.view.hidden) {
            self.fightFriendsViewController.view.hidden = YES;
        }
    } completion:^(BOOL finished) {
        self.filterVisibleState = kFiltersHidden;
        [self.filterToggle setImage:[UIImage imageNamed:@"FilterButtonInactive.png"]
                           forState:UIControlStateNormal];
        
        self.dataTable.hidden = NO;
        self.locationMap.hidden = NO;
    }];
}

- (IBAction)toggleFilterState:(id)sender
{
    if (sender == self.globalButton) {
        self.filterState = kFilterGlobal;
        [self.globalButton setImage:[UIImage imageNamed:@"GlobalFilterButtonActive.png"]
                           forState:UIControlStateNormal];
        [self.nearbyButton setImage:[UIImage imageNamed:@"NearbyFilterButtonInactive.png"]
                           forState:UIControlStateNormal];
        [self.friendsButton setImage:[UIImage imageNamed:@"FriendsFilterButtonInactive.png"]
                            forState:UIControlStateNormal];

        self.listMapToggleButton.userInteractionEnabled = NO;

    } else if (sender == self.nearbyButton) {
        self.filterState = kFilterNearBy;
        [self hideFilterPresentNearbyLocations];
        [self.globalButton setImage:[UIImage imageNamed:@"GlobalFilterButtonInactive.png"]
                           forState:UIControlStateNormal];
        [self.nearbyButton setImage:[UIImage imageNamed:@"NearbyFilterButtonActive.png"]
                           forState:UIControlStateNormal];
        [self.friendsButton setImage:[UIImage imageNamed:@"FriendsFilterButtonInactive.png"]
                            forState:UIControlStateNormal];
        
        self.listMapToggleButton.userInteractionEnabled = YES;
        self.listMapToggleButton.alpha = 1.0f;
    } else {
        self.filterState = kFilterFriends;
        [self hideFilterPresentFindFriends];
        
        [self.globalButton setImage:[UIImage imageNamed:@"GlobalFilterButtonInactive.png"]
                           forState:UIControlStateNormal];
        [self.nearbyButton setImage:[UIImage imageNamed:@"NearbyFilterButtonInactive.png"]
                           forState:UIControlStateNormal];
        [self.friendsButton setImage:[UIImage imageNamed:@"FriendsFilterButtonActive.png"]
                            forState:UIControlStateNormal];
        
        self.listMapToggleButton.userInteractionEnabled = NO;
        self.listMapToggleButton.alpha = 0.5f;
    }
    
    [self reloadData];
}


#pragma mark - Location Manager Actions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)locationsNearByRetrieved:(NSNotification *)notification
{    
    [TMLocation sharedInstance].currentSurroundingLocations = [[notification object] mutableCopy];
    [self plotLocations:[notification object]];
    
    [SVProgressHUD dismiss];
    [self reloadData];
}

- (void)locationsNearByRetrieveFail:(NSNotification *)notification
{
    [SVProgressHUD showErrorWithStatus:@"Nearby location retrieval failed"];
}

#pragma mark - Table Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_filterState == kFilterNearBy) {
        NSDictionary *locationData = [[TMLocation sharedInstance].currentSurroundingLocations objectAtIndex:indexPath.row];
        [[TMMainViewController sharedInstance] presentLocationWithData:locationData];
    }
}

#pragma mark - Table Datasource
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    if (_filterState == kFilterNearBy) {
        return [[[TMLocation sharedInstance] currentSurroundingLocations] count];
    } else if (_filterState == kFilterGlobal) {
        
    } else {
        return [_friendCharacters count];
    }
    
    return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_filterState == kFilterNearBy) {
        static NSString *CellIdentifier = @"LocationCell";
        TMFightListLocationCell *cell = (TMFightListLocationCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FightListLocationCell"
                                                                     owner:self
                                                                   options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell = (TMFightListLocationCell *)currentObject;
                    break;
                }
            }
        }
        
        // Configure the cell.
        NSDictionary *location = [[[TMLocation sharedInstance] currentSurroundingLocations] objectAtIndex:indexPath.row];
        cell.locationName.text = [location objectForKey:@"name"];
        
        if ([location objectForKey:@"boss_name"]) {
            cell.controlledByLabel.text = @"Controlled by";
            cell.bossName.hidden = NO;
            cell.bossName.text = [location objectForKey:@"boss_name"];
            cell.bossName.alpha = 1.0f;
        } else {
            cell.bossName.text = @"???";
            cell.bossName.alpha = 0.5f;
        }
        
        return cell;
    } else if (_filterState == kFilterFriends) {
        static NSString *CellIdentifier = @"FriendCell";
        TMFightListOpponentCell *cell = (TMFightListOpponentCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FightListOpponentCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell = (TMFightListOpponentCell *)currentObject;
                    break;
                }
            }
        }
        
        // Configure the cell.
        NSDictionary *friendCharacter = [_friendCharacters objectAtIndex:indexPath.row];
        cell.name.text = [friendCharacter objectForKey:@"name"];
        cell.level.text = [NSString stringWithFormat:@"Level %@", [friendCharacter objectForKey:@"level"]];
        
        return cell;
    }
    
    return nil;
}

#pragma mark - Plot Locations on Map
////////////////////////////////////////////////////////////////////////////////

- (void)plotLocations:(NSArray *)locations
{
    // Remove plot annotations
    for (id<MKAnnotation> annotation in self.locationMap.annotations) {
        [self.locationMap removeAnnotation:annotation];
    }
    
    NSArray *nearbyLocations = [TMLocation sharedInstance].currentSurroundingLocations;
    DebugLog(@"Total Locations: %d", [locations count]);
    
    for (NSDictionary *location in nearbyLocations) {
        double longitude = [[[location objectForKey:@"coords"] objectAtIndex:0] doubleValue];
        double latitude = [[[location objectForKey:@"coords"] objectAtIndex:1] doubleValue];
        NSString *locationName = [location objectForKey:@"name"];
        
        CLLocationCoordinate2D coord =  CLLocationCoordinate2DMake(latitude , longitude);
        TMLocationAnnotation *annotation = [[TMLocationAnnotation alloc] initWithName:locationName
                                                                           coordinate:coord];
        annotation.locationData = [location copy];
        [self.locationMap addAnnotation:annotation];
    }
    
    [self.locationMap doClustering];
}

#pragma mark - Map View Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (self.locationMap.hidden) {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 20, 20);
        [self.locationMap setRegion:region animated:YES];
        self.locationMap.showsUserLocation = YES;
    }
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated
{
    [self.locationMap removeOverlays:self.locationMap.overlays];
    [self.locationMap doClustering];
}

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    MKAnnotationView *annotationView;

    // if it's a cluster
    if ([annotation isKindOfClass:[OCAnnotation class]]) {
        
        OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation;
        
        annotationView = (MKAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
            annotationView.canShowCallout = YES;
            annotationView.centerOffset = CGPointMake(0, -20);
            annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];

        }
        
        // set title
        clusterAnnotation.title = [NSString stringWithFormat:@"%d Locations", [clusterAnnotation.annotationsInCluster count]];
        
        // set its image
        annotationView.image = [UIImage imageNamed:@"TMMapAnnotation.png"];
        
    } else if ([annotation isKindOfClass:[TMLocationAnnotation class]]){
            TMLocationAnnotation *singleAnnotation = (TMLocationAnnotation *)annotation;
            annotationView = (MKAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"singleAnnotationView"];
            if (!annotationView) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:singleAnnotation reuseIdentifier:@"singleAnnotationView"];
                annotationView.canShowCallout = YES;
                annotationView.centerOffset = CGPointMake(0, -20);
                annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            }
            singleAnnotation.title = annotation.title;
            annotationView.image = [UIImage imageNamed:@"TMMapAnnotation.png"];
    }

    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotation
                      calloutAccessoryControlTapped:(UIControl *)control
{
    if ([annotation.annotation isKindOfClass:[OCAnnotation class]]) {
        OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation.annotation;
        DebugLog(@"Annotations in Cluster: %d", [[clusterAnnotation annotationsInCluster] count]);
        [[TMMainViewController sharedInstance] presentLocationList:[clusterAnnotation annotationsInCluster]];
    } else  if ([annotation.annotation isKindOfClass:[TMLocationAnnotation class]]) {
        TMLocationAnnotation *singleAnnotation = (TMLocationAnnotation *)annotation.annotation;
        DebugLog(@"Single annotation: %@", singleAnnotation.locationData);
        [[TMMainViewController sharedInstance] presentLocationWithData:singleAnnotation.locationData];
    }
    
}


@end

