#import <UIKit/UIKit.h>

@interface TMCreateAccountViewController : UIViewController  <UITextFieldDelegate>

/** Username text field */
@property (nonatomic, strong) IBOutlet UITextField *usernameField;

/** Email text field */
@property (nonatomic, strong) IBOutlet UITextField *emailField;

/** Password text field */
@property (nonatomic, strong) IBOutlet UITextField *passwordField;

/** Password confirmation text field */
@property (nonatomic, strong) IBOutlet UITextField *passwordConfirmationField;

/** Move back to login view */
- (IBAction)backToLoginView:(id)sender;

/** Show the create account view */
- (IBAction)createAccount:(id)sender;

@end
