//
//  CreateAccountViewController.m
//  Termina
//
//  Created by Jason Lagaac on 7/20/13.
//  Copyright (c) 2013 mostlyserious. All rights reserved.
//

#import "TMCreateAccountViewController.h"
#import "TMViewHandlerController.h"
#import "TMUser.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface TMCreateAccountViewController ()

/** Original center position for 3.5 screens */
@property (nonatomic) CGPoint originalCenter;

@end

@implementation TMCreateAccountViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.originalCenter = self.view.center;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCreated:)
                                                 name:kUserCreated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCreationFailed:)
                                                 name:kUserCreateFailed
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userAuthenticated:)
                                                 name:kAuthenticated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userAuthenticationFailed:)
                                                 name:kAuthenticationFailed                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userRetrieved:)
                                                 name:kUserRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userRetrieveFailed:)
                                                 name:kUserRetrieveFailed
                                               object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    [self.emailField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)backToLoginView:(id)sender;
{
    self.usernameField.text = @"";
    self.passwordField.text = @"";
    self.emailField.text = @"";
    self.passwordConfirmationField.text = @"";
    
    [[TMViewHandlerController sharedInstance] presentLoginFromCreateAccount];
}

- (IBAction)createAccount:(id)sender
{
    if ([self.usernameField.text length] && [self.emailField.text length] && [self.passwordField.text length] && [self.passwordConfirmationField.text length]) {
        
        if ([self.passwordConfirmationField.text isEqualToString:self.passwordField.text]) {
            [[TMUser sharedInstance] createUser:self.usernameField.text
                                   withPassword:self.passwordField.text
                                          email:self.emailField.text];
        } else {
            [SVProgressHUD showErrorWithStatus:@"Please ensure that both password match"];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:@"Please complete all fields"];
    }
}

#pragma mark - User Creation API Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)userCreated:(NSNotification *)notification
{
    [TMUser sharedInstance].currentUserDetails = [notification object];
    [[TMUser sharedInstance] authenticateWithUsernameOrEmail:self.usernameField.text
                                                    password:self.passwordField.text];
}

- (void)userCreationFailed:(NSNotification *)notification
{
    NSString *message  = [[notification object] objectForKey:@"message"];
    message = [message stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                               withString:[[message substringToIndex:1] uppercaseString]];
    [SVProgressHUD showErrorWithStatus:message];
}

#pragma mark - User Login API Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)userAuthenticated:(NSNotification *)notification
{
    [TMUser sharedInstance].authDetails = [[notification object] copy];
    [[TMUser sharedInstance] getUser:self.usernameField.text];
}


- (void)userAuthenticationFailed:(NSNotification *)notification
{
    NSString *message  = [[notification object] objectForKey:@"message"];
    message = [message stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                               withString:[[message substringToIndex:1] uppercaseString]];
    [SVProgressHUD showErrorWithStatus:message];
}

#pragma mark - User Retrieved Notification Handlers
////////////////////////////////////////////////////////////////////////////////
- (void)userRetrieved:(NSNotification *)notification
{
    [[TMViewHandlerController sharedInstance] presentCharacterSelect];
}

- (void)userRetrieveFailed:(NSNotification *)notification
{
    
}


#pragma mark - Touch Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];

    if (([self.emailField isFirstResponder] && touch.view != self.emailField) ||
        ([self.usernameField isFirstResponder] && touch.view != self.usernameField) ||
        ([self.passwordField isFirstResponder] && touch.view != self.passwordField) ||
        ([self.passwordConfirmationField isFirstResponder] && touch.view != self.passwordConfirmationField)) {
        
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.view setCenter:self.originalCenter];
                         }];
    
        [self.emailField resignFirstResponder];
    }
    
    
    if (self.usernameField != [touch view] && self.passwordConfirmationField != [touch view] &&
        self.passwordField != [touch view] && self.emailField != [touch view]) {
        
        [self.usernameField resignFirstResponder];
        [self.passwordField resignFirstResponder];
        [self.emailField resignFirstResponder];
        [self.passwordConfirmationField resignFirstResponder];
        
    }
    
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - Text Field Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)textFieldDidBeginEditing:(UITextField *)textField
{    
    if (IS_IPHONE_5) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.view setCenter:CGPointMake(self.view.center.x, self.originalCenter.y - 40)];
                         }];
    } else {
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.view setCenter:CGPointMake(self.view.center.x, self.originalCenter.y - 90)];
                         }];
    }
}

@end
