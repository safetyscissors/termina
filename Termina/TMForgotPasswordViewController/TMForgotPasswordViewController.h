#import <UIKit/UIKit.h>

@interface TMForgotPasswordViewController : UIViewController <UITextFieldDelegate>

/** Username Field */
@property (nonatomic, strong) IBOutlet UITextField *usernameField;

/** Back to login view actions */
- (IBAction)backToLoginView:(id)sender;

@end
