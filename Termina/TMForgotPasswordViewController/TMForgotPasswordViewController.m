#import "TMForgotPasswordViewController.h"
#import "TMViewHandlerController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "TMUser.h"

@interface TMForgotPasswordViewController ()

/** Original center point for 3.5 inch screens */
@property (nonatomic) CGPoint originalCenter;

@end

@implementation TMForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(passwordReset:)
                                                 name:kPasswordReset
                                               object:nil];
    
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(passwordResetFail:)
                                                 name:kPasswordResetFailed
                                               object:nil];
    
    self.originalCenter = self.view.center;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////
- (IBAction)resetPasswordAction:(id)sender
{
    if ([self.usernameField.text length]) {
        [SVProgressHUD showWithStatus:@"Resetting password"];
        [self.usernameField resignFirstResponder];
        
        [[TMUser sharedInstance] forgotPasswordForUsernameOrEmail:self.usernameField.text];
    } else {
        [SVProgressHUD showErrorWithStatus:@"Please enter a username"];
    }
}


- (IBAction)backToLoginView:(id)sender;
{
    [self.usernameField resignFirstResponder];
    [[TMViewHandlerController sharedInstance] presentLoginFromForgotPassword];
}


#pragma mark - Password Reset Responder Actions
////////////////////////////////////////////////////////////////////////////////

- (void)passwordReset:(NSNotification *)notification
{
    [SVProgressHUD showSuccessWithStatus:@"Password reset"];
}

- (void)passwordResetFail:(NSNotification *)notification
{
    [SVProgressHUD showErrorWithStatus:@"Failed to reset password"];
}

#pragma mark - Touch Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    // Shift the view up if 3.5 inch device.
    if ([self.usernameField isFirstResponder] && [touch view] != self.usernameField) {
        if (!IS_IPHONE_5) {
            [UIView animateWithDuration:0.15
                             animations:^{
                                 [self.view setCenter:self.originalCenter];
                             }];
        }
        [self.usernameField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - UITextFieldDelegates Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    DebugLog(@"%@", [[ UIDevice currentDevice] model]);
    
    // Shift view back to original position
    if (!IS_IPHONE_5) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.view setCenter:CGPointMake(self.view.center.x, self.originalCenter.y - 30)];
                         }];
    }
}

@end
