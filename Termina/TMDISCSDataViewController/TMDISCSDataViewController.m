#import "TMDISCSDataViewController.h"
#import "TMCharacterLevelView.h"
#import "TMUserCharacter.h"
#import "TMProgressIndicator.h"

@interface TMDISCSDataViewController ()

/** Character Level View */
@property (nonatomic, strong) TMCharacterLevelView *characterLevelView;

/** Health Indicator  */
@property (nonatomic, strong) TMProgressIndicator *healthIndicator;

/** Strength Indicator  */
@property (nonatomic, strong) TMProgressIndicator *strengthIndicator;

/** Attack Indicator  */
@property (nonatomic, strong) TMProgressIndicator *attackIndicator;

/** Defense Indicator  */
@property (nonatomic, strong) TMProgressIndicator *defenseIndicator;

@end

@implementation TMDISCSDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
 
    [self loadCharacterLevelIndicator];
    [self loadUserCharacterData];
    [self loadAttributeIndicators];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadUserCharacterData];
    [self loadAttributeIndicators];
    
    if (![TMUserCharacter sharedInstance].currentAttributePoints) {
        [self disableAllAttributeButtons];
    } else {
        [self loadAttributeButtons];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCharacterUpdated:)
                                                 name:kUserCharacterUpdated
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unloadAttributeIndicators];
}


#pragma mark - Load Actions
////////////////////////////////////////////////////////////////////////////////

- (void)loadCharacterLevelIndicator
{
    self.characterLevelView = [[TMCharacterLevelView alloc] init];
    [self.characterLevelView.characterImage setImage:[UIImage imageNamed:@"MainCharacterIconSample.png"]];
    [self.view addSubview:self.characterLevelView];
    
    if (IS_IPHONE_5) {
        self.characterLevelView.center = CGPointMake(80.0f, 86.0f);
    } else {
        self.characterLevelView.center = CGPointMake(80.0f, 74.0f);
    }
    
    NSInteger characterLevel = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"level"] integerValue];
    self.characterLevelView.levelLabel.text = [NSString stringWithFormat:@"Level %d", characterLevel];
    
}

 - (void)loadUserCharacterData
{
    self.userCharacterName.text = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"name"];
    
    NSInteger skillPointsAvailable = [TMUserCharacter sharedInstance].currentAttributePoints;
    self.skillPointsLabel.text = [NSString stringWithFormat:@"You have %d skill points to use", skillPointsAvailable];
    
    // Set health label
    NSInteger healthValue = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"health"] integerValue];
    self.healthLabel.text = [NSString stringWithFormat:@"%d/1000 HEALTH", healthValue];
    
    // Set strength label
    NSInteger strengthValue = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"strength"] integerValue];
    self.strengthLabel.text = [NSString stringWithFormat:@"%d/1000 STRENGTH", strengthValue];
    
    // Set attack label
    NSInteger attackValue = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"attack_radius"] integerValue];
    self.attackLabel.text = [NSString stringWithFormat:@"%d/1000 ATTACK", attackValue];
    
    // Set defense label
    NSInteger defenseValue = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"defense_radius"] integerValue];
    self.defenseLabel.text = [NSString stringWithFormat:@"%d/1000 DEFENSE", defenseValue];
}

#pragma mark - Attribute Indicators
////////////////////////////////////////////////////////////////////////////////

- (void)loadAttributeIndicators
{
    if (IS_IPHONE_5) {
        [self loadAttributeIndicatorsFor4inch];
    } else {
        [self loadAttributeIndicatorsfor3inch];
    }
}

- (void)unloadAttributeIndicators
{
    [self.healthIndicator removeFromSuperview];
    [self.strengthIndicator removeFromSuperview];
    [self.attackIndicator removeFromSuperview];
    [self.defenseIndicator removeFromSuperview];
    
    self.healthIndicator = nil;
    self.strengthIndicator = nil;
    self.attackIndicator = nil;
    self.defenseIndicator = nil;
}

- (void)loadAttributeIndicatorsFor4inch
{
    // Load health indicator
    float healthLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"health"] floatValue];
    self.healthIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(68.0f, 160.0f, 228.0f, 28.0f)
                                                             progress:healthLevel / 1000 * 100
                                                                 type:TMProgressBarProgress];
    self.healthIndicator.alpha = 0.6f;
    [self.view insertSubview:self.healthIndicator
                belowSubview:self.healthLabel];
    
    // Load strength indicator
    float strengthLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"strength"] floatValue];
    self.strengthIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(68.0f, 200.0f, 228.0f, 28.0f)
                                                               progress:strengthLevel / 1000 * 100
                                                                   type:TMProgressBarProgress];
    self.strengthIndicator.alpha = 0.6f;
    [self.view insertSubview:self.strengthIndicator
                belowSubview:self.strengthLabel];
    
    // Load attack indicator
    float attackLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"attack_radius"] floatValue];
    self.attackIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(68.0f, 238.0f, 228.0f, 28.0f)
                                                             progress:attackLevel / 1000 * 100
                                                                 type:TMProgressBarProgress];
    self.attackIndicator.alpha = 0.6f;
    [self.view insertSubview:self.attackIndicator
                belowSubview:self.attackLabel];
    
    // Defense Indicator
    float defenseLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"defense_radius"] floatValue];
    self.defenseIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(68.0f, 276.0f, 228.0f, 28.0f)
                                                             progress:defenseLevel / 1000 * 100
                                                                 type:TMProgressBarProgress];
    self.defenseIndicator.alpha = 0.6f;
    [self.view insertSubview:self.defenseIndicator
                belowSubview:self.defenseLabel];
}

- (void)loadAttributeIndicatorsfor3inch
{
    // Load health indicator
    float healthLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"health"] floatValue];
    self.healthIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(65.0f, 135.0f, 228.0f, 28.0f)
                                                             progress:healthLevel / 1000 * 100
                                                                 type:TMProgressBarProgress];
    self.healthIndicator.alpha = 0.4f;
    [self.view insertSubview:self.healthIndicator
                belowSubview:self.healthLabel];
    
    // Load strength indicator
    float strengthLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"strength"] floatValue];
    self.strengthIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(65.0f, 175.0f, 228.0f, 28.0f)
                                                               progress:strengthLevel / 1000 * 100
                                                                   type:TMProgressBarProgress];
    self.strengthIndicator.alpha = 0.4f;
    [self.view insertSubview:self.strengthIndicator
                belowSubview:self.strengthLabel];
    
    // Load attack indicator
    float attackLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"attack_radius"] floatValue];
    self.attackIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(65.0f, 213.0f, 228.0f, 28.0f)
                                                             progress:attackLevel / 1000 * 100
                                                                 type:TMProgressBarProgress];
    self.attackIndicator.alpha = 0.4f;
    [self.view insertSubview:self.attackIndicator
                belowSubview:self.attackLabel];
    
    // Defense Indicator
    float defenseLevel = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"defense_radius"] floatValue];
    self.defenseIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(65.0f, 251.0f, 228.0f, 28.0f)
                                                              progress:defenseLevel / 1000 * 100
                                                                  type:TMProgressBarProgress];
    self.defenseIndicator.alpha = 0.4f;
    [self.view insertSubview:self.defenseIndicator
                belowSubview:self.defenseLabel];
}

- (void)loadAttributeButtons
{
    if ([[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"health"] integerValue] >= 1000) {
        self.addHealthButton.userInteractionEnabled = NO;
        self.addHealthButton.alpha = 0.6f;
    }
    
    if ([[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"strength"] integerValue] >= 1000) {
        self.addStrengthButton.userInteractionEnabled = NO;
        self.addStrengthButton.alpha = 0.6f;
    }
    
    if ([[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"attack_radius"] integerValue] >= 1000) {
        self.addAttackButton.userInteractionEnabled = NO;
        self.addAttackButton.alpha = 0.6f;
    }
    
    if ([[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"defense_radius"] integerValue] >= 1000) {
        self.addDefenseButton.userInteractionEnabled = NO;
        self.addDefenseButton.alpha = 0.6f;
    }
}

- (void)disableAllAttributeButtons
{
    self.addAttackButton.userInteractionEnabled = NO;
    self.addAttackButton.alpha = 0.6f;
    
    self.addDefenseButton.userInteractionEnabled = NO;
    self.addDefenseButton.alpha = 0.6f;
    
    self.addHealthButton.userInteractionEnabled = NO;
    self.addHealthButton.alpha = 0.6f;
    
    self.addStrengthButton.userInteractionEnabled = NO;
    self.addStrengthButton.alpha = 0.6f;
}

#pragma mark - Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)userCharacterUpdated:(NSNotification *)notification
{
    DebugLog(@"User character updated: %@", [notification object]);
}

#pragma mark - Add item button actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)addAttributePoint:(id)sender
{
    if ([sender isEqual:self.addHealthButton]) {
        // Health attribute added
        NSInteger health = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"health"] integerValue];
        [TMUserCharacter sharedInstance].currentAttributePoints--;
        health++;
        
        [self.healthIndicator reduceBarToProgressLevel:health];
        [[TMUserCharacter sharedInstance].currentAttributes setValue:[NSNumber numberWithInt:health]
                                                              forKey:@"health"];
    }
    
    if ([sender isEqual:self.addStrengthButton]) {
        // Strength attribute added
        NSInteger strength = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"strength"] integerValue];
        [TMUserCharacter sharedInstance].currentAttributePoints--;
        strength++;
        
        [self.strengthIndicator reduceBarToProgressLevel:strength];
        [[TMUserCharacter sharedInstance].currentAttributes setValue:[NSNumber numberWithInt:strength]
                                                              forKey:@"strength"];
    }
    
    if ([sender isEqual:self.addDefenseButton]) {
        // Defense attribute added
        NSInteger defense = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"defense_radius"] integerValue];
        [TMUserCharacter sharedInstance].currentAttributePoints--;
        defense++;
        
        [self.defenseIndicator reduceBarToProgressLevel:defense];
        [[TMUserCharacter sharedInstance].currentAttributes setValue:[NSNumber numberWithInt:defense]
                                                              forKey:@"defense_radius"];
    }
    
    if ([sender isEqual:self.addAttackButton]) {
        // Attack attribute added
        NSInteger attack = [[[TMUserCharacter sharedInstance].currentAttributes objectForKey:@"attack_radius"] integerValue];
        [TMUserCharacter sharedInstance].currentAttributePoints--;
        attack++;
        
        [self.attackIndicator reduceBarToProgressLevel:attack];
        [[TMUserCharacter sharedInstance].currentAttributes setValue:[NSNumber numberWithInt:attack]
                                                              forKey:@"attack_radius"];
    }
    
    if ([TMUserCharacter sharedInstance].currentAttributePoints) {
        [self loadAttributeButtons];
    } else {
        [self disableAllAttributeButtons];
    }
    
    [self loadUserCharacterData];
    NSString *currentUserCharacterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    NSDictionary *userCharacterAttributes = [TMUserCharacter sharedInstance].currentAttributes;
    NSInteger currentAttributePoints = [TMUserCharacter sharedInstance].currentAttributePoints;
    
    NSDictionary *updatedParameters = @{ @"attributes":userCharacterAttributes,
                                         @"attribute_points":[NSNumber numberWithInt:currentAttributePoints] };
    
    [[TMUserCharacter sharedInstance] updateCharacter:currentUserCharacterID
                                           withParams:updatedParameters];
    
}



@end
