//
//  TMDISCSDataViewController.h
//  Termina
//
//  Created by Jason Lagaac on 27/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMDISCSDataViewController : UIViewController

/** User character name */
@property (nonatomic, strong) IBOutlet UILabel *userCharacterName;

/** Skill Points Label */
@property (nonatomic, strong) IBOutlet UILabel *skillPointsLabel;

/** Health Label */
@property (nonatomic, strong) IBOutlet UILabel *healthLabel;

/** Strength Label */
@property (nonatomic, strong) IBOutlet UILabel *strengthLabel;

/** Attack Label */
@property (nonatomic, strong) IBOutlet UILabel *attackLabel;

/** Defense Label */
@property (nonatomic, strong) IBOutlet UILabel *defenseLabel;

/** Health Button */
@property (nonatomic, strong) IBOutlet UIButton *addHealthButton;

/** Strength Button */
@property (nonatomic, strong) IBOutlet UIButton *addStrengthButton;

/** Attack Button */
@property (nonatomic, strong) IBOutlet UIButton *addAttackButton;

/** Defense Button */
@property (nonatomic, strong) IBOutlet UIButton *addDefenseButton;

@end
