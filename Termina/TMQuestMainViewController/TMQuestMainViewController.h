#import <UIKit/UIKit.h>

@class TMQuestProgressIndicator;

@interface TMQuestMainViewController : UIViewController

/** Quest Progress Lines */
@property (nonatomic, strong) IBOutlet UIImageView *questProgressLines;

/** Back to main */
- (IBAction)backToCentral:(id)sender;

@end