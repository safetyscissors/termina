//
//  QuestMainViewController.m
//  Termina
//
//  Created by Jason Lagaac on 7/22/13.
//  Copyright (c) 2013 mostlyserious. All rights reserved.
//

#import "TMQuestMainViewController.h"
#import "TMViewHandlerController.h"
#import "TMProgressIndicator.h"

@interface TMQuestMainViewController ()

@property (nonatomic, strong) TMProgressIndicator *progressIndicator;

@end

@implementation TMQuestMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadQuestProgressIndicator];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Quest Progress Indicator
////////////////////////////////////////////////////////////////////////////////

- (void)loadQuestProgressIndicator
{
    CGFloat pos_y = self.view.frame.size.height - 43.0f;
    self.progressIndicator = [[TMProgressIndicator alloc] initWithFrame:CGRectMake(9, pos_y, 0, 19)
                                                               progress:80.0f
                                                                   type:TMProgressBarQuestProgress];
    self.progressIndicator.alpha = 0.6f;
    [self.view insertSubview:self.progressIndicator
                belowSubview:self.questProgressLines];
}


#pragma mark - Back to central interface action
////////////////////////////////////////////////////////////////////////////////

- (IBAction)backToCentral:(id)sender
{
    [[TMViewHandlerController sharedInstance] presentMainView];
}

@end
