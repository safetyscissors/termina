//
//  TMCheckin.h
//  Termina
//
//  Created by Jason Lagaac on 18/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMBaseDataModel.h"
#import <CoreLocation/CoreLocation.h>

@interface TMCheckin : TMBaseDataModel

/** List of current surrounding locations */
@property (nonatomic, strong) NSMutableArray *currentLocationCheckins;

/** Current Checkin */
@property (nonatomic, strong) NSDictionary *currentCheckin;

/** Shared instance of the user object */
+ (TMCheckin *)sharedInstance;

/** Create a checkin */
- (void)createCheckinAtLocation:(NSString *)locationID
                    coordinates:(CLLocation *)location
                  userCharacter:(NSString *)userCharacterID;

/** Running Checkins for User Character */
- (void)getRunningCheckinsForUserCharacter:(NSDictionary *)userCharacter
                                atLocation:(NSString *)locationID;


/** Checkins for current location */
- (void)getCheckinsForLocation:(NSString *)locationID;

/** Get Checkins for Player */
- (void)getPlayerCheckin:(NSString *)checkinID;

/** Get Opponent Checkin */
- (void)getPlayerCheckin:(NSString *)checkinID;

/** Reset data */
- (void)reset;


@end
