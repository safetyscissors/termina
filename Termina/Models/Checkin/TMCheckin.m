//
//  TMCheckin.m
//  Termina
//
//  Created by Jason Lagaac on 18/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMCheckin.h"
#import "TMUser.h"
#import "TMCustomRequest.h"

@implementation TMCheckin

/** Shared instance of the user object */
+ (TMCheckin *)sharedInstance
{
    static TMCheckin *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        
    }
    
    return self;
}

- (void)reset
{
    self.currentLocationCheckins = nil;
}

#pragma mark - API Actions
////////////////////////////////////////////////////////////////////////////////

- (void)getCheckinsForLocation:(NSString *)locationID
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/locations/%@/checkins",  locationID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    
    NSMutableURLRequest *request = customJSONRequest(url, @"GET", nil, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kCheckinsForLocationRetrived
                 failNotification:kCheckinsForLocationRetriveFailed];
}

- (void)getRunningCheckinsForUserCharacter:(NSDictionary *)userCharacter
                               atLocation:(NSString *)locationID
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/checkins"];
    
    NSDictionary *params = @{ @"user_id" : [userCharacter objectForKey:@"_user"],
                              @"user_character_id" : [userCharacter objectForKey:@"_id"],
                              @"state" : @"running",
                              @"l" : locationID };
    
    NSMutableURLRequest *request = customRequestWithQueryString(url, @"GET", params, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kCheckinsForUserRetrieved
                 failNotification:kCheckinsForUserRetrieveFailed];
}

- (void)createCheckinAtLocation:(NSString *)locationID
                    coordinates:(CLLocation *)location
                  userCharacter:(NSString *)userCharacterID
{    
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/checkins"];
    NSDictionary *params = @{@"latitude" : [NSNumber numberWithFloat:location.coordinate.latitude],
                             @"longitude" : [NSNumber numberWithFloat:location.coordinate.longitude],
                             @"_user_character" : userCharacterID,
                             @"_location" : locationID};
                             
    
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", params, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kCheckinCreated
                 failNotification:kCheckinCreateFailed];
}
@end

