#import "TMUserCharacter.h"
#import "TMUser.h"
#import "TMURLEncoding.h"
#import "TMCustomRequest.h"
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@interface TMUserCharacter ()
@property (nonatomic) BOOL loadingActiveCharacter;
@end

@implementation TMUserCharacter

+ (TMUserCharacter *)sharedInstance {
    
    static TMUserCharacter *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init {
    
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(userCharacterRetrieved:)
                                                     name:kUserCharacterRetrieved
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(userCharacterRetrieveFailed:)
                                                     name:kUserCharacterRetrieveFailed
                                                   object:nil];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Get User Character
////////////////////////////////////////////////////////////////////////////////

- (void)getUserCharacter:(NSString *)userCharacterID
{
    NSString *requestURL = [NSString stringWithFormat:@"/userCharacters/%@",userCharacterID];
    NSString *url = [kBaseURL stringByAppendingString:requestURL];
    NSMutableURLRequest *request = customRequest(url, @"GET", nil, [TMUser sharedInstance].authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserCharacterRetrieved
                     failNotification:kUserCharacterRetrieveFailed];
}

#pragma mark - Get Current User's Characters
////////////////////////////////////////////////////////////////////////////////
- (void)getUserCharacters
{
    NSString *url = [kBaseURL stringByAppendingString:@"/userCharacters"];
    NSMutableURLRequest *request = customRequest(url, @"GET", nil, [TMUser sharedInstance].authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserCharacterRetrieved
                     failNotification:kUserCharacterRetrieveFailed];
}


#pragma mark - Create User Character
////////////////////////////////////////////////////////////////////////////////

- (void)createUserCharacter:(NSString *)characterType
                   withName:(NSString *)characterName
{
    NSString *url = [kBaseURL stringByAppendingString:@"/userCharacters"];
    NSDictionary *characterParams = @{ @"_character" : characterType,
                                       @"name" : characterName };
        
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", characterParams, [TMUser sharedInstance].authDetails);
    [self runJSONOperationWithRequest:request
                  successNotification:kUserCharacterCreated
                     failNotification:kUserCharacterCreateFailed];
}

#pragma mark - Create User Character
////////////////////////////////////////////////////////////////////////////////

- (void)updateCharacter:(NSString *)userCharacterID
             withParams:(NSDictionary *)params
{
    NSString *url = [NSString stringWithFormat:@"/userCharacters/%@",userCharacterID];
    NSString *requestURL = [kBaseURL stringByAppendingString:url];
    NSMutableURLRequest *request = customJSONRequest(requestURL, @"PUT", params, [TMUser sharedInstance].authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserCharacterUpdated
                     failNotification:kUserCharacterUpdateFailed];
}

#pragma mark - UserCharacter Retriveal Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)loadActiveCharacter
{
    self.loadingActiveCharacter = YES;
    NSString *activeCharacter = [[[TMUser sharedInstance] currentUserDetails] objectForKey:@"_active_character"];
    [self getUserCharacter:activeCharacter];
}


#pragma mark - Add item to user character
////////////////////////////////////////////////////////////////////////////////
- (void)addItem:(NSDictionary *)item toUserCharacter:(NSString *)userCharacterID
{
    NSString *url = [NSString stringWithFormat:@"/userCharacters/%@/items/%@", userCharacterID, [item objectForKey:@"_id"]];
    NSString *requestURL = [kBaseURL stringByAppendingString:url];
    NSMutableURLRequest *request = customRequest(requestURL, @"POST", nil, [TMUser sharedInstance].authDetails);
    
    self.lastPurchasedItem = [item copy];
    
    [self runJSONOperationWithRequest:request
                  successNotification:kItemAddedToUserCharacter
                     failNotification:kItemAddToUserCharacterFailed];
}

#pragma mark - Delete item to user character
////////////////////////////////////////////////////////////////////////////////
- (void)deleteItem:(NSString *)itemID toUserCharacter:(NSString *)userCharacterID
{
    NSString *url = [NSString stringWithFormat:@"/userCharacters/%@/items/%@", userCharacterID, itemID];
    NSMutableURLRequest *request = customRequest(url, @"DELETE", nil, [TMUser sharedInstance].authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kItemDeletedFromUserCharacter
                     failNotification:kItemDeleteFromUserCharacterFailed];
}

#pragma mark - Equip item to user character
////////////////////////////////////////////////////////////////////////////////
- (void)equipItem:(NSString *)itemID toUserCharacter:(NSString *)userCharacterID
{
    NSString *url = [NSString stringWithFormat:@"/userCharacters/%@/equipped_items/%@", userCharacterID, itemID];
    url = [kBaseURL stringByAppendingString:url];

    NSMutableURLRequest *request = customRequest(url, @"POST", nil, [TMUser sharedInstance].authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kItemEquippedToUserCharacter
                     failNotification:kItemEquipToUserCharacterFailed];
}

#pragma mark - Equip item to user character
////////////////////////////////////////////////////////////////////////////////
- (void)unequipItem:(NSString *)itemID toUserCharacter:(NSString *)userCharacterID
{
    NSString *url = [NSString stringWithFormat:@"/userCharacters/%@/equipped_items/%@", userCharacterID, itemID];
    url = [kBaseURL stringByAppendingString:url];

    NSMutableURLRequest *request = customRequest(url, @"DELETE", nil, [TMUser sharedInstance].authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kItemUnequippedFromUserCharacter
                     failNotification:kItemUnequipFromUserCharacterFailed];
}


#pragma mark - UserCharacter Retriveal Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)userCharacterRetrieved:(NSNotification *)notification
{
    if (self.loadingActiveCharacter)
    {
        self.loadingActiveCharacter = NO;
        self.activeCharacter = [[notification object] mutableCopy];
        self.currentAttributePoints = [[[notification object] objectForKey:@"attribute_points"] integerValue];
        self.currentAttributes = [[[notification object] objectForKey:@"attributes"] mutableCopy];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kActiveUserCharacterLoaded
                                                            object:[notification object]];
    }
}

- (void)userCharacterRetrieveFailed:(NSNotification *)notification
{
    if (self.loadingActiveCharacter)
    {
        self.loadingActiveCharacter = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kActiveUserCharacterLoadFailed
                                                            object:nil];
    }
}





@end
