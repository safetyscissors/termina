#import <Foundation/Foundation.h>
#import "TMBaseDataModel.h"

@interface TMUserCharacter : TMBaseDataModel

/** Current active character */
@property (nonatomic, strong) NSMutableDictionary *activeCharacter;

/** Last purchased item */
@property (nonatomic, strong) NSDictionary *lastPurchasedItem;

/** Current available attribute points */
@property (nonatomic) NSInteger currentAttributePoints;

/** Current attributes */
@property (nonatomic) NSMutableDictionary *currentAttributes;

+ (TMUserCharacter *)sharedInstance;

/** Retrieve user character */
- (void)getUserCharacter:(NSString *)userCharacterID;

/** Retrieve all user characters */
- (void)getUserCharacters;

/** Create a new user character */
- (void)createUserCharacter:(NSString *)characterType
                   withName:(NSString *)characterName;

/** Update user character with details */
- (void)updateCharacter:(NSString *)userCharacterID
             withParams:(NSDictionary *)params;

/** Load the active character */
- (void)loadActiveCharacter;

/** Add item to user character */
- (void)addItem:(NSDictionary *)item toUserCharacter:(NSString *)userCharacterID;

/** Delete item from user character */
- (void)deleteItem:(NSString *)itemID toUserCharacter:(NSString *)userCharacterID;

/** Equip item to user character */
- (void)equipItem:(NSString *)itemID toUserCharacter:(NSString *)userCharacterID;

/** Unequip item from user character */
- (void)unequipItem:(NSString *)itemID toUserCharacter:(NSString *)userCharacterID;

@end
