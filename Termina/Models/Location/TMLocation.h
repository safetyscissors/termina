#import "TMBaseDataModel.h"
#import <CoreLocation/CoreLocation.h>

@interface TMLocation : TMBaseDataModel <CLLocationManagerDelegate>

/** Current Location */
@property (nonatomic, strong) CLLocation *currentLocation;

/** Current Surrounding Locations */
@property (nonatomic, strong) NSArray *currentSurroundingLocations;

/** Shared instance of the user object */
+ (TMLocation *)sharedInstance;

/** Reset action */
- (void)reset;

/** Get a specific location from ID */
- (void)getLocation:(NSString *)locationID;

/** Get locations within a specified area */
- (void)getLocationsWithin:(NSInteger)radius;

/** Update a location within a specified area */
- (void)updateLocation:(NSString *)locationID
            parameters:(NSDictionary *)params;

/** Start monitoring the user's location */
- (void)startMonitoringLocation;

/** Stop monitoring the user's location */
- (void)stopMonitoringLocation;

@end
