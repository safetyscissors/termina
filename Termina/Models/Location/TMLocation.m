#import "TMLocation.h"
#import "TMUser.h"
#import "TMURLEncoding.h"
#import "TMCustomRequest.h"
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@interface TMLocation ()

/** Core Location Manager */
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation TMLocation

/** Shared instance of the user object */
+ (TMLocation *)sharedInstance
{
    static TMLocation *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = 5.0f;
        self.locationManager.delegate = self;
        self.locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        self.currentLocation = [[CLLocation alloc] initWithLatitude:0.0f longitude:0.0f];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(nearbyLocationsRetrieved:)
                                                     name:kLocationsWithinRadiusRetrieved
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(nearbyLocationsRetrieveFailed:)
                                                     name:kLocationsWithinRadiusRetrieveFailed
                                                   object:nil];
    }
    
    return self;
}


- (void)reset
{
    self.currentSurroundingLocations = nil;
}

#pragma mark - Location Actions
////////////////////////////////////////////////////////////////////////////////

- (void)startMonitoringLocation
{
    self.currentLocation = [[CLLocation alloc] initWithLatitude:0.0f longitude:0.0f];
    [self.locationManager startUpdatingLocation];
}


- (void)stopMonitoringLocation
{
    [self.locationManager stopUpdatingLocation];
}


#pragma mark - Location API Actions
////////////////////////////////////////////////////////////////////////////////

// Location Actions
- (void)getLocation:(NSString *)locationID
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/locations/%@",  locationID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    
    NSMutableURLRequest *request = customJSONRequest(url, @"GET", nil, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kLocationRetrieved
                 failNotification:kLocationRetrieveFailed];
    
}

- (void)getLocationsWithin:(NSInteger)radius
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/locations/near"];
    
    double latitude = self.currentLocation.coordinate.latitude;
    double longitude = self.currentLocation.coordinate.longitude;
    
    NSString *coords = [NSString stringWithFormat:@"%f,%f", longitude, latitude];
    NSMutableURLRequest *request = customRequestWithQueryString(url, @"GET", @{ @"radius" : [NSNumber numberWithInt:radius], @"coords" :  coords}, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kLocationsWithinRadiusRetrieved
                 failNotification:kLocationsWithinRadiusRetrieveFailed];
}

- (void)updateLocation:(NSString *)locationID
            parameters:(NSDictionary *)params
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/locations/%@",  locationID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    
    NSMutableURLRequest *request = customJSONRequest(url, @"PUT", params, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kLocationUpdated
                 failNotification:kLocationUpdateFailed];
}

#pragma mark - Core Location Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    if (abs(howRecent) < 60.0f && ([self.currentLocation distanceFromLocation:location] > 100.0f)) {
        self.currentLocation = location;
        [self getLocationsWithin:500];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLocationChanged
                                                            object:self.currentLocation];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationChangeFailed
                                                        object:self.currentLocation];
    
}

#pragma mark - Nearby Locations Retrieved Handler
////////////////////////////////////////////////////////////////////////////////

- (void)nearbyLocationsRetrieved:(NSNotification *)notification
{
    // Sort locations by name
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                   ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.currentSurroundingLocations = [[notification object] sortedArrayUsingDescriptors:sortDescriptors];
    
    DebugLog(@"Current Surrounding Locations: %@", self.currentSurroundingLocations);
}

- (void)nearbyLocationsRetrieveFailed:(NSNotification *)notification
{
    
}


@end
