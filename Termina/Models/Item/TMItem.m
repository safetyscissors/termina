#import "TMItem.h"
#import "TMUser.h"
#import "TMURLEncoding.h"
#import "TMCustomRequest.h"

@implementation TMItem

/** Singleton Instance */
+ (TMItem *)sharedInstance
{
    static TMItem *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    if (self = [super init])
    {
        self.storeItems = [self loadItems];
    }
    
    return self;
}


#pragma mark - API Actions
////////////////////////////////////////////////////////////////////////////////

/** Get items */
- (void)getItemsForType:(NSString *)area
           purchaseable:(BOOL)purchaseable
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/items"];
    NSDictionary *params = @{ @"type" : area,
                              @"purchaseable" : [NSNumber numberWithBool:purchaseable] };
    
    NSMutableURLRequest *request = customRequestWithQueryString(url, @"GET", params, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kItemsForTypeRetrieved
                     failNotification:kItemsForTypeRetrieveFailed];
}

/** Get an item */
- (void)getItem:(NSString *)itemID
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:[NSString stringWithFormat:@"/items/%@",itemID]];
    NSMutableURLRequest *request = customRequestWithQueryString(url, @"GET", nil, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kItemRetrieved
                     failNotification:kItemRetrieveFailed];
}

- (void)getItems
{
    DebugLog(@"Retrieving items");
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/items"];
    NSMutableURLRequest *request = customRequestWithQueryString(url, @"GET", nil, authData);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(itemsRetrieved:)
                                                 name:kItemsRetrieved
                                               object:nil];
    
    [self runJSONOperationWithRequest:request
                  successNotification:kItemsRetrieved
                     failNotification:kItemsRetrieveFailed];
}

#pragma mark - Notification handlers
////////////////////////////////////////////////////////////////////////////////

- (void)itemsRetrieved:(NSNotification *)notification
{
    self.storeItems = [[notification object] copy];
    
    DebugLog(@"Store items retrieved: %@", self.storeItems);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"store_items.dat"];
    [self.storeItems writeToFile:filePath atomically:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)loadItems
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"store_items.dat"];
    
    return [NSArray arrayWithContentsOfFile:filePath];
}



@end
