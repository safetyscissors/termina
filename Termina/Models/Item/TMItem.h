#import "TMBaseDataModel.h"

@interface TMItem : TMBaseDataModel

/** Array of store items */
@property (nonatomic, strong) NSArray *storeItems;

/** Singleton Instance */
+ (TMItem *)sharedInstance;

/** Get items */
- (void)getItemsForType:(NSString *)area
           purchaseable:(BOOL)purchaseable;

/** Get an item */
- (void)getItem:(NSString *)itemID;

/** Retrieve all items */
- (void)getItems;

@end
