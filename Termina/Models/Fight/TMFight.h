//
//  TMFight.h
//  Termina
//
//  Created by Jason Lagaac on 16/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMBaseDataModel.h"
#import "TMUserCharacter.h"

enum
{
    kFightRunning,
    kFightCompleted,
    kFightAll
} typedef FightState;

enum
{
    kSequenceTypeAttack,
    kSequenceTypeDefend
} typedef SequenceType;


@interface TMFight : TMBaseDataModel

/** Current active fight */
@property (nonatomic, strong) NSMutableDictionary *currentActiveFight;

/** List of current active fights */
@property (nonatomic, strong) NSArray *currentActiveFights;

/** Singleton Instance */
+ (TMFight *)sharedInstance;

////////////////////////////////////////////////////////////////////////////////
// API Actions

/** Create fight */
- (void)createFightWithAttacker:(NSDictionary *)attacker
                       defender:(NSDictionary *)defender
                     questFight:(BOOL)questFight
                      bossFight:(BOOL)bossFight
                       location:(NSString *)locationID;

/** Get Fights */
- (void)getFights:(FightState)fightState;

/** Get Fights By ID */
- (void)getFight:(NSString *)fightID;

/** Update Fight*/
- (void)updateFight:(NSString *)fightID
         withParams:(NSDictionary *)params;

/** Stop Fight */
- (void)stopFight:(NSString *)fightID
       withWinner:(NSString *)winnerID;

/** Create Fight */
- (void)createSequence:(NSMutableArray *)sequence
              forFight:(NSString *)fightID
         withCharacter:(NSString *)userCharacterID
          sequenceType:(SequenceType)type;

/** Create fight */
- (void)createFightWithAttacker:(NSDictionary *)attacker
                       defender:(NSDictionary *)defender;

/** Create boss fight */
- (void)createBossFightWithAttacker:(NSDictionary *)attacker
                    attackerCheckin:(NSString *)attackerCheckin
                           defender:(NSDictionary *)defender
                    defenderCheckin:(NSString *)defenderCheckin
                           location:(NSString *)locationID;

/** Get sequences for fight */
- (void)getSequencesForFight:(NSString *)fightID;


////////////////////////////////////////////////////////////////////////////////
// Monitoring Actions

/** Retrieve Active Fights */
- (void)retrieveActiveFights;

/** Start monitoring for active fights */
- (void)startMonitoringForActiveFights;

/** Stop monitoring for active fights */
- (void)stopMonitoringForActiveFights;


@end
