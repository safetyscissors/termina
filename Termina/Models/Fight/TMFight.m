//
//  TMFight.m
//  Termina
//
//  Created by Jason Lagaac on 16/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMFight.h"
#import "TMUser.h"
#import "TMURLEncoding.h"
#import "TMCustomRequest.h"

@interface TMFight ()

/** Timer to retrieve currently active fights */
@property (nonatomic, strong) NSTimer *updateFightsTimer;

/** Monitoring fights toggle */
@property (nonatomic) BOOL monitoringFights;

@end

@implementation TMFight

/** Singleton Instance */
+ (TMFight *)sharedInstance
{
    static TMFight *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - API Actions
////////////////////////////////////////////////////////////////////////////////

/** Create fight */
- (void)createFightWithAttacker:(NSDictionary *)attacker
                       defender:(NSDictionary *)defender
                     questFight:(BOOL)questFight
                      bossFight:(BOOL)bossFight
                       location:(NSString *)locationID
{
    NSDictionary *parameters = @{ @"_attacker_user" : [attacker objectForKey:@"_user"],
                                  @"_attacker" : [attacker objectForKey:@"_user_character"],
                                  @"_defender_user" : [defender objectForKey:@"_user"],
                                  @"_defender" : [defender objectForKey:@"_user_character"],
                                  @"boss_fight" : [NSNumber numberWithBool:bossFight],
                                  @"questFight" : [NSNumber numberWithBool:questFight],
                                  @"attacker" : attacker,
                                  @"defender" : defender,
                                  @"_location" : locationID };
    
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/fights"];
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", parameters, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightCreated
                     failNotification:kFightCreateFailed];
}

/** Create fight */
- (void)createFightWithAttacker:(NSDictionary *)attacker
                       defender:(NSDictionary *)defender
{
    NSDictionary *parameters = @{ @"_attacker_user" : [attacker objectForKey:@"_user"],
                                  @"_attacker" : [attacker objectForKey:@"_user_character"],
                                  @"_defender_user" : [defender objectForKey:@"_user"],
                                  @"_defender" : [defender objectForKey:@"_user_character"],
                                  @"boss_fight" : @NO,
                                  @"questFight" : @NO,
                                  @"attacker" : attacker,
                                  @"defender" : defender };
    
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/fights"];
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", parameters, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightCreated
                     failNotification:kFightCreateFailed];
}

/** Create boss fight */
- (void)createBossFightWithAttacker:(NSDictionary *)attacker
                    attackerCheckin:(NSString *)attackerCheckin
                           defender:(NSDictionary *)defender
                    defenderCheckin:(NSString *)defenderCheckin
                           location:(NSString *)locationID
{
    NSDictionary *parameters = @{ @"_attacker_user" : [attacker objectForKey:@"_user"],
                                  @"_attacker" : [attacker objectForKey:@"_user_character"],
                                  @"_defender_user" : [defender objectForKey:@"_user"],
                                  @"_defender" : [defender objectForKey:@"_user_character"],
                                  @"_attacker_checkin" : attackerCheckin,
                                  @"_defender_checkin" : defenderCheckin,
                                  @"boss_fight" : [NSNumber numberWithBool:YES],
                                  @"questFight" : [NSNumber numberWithBool:NO],
                                  @"attacker" : attacker,
                                  @"defender" : defender,
                                  @"_location": locationID };
    
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/fights"];
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", parameters, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kBossFightCreated
                     failNotification:kBossFightCreateFailed];


}

/** Get Fights */
- (void)getFights:(FightState)fightState
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/fights"];
    NSMutableURLRequest *request = nil;
    
    
    switch (fightState) {
        case kFightAll:
            request = customRequest(url, @"GET", nil, authData);
            break;
            
        case kFightCompleted:
            request = customRequestWithQueryString(url, @"GET", @{@"state": @"completed"}, authData);
            break;
            
        case kFightRunning:
            request = customRequestWithQueryString(url, @"GET", @{@"state": @"running"}, authData);
            break;
            
        default:
            break;
    }
    
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightsRetrieved
                     failNotification:kFightsRetrieveFailed];

}

/** Get Fights By ID */
- (void)getFight:(NSString *)fightID
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/fights/%@", fightID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    NSMutableURLRequest *request = customRequest(url, @"GET", nil, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightRetrieved
                     failNotification:kFightRetrieveFailed];
}

/** Update Fight*/
- (void)updateFight:(NSString *)fightID
         withParams:(NSDictionary *)params
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/fights/%@", fightID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    NSMutableURLRequest *request = customRequest(url, @"PUT", params, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightUpdated
                     failNotification:kFightUpdateFailed];
}

/** Stop Fight */
- (void)stopFight:(NSString *)fightID
       withWinner:(NSString *)winnerID
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/fights/%@?stop=1", fightID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    NSDictionary *params = @{ @"_winner" : winnerID };
    
    NSMutableURLRequest *request = customRequest(url, @"PUT", params, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightStopped
                     failNotification:kFightStopFailed];
}

/** Create Fight */
- (void)createSequence:(NSMutableArray *)sequence
              forFight:(NSString *)fightID
         withCharacter:(NSString *)userCharacterID
          sequenceType:(SequenceType)sequenceType
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/fights/%@/sequences", fightID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    NSString *sequenceTypeVal = sequenceType ? @"defend" : @"attack";
    
    NSDictionary *params = nil;
    
    if (sequence != nil) {
     params = @{ @"type" : sequenceTypeVal,
                 @"points" : sequence,
                 @"_user_character" : userCharacterID };
    } else {
        params = @{ @"type" : sequenceTypeVal,
                    @"points" : [[NSArray alloc] init],
                    @"_user_character" : userCharacterID };
    }
    
    DebugLog(@"Fight Sequence Parameters: %@", params);
    
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", params, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightSequenceCreated
                     failNotification:kFightSequenceCreateFailed];

}

/** Get sequences for fight */
- (void)getSequencesForFight:(NSString *)fightID
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/fights/%@/sequences", fightID];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    NSMutableURLRequest *request = customRequest(url, @"GET", nil, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kFightSequencesRetrieved
                     failNotification:kFightSequencesRetrieveFailed];
}

#pragma mark - Fight Monitoring Actions
////////////////////////////////////////////////////////////////////////////////

- (void)startMonitoringForActiveFights
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightsRetrieved:)
                                                 name:kFightsRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightsRetrieveFailed:)
                                                 name:kFightsRetrieveFailed
                                               object:nil];

    
    self.monitoringFights = YES;
    self.updateFightsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                              target:self
                                                            selector:@selector(retrieveActiveFights)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)stopMonitoringForActiveFights
{
    self.monitoringFights = NO;
    [self.updateFightsTimer invalidate];
    self.updateFightsTimer = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)retrieveActiveFights
{
    if (self.monitoringFights && !self.updateFightsTimer) {
        [self getFights:kFightRunning];
    }
}

#pragma mark - Fight Retreival Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)fightsRetrieved:(NSNotification *)notification
{
    DebugLog(@"Fights Retrieved: %@", [notification object]);
    self.currentActiveFights = [[notification object] copy];
    self.updateFightsTimer = [NSTimer scheduledTimerWithTimeInterval:30.0f
                                                              target:self
                                                            selector:@selector(retrieveActiveFights)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)fightsRetrieveFailed:(NSNotification *)notification
{
    
}

@end
