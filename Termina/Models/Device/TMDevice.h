#import "TMBaseDataModel.h"

@interface TMDevice : TMBaseDataModel

@property (nonatomic, strong) NSArray *registeredDevices;

/** Shared instance of the device object */
+ (TMDevice *)sharedInstance;

/** Record the device token to NSUserDefaults **/
- (void)setDeviceToken:(NSString *)deviceToken;

/** Retrieve the device token from NSUserDefaults **/
- (NSString *)deviceToken;

/** Retrieve all devices which are registered to the user */
- (void)getDevices;

/** Reset the recorded device token */
- (void)resetDeviceToken;

/** Register a new device in the API */
- (void)registerNewDevice:(NSString *)deviceToken;

/** Unregister a device from the API */
- (void)unregisterDevice:(NSString *)deviceToken;


@end