#import "TMDevice.h"
#import "TMUser.h"
#import "TMCustomRequest.h"

static NSString * const DeviceTokenKey = @"TerminaDeviceToken";

@implementation TMDevice

#pragma mark - Shared instance of the device object
////////////////////////////////////////////////////////////////////////////////

+ (TMDevice *)sharedInstance
{
    static TMDevice *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        if ([self deviceToken] == nil) {
            [self resetDeviceToken];
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(devicesRetrieved:)
                                                     name:kDevicesRetrieved
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(devicesRetrievalFailed:)
                                                     name:kDeviceRetrivevalFailed
                                                   object:nil];
    }
    
    return self;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/** Record the device token to NSUserDefaults **/
- (void)setDeviceToken:(NSString *)deviceToken
{
    [[NSUserDefaults standardUserDefaults] setValue:deviceToken
                                             forKey:DeviceTokenKey];
    
}

/** Retrieve the device token from NSUserDefaults **/
- (NSString *)deviceToken
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:DeviceTokenKey];
}

/** Reset the recorded device token */
- (void)resetDeviceToken
{
    [[NSUserDefaults standardUserDefaults] setValue:@"0"
                                             forKey:DeviceTokenKey];
}


/** Register a new device in the API */
- (void)registerNewDevice:(NSString *)deviceToken
{
    NSDictionary *parameters = @{@"app_id" : deviceToken};
    
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/users/%@/devices", [TMUser sharedInstance].username];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", parameters, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kDeviceRegistered
                 failNotification:kDeviceRegisterFailed];
}

/** Register a new device in the API */
- (void)getDevices
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/users/%@/devices", [TMUser sharedInstance].username];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    
    NSMutableURLRequest *request = customJSONRequest(url, @"GET", nil, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kDevicesRetrieved
                 failNotification:kDeviceRetrivevalFailed];
}


- (void)unregisterDevice:(NSString *)deviceToken
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *requestPath = [NSString stringWithFormat:@"/users/%@/devices/%@", [TMUser sharedInstance].username, deviceToken];
    NSString *url = [kBaseURL stringByAppendingString:requestPath];
    
    NSMutableURLRequest *request = customJSONRequest(url, @"DELETE", nil, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    
    [self runOperationWithRequest:request
              successNotification:kDeviceUnregistered
                 failNotification:kDeviceUnregistrationFailed];
}

#pragma mark - Device retrieval handlers
////////////////////////////////////////////////////////////////////////////////

- (void)devicesRetrieved:(NSNotification *)notification
{
    self.registeredDevices = [[notification object] copy];
    NSPredicate *registeredDevicesPredicate = [NSPredicate predicateWithFormat:@"(apn_token == %@)", [self deviceToken]];
    
    NSArray *matchedDevices = [self.registeredDevices filteredArrayUsingPredicate:registeredDevicesPredicate];
    
    if ([matchedDevices count] == 0) {
        // Register new device
        [self registerNewDevice:[self deviceToken]];
    }
}

- (void)deviceRetrievalFailed:(NSNotification *)notification
{
    self.registeredDevices = nil;
}

#pragma mark - Device Registration Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)deviceRegistered:(NSNotification *)notification
{
    
}

- (void)deviceRegisterFailed:(NSNotification *)notification
{
    
}



@end