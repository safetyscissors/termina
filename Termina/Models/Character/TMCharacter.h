//
//  TMCharacter.h
//  Termina
//
//  Created by Jason Lagaac on 3/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMBaseDataModel.h"

@interface TMCharacter : TMBaseDataModel

+ (TMCharacter *)sharedInstance;

/** Get all player characters */
- (void)getAllPlayerCharacters;

@end
