#import "TMCharacter.h"
#import "TMUser.h"
#import "TMURLEncoding.h"
#import "TMCustomRequest.h"
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation TMCharacter

+ (TMCharacter *)sharedInstance
{    
    static TMCharacter *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - Get Player Characters
////////////////////////////////////////////////////////////////////////////////

- (void)getAllPlayerCharacters
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/characters"];
    NSMutableURLRequest *request = customRequestWithQueryString(url, @"GET", @{@"type":@"player"}, authData);
    NSLog(@"Request: %@", [[request URL] absoluteString]);
    [self runOperationWithRequest:request
              successNotification:kPlayerCharactersRetrieved
                 failNotification:kPlayerCharactersRetrieveFailed];
}

@end
