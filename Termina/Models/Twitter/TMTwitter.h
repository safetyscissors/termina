#import <Foundation/Foundation.h>
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>

@interface TMTwitter : NSObject

/** Selected Account */
@property (nonatomic, strong) ACAccount *selectedTwitterAccount;

+ (TMTwitter *)sharedInstance;

/** Link Twitter Account */
- (void)linkTwitterAccount;

/** Retrieve all available Twitter accounts */
- (NSArray *)getTwitterAccounts;

@end
