#import "TMTwitter.h"

@interface TMTwitter ()

/** Account Store */
@property (nonatomic, strong) ACAccountStore *accountStore;

@end

@implementation TMTwitter

/** Shared instance of the user object */
+ (TMTwitter *)sharedInstance
{
    static TMTwitter *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.accountStore = [[ACAccountStore alloc] init];
        [self loadTwitterAccount];
    }
    
    return self;
}

#pragma mark - Load & Save Actions
////////////////////////////////////////////////////////////////////////////////

- (BOOL)loadTwitterAccount
{
    ACAccount *twitter = [[NSUserDefaults standardUserDefaults] objectForKey:@"TerminaTwitter"];
    
    if (twitter) {
        self.selectedTwitterAccount = twitter;
        return YES;
    }
    
    return NO;
}

- (BOOL)saveTwitterAccount
{
    if (self.selectedTwitterAccount) {
        [[NSUserDefaults standardUserDefaults] setObject:self.selectedTwitterAccount
                                                  forKey:@"TerminaTwitter"];
        return YES;
    }
    
    return NO;
}

#pragma mark - Twitter Account Retrieve Actions
////////////////////////////////////////////////////////////////////////////////

- (NSArray *)getTwitterAccounts
{
    ACAccountType *twitterType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    return [self.accountStore accountsWithAccountType:twitterType];
}

// Link Twitter
- (void)linkTwitterAccount
{
    ACAccountType *twitterType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:twitterType
                                               options:nil
                                            completion:^(BOOL granted, NSError *error) {
                                                if (granted && self.selectedTwitterAccount == nil) {
                                                    if ([[self getTwitterAccounts] count] == 0) {
                                                        [[NSNotificationCenter defaultCenter] postNotificationName:kNoTwitterAccountsAvailable
                                                                                                            object:nil];
                                                    } else {
                                                        if ([[self getTwitterAccounts] count] > 1) {
                                                            // Multiple Twitter Account Available
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:kMultipleTwitterAccountsAvailable
                                                                                                               object:[self getTwitterAccounts]];
                                                        } else {
                                                            // Single Twitter Account Available
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:kSingleTwitterAccountAvailable
                                                                                                                object:[[self getTwitterAccounts] objectAtIndex:0]];
                                                        }
                                                    }
                                                } else {
                                                    // Single Twitter Account Available
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:kTwitterAccountAlreadyLinked
                                                                                                        object:[[self getTwitterAccounts] objectAtIndex:0]];
                                                }
                                            }];
    
}


@end
