#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "TMBaseDataModel.h"

@interface TMUser : TMBaseDataModel <NSCoding>

/** Authentication keys  */
@property (nonatomic, strong) NSDictionary *authDetails;

/** Current user details  */
@property (nonatomic, strong) NSDictionary *currentUserDetails;

/** Shared instance of the user object */
+ (TMUser *)sharedInstance;

- (NSString *)username;

/** Authenticate user */
- (void)authenticateWithUsernameOrEmail:(NSString *)usernameOrEmail
                               password:(NSString *)password;

/** Forgot password action */
- (void)forgotPasswordForUsernameOrEmail:(NSString *)usernameOrEmail;

/** Get user*/
- (void)getUser:(NSString *)username;

/** Create a user */
- (void)createUser:(NSString *)username
      withPassword:(NSString *)password
             email:(NSString *)email;

/** Update a user's details */
- (void)updateUser:(NSString *)username
        parameters:(NSDictionary *)params;

/** Return the ids of the user's friends */
- (NSArray *)getFriends;

/** Returns a friend */
- (void)getFriend:(NSString *)friendUsername;

/** Add friends to user */
- (void)addFriend:(NSString *)friendID;

/** Search for a user based on their username */
- (void)searchForUser:(NSString *)name;


@end
