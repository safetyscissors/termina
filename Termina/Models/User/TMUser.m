#import "TMUser.h"
#import "TMURLEncoding.h"
#import "TMCustomRequest.h"
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>


@implementation TMUser

+ (TMUser *)sharedInstance {
    
    static TMUser *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init {
    
    if (self = [super init]) {

    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self.authDetails = [decoder decodeObjectForKey:@"auth_details"];
    self.currentUserDetails = [decoder decodeObjectForKey:@"user_details"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.authDetails forKey:@"auth_details"];
    [aCoder encodeObject:self.currentUserDetails forKey:@"user_details"];
}


#pragma mark - Authentication
////////////////////////////////////////////////////////////////////////////////

- (void)authenticateWithUsernameOrEmail:(NSString *)usernameOrEmail
                               password:(NSString *)password
{
    NSDictionary *parameters = @{@"username" : usernameOrEmail,
                                 @"password" : password};
    
    NSString *url = [kBaseURL stringByAppendingString:@"/authenticate"];
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", parameters, nil);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kAuthenticated
                     failNotification:kAuthenticationFailed];
    
    
}

#pragma mark - Forgot Password
////////////////////////////////////////////////////////////////////////////////

- (void)forgotPasswordForUsernameOrEmail:(NSString *)usernameOrEmail
{
    NSDictionary *parameters = @{@"username": usernameOrEmail};
    
    NSString *url = [kBaseURL stringByAppendingString:@"/forgot"];
    NSMutableURLRequest *request = customJSONRequest(url, @"POST", parameters, nil);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kPasswordReset
                     failNotification:kPasswordResetFailed];
}

#pragma mark - Create User
////////////////////////////////////////////////////////////////////////////////

- (void)createUser:(NSString *)username
      withPassword:(NSString *)password
             email:(NSString *)email
{
    NSDictionary *parameters = @{@"username" : username,
                                 @"password" : password,
                                 @"email" : email};
    
    NSString *url = [kBaseURL stringByAppendingString:@"/users"];
    NSMutableURLRequest *request = customRequest(url, @"POST", parameters, nil);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserCreated
                     failNotification:kUserCreateFailed];
}

#pragma mark - Username
////////////////////////////////////////////////////////////////////////////////
- (NSString *)username
{
    return [self.currentUserDetails objectForKey:@"username"];
}

#pragma mark - Get User
////////////////////////////////////////////////////////////////////////////////

- (void)getUser:(NSString *)username
{
    NSString *urlEncodedUsername = urlEncode(username);
    NSString *requestURL = [NSString stringWithFormat:@"/users/%@",urlEncodedUsername];
    NSString *url = [kBaseURL stringByAppendingString:requestURL];
    NSMutableURLRequest *request = customRequest(url, @"GET", nil, self.authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserRetrieved
                     failNotification:kUserRetrieveFailed];
}

#pragma mark - Update User
////////////////////////////////////////////////////////////////////////////////

- (void)updateUser:(NSString *)username
        parameters:(NSDictionary *)params
{
    NSString *requestURL = [NSString stringWithFormat:@"/users/%@",username];
    NSString *url = [kBaseURL stringByAppendingString:requestURL];
    NSMutableURLRequest *request = customRequest(url, @"PUT", params, self.authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserUpdated
                     failNotification:kUserUpdateFailed];
}

#pragma mark - Get Friend From User
////////////////////////////////////////////////////////////////////////////////
- (NSArray *)getFriends
{
    return [self.currentUserDetails objectForKey:@"friends"];
}

- (void)getFriend:(NSString *)friendUsername
{
    NSString *username = [self.currentUserDetails objectForKey:@"username"];
    NSString *requestURL = [NSString stringWithFormat:@"/users/%@/friends/%@", username, friendUsername];
    NSString *url = [kBaseURL stringByAppendingString:requestURL];
    NSMutableURLRequest *request = customRequest(url, @"GET", nil, self.authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserFriendRetrieved
                     failNotification:kUserFriendRetrieveFailed];
}

#pragma mark - Add friend to user
////////////////////////////////////////////////////////////////////////////////

- (void)addFriend:(NSString *)friendID
{
    NSString *username = [self.currentUserDetails objectForKey:@"username"];
    NSString *requestURL = [NSString stringWithFormat:@"/users/%@/friends/%@", username, friendID];
    NSString *url = [kBaseURL stringByAppendingString:requestURL];
    NSMutableURLRequest *request = customRequest(url, @"POST", nil, self.authDetails);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserFriendAdded
                     failNotification:kUserFriendAddFailed];
}

#pragma mark - Search for user based on name
///////////////////////////////////////////////////////////////////////////////
- (void)searchForUser:(NSString *)name
{
    NSDictionary *authData = [TMUser sharedInstance].authDetails;
    NSString *url = [kBaseURL stringByAppendingString:@"/users"];
    NSMutableURLRequest *request = customRequestWithQueryString(url, @"GET", @{@"searchTerm": name}, authData);
    
    [self runJSONOperationWithRequest:request
                  successNotification:kUserSearchCompleted
                     failNotification:kUserSearchFailed];
}




@end
