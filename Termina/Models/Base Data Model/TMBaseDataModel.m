#import "TMBaseDataModel.h"
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation TMBaseDataModel


- (void)runJSONOperationWithRequest:(NSMutableURLRequest *)request
                successNotification:(NSString *)successNotification
                   failNotification:(NSString *)failNotification
{
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:successNotification
                                                            object:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id payload = nil;
        
        if (operation.responseData) {
            payload = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                      options:NSJSONReadingMutableContainers
                                                        error:nil];
        } else {
            payload = error;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:failNotification
                                                            object:payload];
    }];
    
    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)runOperationWithRequest:(NSMutableURLRequest *)request
            successNotification:(NSString *)successNotification
               failNotification:(NSString *)failNotification
{
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:successNotification
                                                            object:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id payload = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                     options:NSJSONReadingMutableContainers
                                                       error:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:failNotification
                                                            object:payload];
    }];
    
    [[NSOperationQueue mainQueue] addOperation:op];
}

@end
