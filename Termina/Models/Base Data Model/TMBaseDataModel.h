//
//  TMBaseDataModel.h
//  Termina
//
//  Created by Jason Lagaac on 4/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMBaseDataModel : NSObject

- (void)runJSONOperationWithRequest:(NSMutableURLRequest *)request
                successNotification:(NSString *)successNotification
                   failNotification:(NSString *)failNotification;

- (void)runOperationWithRequest:(NSMutableURLRequest *)request
            successNotification:(NSString *)successNotification
               failNotification:(NSString *)failNotification;

@end
