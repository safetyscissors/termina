//
//  TBCircularSlider.m
//  TB_CircularSlider
//
//  Created by Yari Dareglia on 1/12/13.
//  Copyright (c) 2013 Yari Dareglia. All rights reserved.
//

#import "TMEnergyLevelView.h"

/** Helper Functions **/
#define ToRad(deg) 		( (M_PI * (deg)) / 180.0 )
#define ToDeg(rad)		( (180.0 * (rad)) / M_PI )
#define SQR(x)			( (x) * (x) )

/** Parameters **/
#define TB_SAFEAREA_PADDING 60


#pragma mark - Private -

@interface TMEnergyLevelView(){
    UITextField *_textField;
    int radius;
}

@property (nonatomic,assign) int angle;
@property (nonatomic) CGFloat energyLevel;

@end


#pragma mark - Implementation -

@implementation TMEnergyLevelView

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        self.opaque = NO;
        
        //Define the circle radius taking into account the safe area
        radius = 145;
        
        //Initialize the Angle at 0
        self.angle = 0;
        self.energyLevel = 0;
    }
    
    return self;
}


#pragma mark - UIControl Override -


#pragma mark - Drawing Functions - 

//Use the draw rect to draw the Background, the Circle and the Handle 
-(void)drawRect:(CGRect)rect{
    
    [super drawRect:rect];
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
//** Draw the circle (using a clipped gradient) **/
    
    /** Create THE MASK Image **/
    UIGraphicsBeginImageContext(CGSizeMake(TB_SLIDER_SIZE,TB_SLIDER_SIZE));
    CGContextRef imageCtx = UIGraphicsGetCurrentContext();
    DebugLog(@"New Energy Level: %f", self.energyLevel);
    CGContextAddArc(imageCtx, self.frame.size.width + 10 , self.frame.size.height, radius, ToRad(220), ToRad((260 * self.energyLevel) -40), 1);
    
    //Use shadow to create the Blur effect
    CGContextSetShadowWithColor(imageCtx, CGSizeMake(0, 0), self.angle/20, [UIColor blackColor].CGColor);
    
    //define the path
    CGContextSetLineWidth(imageCtx, TB_LINE_WIDTH);
    CGContextDrawPath(imageCtx, kCGPathStroke);
    
    //save the context content into the image mask
    CGImageRef mask = CGBitmapContextCreateImage(UIGraphicsGetCurrentContext());
    UIGraphicsEndImageContext();
    
    /** Clip Context to the mask **/
    CGContextSaveGState(ctx);
    
    CGContextClipToMask(ctx, self.bounds, mask);
    CGImageRelease(mask);
    
    /** THE GRADIENT **/
    
    //list of components
    /*
    CGFloat components[8] = {
        0.94f, 0.30f, 0.03f, 1.0,     // Start color - Blue
        0.64f, 0.17f, 0.17f, 1.0
     };   // End color - Violet*/
    
    CGFloat components[8] = {
        0.76f, 0.66f, 0.26f, 1.0, // Start color - Blue
        255.0f/255.0f, 208.0f/255.0f, 14.0f/255.0f, 1.0
    };
    
    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, components, NULL, 2);
    CGColorSpaceRelease(baseSpace), baseSpace = NULL;
    
    //Gradient direction
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    //Draw the gradient
    CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0);
    CGGradientRelease(gradient), gradient = NULL;
    
    CGContextRestoreGState(ctx);
}

- (void)updateEnergyLevel:(CGFloat)energyLevel
{
    self.energyLevel = 1.0f - energyLevel;
    [self setNeedsDisplay];
}
@end


