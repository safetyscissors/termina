#import <UIKit/UIKit.h>

@interface TMLoginViewController : UIViewController <UITextFieldDelegate>

/** Username field */
@property (nonatomic, strong) IBOutlet UITextField *usernameField;

/** Password field */
@property (nonatomic, strong) IBOutlet UITextField *passwordField;

/** Sign in button */
@property (nonatomic, strong) IBOutlet UIButton *signInButton;

/** Create new account button */
@property (nonatomic, strong) IBOutlet UIButton *createNewAccountButton;

/** Login user action */
- (IBAction)loginUser:(id)sender;

/** Present the create user view */
- (IBAction)createUser:(id)sender;

/** Present the forgot password view */
- (IBAction)forgotPassword:(id)sender;

@end
