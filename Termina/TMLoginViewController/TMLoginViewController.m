#import "TMLoginViewController.h"
#import "TMViewHandlerController.h"
#import "TMUser.h"
#import "TMFight.h"
#import "TMUserCharacter.h"
#import "TMCharacter.h"
#import "TMURLEncoding.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "KeychainItemWrapper.h"

@interface TMLoginViewController ()
/** View center for 3.5 inch screens */
@property (nonatomic) CGPoint originalCenter;
@end

@implementation TMLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Reset Fields
    [self.usernameField setText:@""];
    [self.passwordField setText:@""];
    
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userAuthenticated:)
                                                 name:kAuthenticated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userAuthenticationFailed:)
                                                 name:kAuthenticationFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userRetrieved:)
                                                 name:kUserRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activeCharacterLoaded:)
                                                 name:kActiveUserCharacterLoaded
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activeCharacterLoadFailed:)
                                                 name:kActiveUserCharacterLoadFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activeCharacterLoadFailed:)
                                                 name:kActiveUserCharacterLoadFailed
                                               object:nil];
    
    self.originalCenter = self.view.center;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - User Authentication Response Actions
////////////////////////////////////////////////////////////////////////////////

- (void)userAuthenticated:(NSNotification *)notification
{
    DebugLog(@"Authenticated: %@", [notification object]);
    [TMUser sharedInstance].authDetails = [[notification object] copy];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[TMUser sharedInstance]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"termina"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Save the current password
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Termina" accessGroup:nil];
    [keychainItem setObject:self.passwordField.text
                     forKey:(__bridge id)kSecValueData];
    [keychainItem setObject:self.usernameField.text
                     forKey:(__bridge id)kSecAttrAccount];
    
    NSString *username = self.usernameField.text;
    [[TMUser sharedInstance] getUser:username];
    [[TMFight sharedInstance] startMonitoringForActiveFights];
}

- (void)userAuthenticationFailed:(NSNotification *)notification
{
    DebugLog(@"Error: %@", [notification object]);
    [SVProgressHUD showErrorWithStatus:@"There was a problem with your login.\n\nPlease try again."];
}

#pragma mark - User Retrieval Response Actions
////////////////////////////////////////////////////////////////////////////////

- (void)userRetrieved:(NSNotification *)notification
{
    [TMUser sharedInstance].currentUserDetails = [[notification object] copy];
    NSDictionary *currentUser = [TMUser sharedInstance].currentUserDetails;
        
    if ([currentUser objectForKey:@"_active_character"] && ![[currentUser objectForKey:@"_active_character"] isEqual: [NSNull null]]) {
        DebugLog(@"Current User: %@", [currentUser objectForKey:@"_active_character"]);
        [[TMUserCharacter sharedInstance] loadActiveCharacter];
    } else {
        DebugLog(@"Current User: %@", currentUser);
        // Present the character select view
        [[TMViewHandlerController sharedInstance] presentCharacterSelect];
        [SVProgressHUD dismiss];
    }
}

#pragma mark - User Character Response Actions
////////////////////////////////////////////////////////////////////////////////

- (void)activeCharacterLoaded:(NSNotification *)notification
{
    DebugLog(@"Retrieved user character: %@", [notification object]);    
    [[TMViewHandlerController sharedInstance] presentMainView];
    [SVProgressHUD dismiss];
}

- (void)activeCharacterLoadFailed:(NSNotification *)notification
{
    
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)loginUser:(id)sender
{
    if (!IS_IPHONE_5) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.view setCenter:_originalCenter];
                         }];
    }
    
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    DebugLog(@"%@",urlEncode(self.usernameField.text));
    DebugLog(@"%@",urlEncode(self.passwordField.text));


    [SVProgressHUD showWithStatus:@"Logging In"
                         maskType:SVProgressHUDMaskTypeGradient];
    
    
    [[TMUser sharedInstance] authenticateWithUsernameOrEmail:self.usernameField.text
                                                    password:self.passwordField.text];
}

- (IBAction)createUser:(id)sender
{
    [[TMViewHandlerController sharedInstance] presentCreateAccount];
}

- (IBAction)forgotPassword:(id)sender
{
    
    [[TMViewHandlerController sharedInstance] presentForgotPassword];
}


#pragma mark - Touch Delegate Actions
////////////////////////////////////////////////////////////////////////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.usernameField isFirstResponder] && [touch view] != self.usernameField) {
        if (!IS_IPHONE_5) {
            [UIView animateWithDuration:0.15
                             animations:^{
                                 [self.view setCenter:self.originalCenter];
                             }];
        }
        [self.usernameField resignFirstResponder];
    } else if ([self.passwordField isFirstResponder] && [touch view] != self.passwordField) {
        if (!IS_IPHONE_5) {
            [UIView animateWithDuration:0.15
                             animations:^{
                                 [self.view setCenter:self.originalCenter];
                             }];
        }
        [self.passwordField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}


#pragma mark - Text Field Delegate
////////////////////////////////////////////////////////////////////////////////

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    DebugLog(@"%@", [[ UIDevice currentDevice] model]);
    
    if (!IS_IPHONE_5) {
        [UIView animateWithDuration:0.15
                         animations:^{
                             [self.view setCenter:CGPointMake(self.view.center.x, self.originalCenter.y - 70)];
                         }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameField) {
        [self.passwordField becomeFirstResponder];
    } else {
        [self loginUser:nil];
    }
    
    return YES;
}

@end
