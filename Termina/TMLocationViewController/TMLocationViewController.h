//
//  TMLocationViewController.h
//  Termina
//
//  Created by Jason Lagaac on 17/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MarqueeLabel/MarqueeLabel.h>

@interface TMLocationViewController : UIViewController <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>

/** Location Data */
@property (nonatomic, strong) NSDictionary *locationData;

/** Active checkins table */
@property (nonatomic, strong) IBOutlet UITableView *checkinsTable;

/** Background image */
@property (nonatomic, strong) IBOutlet UIImageView *backgroundImage;

/** Current location name */
@property (nonatomic, strong) MarqueeLabel *locationName;

/** View from location list view */
@property (nonatomic) BOOL fromLocationListView;

/** Back to fight list action */
- (IBAction)backToFightList:(id)sender;

/** Reload the table data */
- (void)reloadData;

@end