#import "TMLocationViewController.h"
#import "TMLocationBossCell.h"
#import "TMFightListOpponentCell.h"
#import "TMMainViewController.h"
#import "TMViewHandlerController.h"
#import "TMUserCharacter.h"
#import "TMUser.h"
#import "TMLocation.h"
#import "TMCheckin.h"
#import "TMFight.h"
#import "TMUserToFighterConvert.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface TMLocationViewController ()

/** Fight Data */
@property (nonatomic, strong) NSDictionary *fightData;

/** Boss Data */
@property (nonatomic, strong) NSDictionary *bossCheckin;

/** Boss Fight Status */
@property (nonatomic) BOOL bossFight;

/** Selected opponent checkin */
@property (nonatomic, strong) NSDictionary *selectedOpponentCheckin;

/** Selected opponent data */
@property (nonatomic) NSDictionary *selectedOpponent;

/** Current Checkins */
@property (nonatomic, strong) NSMutableArray *currentCheckins;

/** Current Checkin */
@property (nonatomic, strong) NSDictionary *currentCheckin;

/** Selected opponent row */
@property (nonatomic) NSInteger selectedCheckinRow;

/** Checkins Loaded */
@property (nonatomic) BOOL checkinsLoaded;

/** Not enough energy alert */
@property (nonatomic, strong) UIAlertView *notEnoughEnergyAlert;

@end

@implementation TMLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.notEnoughEnergyAlert = [[UIAlertView alloc] initWithTitle:@"Not Enough Energy Available"
                                                               message:@"Would you like to purchase a refill?"
                                                              delegate:nil
                                                     cancelButtonTitle:@"No"
                                                     otherButtonTitles:@"Purchase Refill", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.locationName = [[MarqueeLabel alloc] initWithFrame:CGRectMake(70, 29, 180, 21)
                                                   duration:5.0f
                                              andFadeLength:1.0f];
    self.locationName.textColor = [UIColor whiteColor];
    self.locationName.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:self.locationName];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [SVProgressHUD showWithStatus:@"Loading"];
    
    self.checkinsLoaded = NO;
    self.checkinsTable.userInteractionEnabled = YES;
    self.bossFight = NO;
    self.fightData = nil;
    [self updateLocationLabel];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationRetrieved:)
                                                 name:kLocationRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationRetrieveFailed:)
                                                 name:kLocationRetrieveFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkinsForLocationRetrieved:)
                                                 name:kCheckinsForLocationRetrived
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkinsForLocationRetrieveFailed:)
                                                 name:kCheckinsForLocationRetriveFailed
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkinCreated:)
                                                 name:kCheckinCreated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkinCreateFailed:)
                                                 name:kCheckinCreateFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkinsForUserRetrieved:)
                                                 name:kCheckinsForUserRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkinsForUserRetrieveFailed:)
                                                 name:kCheckinsForUserRetrieveFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCharacterRetrieved:)
                                                 name:kUserCharacterRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCharacterRetrieveFailed:)
                                                 name:kUserCharacterRetrieveFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightCreated:)
                                                 name:kFightCreated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightCreateFailed:)
                                                 name:kFightCreateFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bossFightCreated:)
                                                 name:kBossFightCreated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bossFightCreateFailed:)
                                                 name:kBossFightCreateFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightsRetrieved:)
                                                 name:kFightsRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fightsRetrieveFailed:)
                                                 name:kFightsRetrieveFailed
                                               object:nil];
    
    [[TMLocation sharedInstance] getLocation:[self.locationData objectForKey:@"_id"]];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[TMCheckin sharedInstance] reset];
    self.selectedOpponentCheckin = nil;
    self.selectedCheckinRow = NSNotFound;
    self.bossFight = NO;
    [self reloadData];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)backToFightList:(id)sender
{
    if (self.fromLocationListView) {
        [[TMMainViewController sharedInstance] presentLocationListFromLocation];
    } else {
        [[TMMainViewController sharedInstance] presentFightListFromLocation];
    }

    self.locationData = nil;
    self.bossCheckin = nil;
    self.currentCheckins = nil;
    self.fromLocationListView = NO;
    
}

- (void)reloadData
{
    //DebugLog(@"Location %@", [[User sharedInstance].currentLocation objectForKey:@"name"]);
    [UIView transitionWithView:self.checkinsTable
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        [self.checkinsTable reloadData];
                    } completion:nil];
}

- (void)updateLocationLabel
{
    self.locationName.text = [[self.locationData objectForKey:@"name"] uppercaseString];

    if ([self.locationName.text length] > 20) {
        self.locationName.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0f];
    } else if ([self.locationName.text length] > 24) {
        self.locationName.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0f];
    } else {
        self.locationName.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
    }

}

- (void)startBossFight
{
    NSDictionary *playerData = [TMUserCharacter sharedInstance].activeCharacter;
    NSDictionary *player = userToFighter(playerData, [self.currentCheckin objectForKey:@"_id"]);
    NSDictionary *opponent = userToFighter(self.selectedOpponent, [self.selectedOpponentCheckin objectForKey:@"_id"]);

    
    [[TMFight sharedInstance] createBossFightWithAttacker:player
                                          attackerCheckin:[self.currentCheckin objectForKey:@"_id"]
                                                 defender:opponent
                                          defenderCheckin:[self.selectedOpponentCheckin objectForKey:@"_id"]
                                                 location:[self.locationData objectForKey:@"_id"]];
}

- (void)startDefaultFight
{
    NSDictionary *playerData = [TMUserCharacter sharedInstance].activeCharacter;
    NSDictionary *player = userToFighter(playerData, nil);
    NSDictionary *opponent = userToFighter(self.selectedOpponent, nil);
    [[TMFight sharedInstance] createFightWithAttacker:player
                                             defender:opponent];
}

- (void)presentFightView
{
    [[TMViewHandlerController sharedInstance] presentNewFight:self.fightData
                                                playerCheckin:self.currentCheckin
                                              opponentCheckin:self.selectedOpponentCheckin
                                                     location:self.locationData
                                                    bossFight:self.bossFight
                                                   questFight:NO];
}

#pragma mark - Table Delegate
////////////////////////////////////////////////////////////////////////////////

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"Selected Row: %d", indexPath.row);
    NSString *opponentCharacterID = nil;
    
    if (indexPath.row == 0) {
        opponentCharacterID = [self.bossCheckin objectForKey:@"_user_character"];
        self.selectedOpponentCheckin = self.bossCheckin;
        self.bossFight = YES;
        TMLocationBossCell *cell = (TMLocationBossCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
    } else {
        self.selectedOpponentCheckin = [self.currentCheckins objectAtIndex:indexPath.row - 1];
        opponentCharacterID = [[self.currentCheckins objectAtIndex:indexPath.row - 1] objectForKey:@"_user_character"];
        self.bossFight = NO;
        
        TMFightListOpponentCell *cell = (TMFightListOpponentCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
    }
    
    if ([[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"energy"] floatValue]) {
        self.checkinsTable.userInteractionEnabled = NO;
        [[TMUserCharacter sharedInstance] getUserCharacter:opponentCharacterID];
    } else {
        self.checkinsTable.userInteractionEnabled = YES;
        [self.notEnoughEnergyAlert show];
        [SVProgressHUD dismiss];
    }
}

#pragma mark - Table Datasource
////////////////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 162.0f;
    } else {
        return 86.0f;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    NSInteger checkinTotal = [self.currentCheckins count];
    
    if (self.bossCheckin) {
        checkinTotal++;
    }
    
    return checkinTotal;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"BossCell";
        TMLocationBossCell *cell = (TMLocationBossCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LocationBossCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell = (TMLocationBossCell *)currentObject;
                    break;
                }
            }
        }
        
        NSString *bossName = [self.bossCheckin objectForKey:@"name"];
        cell.bossName.text = [bossName uppercaseString];
        cell.bossLevel.text = [NSString stringWithFormat:@"Level %@ • Boss of 100 Locations", [self.bossCheckin objectForKey:@"level"]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    } else {
        static NSString *CellIdentifier = @"CheckinCell";
        TMFightListOpponentCell *cell = (TMFightListOpponentCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FightListOpponentCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell = (TMFightListOpponentCell *)currentObject;
                    break;
                }
            }
        }
        
        cell.name.text = [[self.currentCheckins objectAtIndex:indexPath.row - 1] objectForKey:@"name"];
        NSInteger level = [[[self.currentCheckins objectAtIndex:indexPath.row - 1] objectForKey:@"level"] integerValue];
        cell.level.text = [NSString stringWithFormat:@"Level %d", level];
        cell.name.textColor = [UIColor whiteColor];
        cell.level.textColor = [UIColor whiteColor];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    
    return nil;
    
}



#pragma mark - Location Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)locationRetrieved:(NSNotification *)notification
{
    DebugLog(@"Location Retrieved: %@", [notification object]);
    [[TMCheckin sharedInstance] getCheckinsForLocation:[self.locationData objectForKey:@"_id"]];
}

- (void)locationRetrieveFailed:(NSNotification *)notification
{
    
}


#pragma mark - Checkin Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)checkinsForLocationRetrieved:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    self.currentCheckins = [[notification object] mutableCopy];
    NSPredicate *bossPredicate = nil;
    
    if ([[self.locationData objectForKey:@"boss_type"] isEqualToString:@"npc"]) {
        // NPC character is current location boss
        bossPredicate = [NSPredicate predicateWithFormat:@"(type == %@)", @"npc"];
        self.bossCheckin = [[self.currentCheckins filteredArrayUsingPredicate:bossPredicate] objectAtIndex:0];
    } else {
        // User character is boss
        NSString *bossID = [self.locationData objectForKey:@"_boss"];
        bossPredicate = [NSPredicate predicateWithFormat:@"(_boss ==  '%@')", bossID];
        self.bossCheckin = [[self.currentCheckins filteredArrayUsingPredicate:bossPredicate] objectAtIndex:0];
    }
    
    [self.currentCheckins removeObjectIdenticalTo:self.bossCheckin];
    [self reloadData];
}

- (void)checkinsForLocationRetrieveFailed:(NSNotification *)notification
{
    
}

#pragma mark - User Character Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)userCharacterRetrieved:(NSNotification *)notification
{
    // Retrieved the opponent character
    self.selectedOpponent = [notification object];
    // Create a new check-in
    [[TMCheckin sharedInstance] createCheckinAtLocation:[self.locationData objectForKey:@"_id"]
                                            coordinates:[TMLocation sharedInstance].currentLocation
                                          userCharacter:[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"]];
    
}

- (void)userCharacterRetrieveFailed:(NSNotification *)notification
{
    
}

#pragma mark - Checkin Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)checkinCreated:(NSNotification *)notification
{
    // Checkin has been successfully created
    self.currentCheckin = [notification object];
    
    if (self.bossFight) {
        [self startBossFight];
    } else {
        [self startDefaultFight];
    }
}

- (void)checkinCreateFailed:(NSNotification *)notification
{
    // Checkin already exists for the current user character
    [[TMCheckin sharedInstance] getRunningCheckinsForUserCharacter:[TMUserCharacter sharedInstance].activeCharacter
                                                        atLocation:[self.locationData objectForKey:@"_id"]];
}

#pragma mark - User Checkin Notification Hanlders
////////////////////////////////////////////////////////////////////////////////

- (void)checkinsForUserRetrieved:(NSNotification *)notification
{
    DebugLog(@"Retrieve Checkins Running: %@", [notification object]);
    self.currentCheckin = [[notification object] lastObject];
    
    if (self.bossFight) {
        [self startBossFight];
    } else {
        [self startDefaultFight];
    }
}

- (void)checkinsForUserRetrieveFailed:(NSNotification *)notification
{
    
}

#pragma mark - Fight Handler Actions
////////////////////////////////////////////////////////////////////////////////

- (void)fightCreated:(NSNotification *)notification
{
    DebugLog(@"Fight Created: %@", [notification object]);
    self.fightData = [notification object];
    [self presentFightView];
}


- (void)fightCreateFailed:(NSNotification *)notification
{
    DebugLog(@"Fight Create Failed: %@", [notification object]);
    [self.notEnoughEnergyAlert show];
}

#pragma mark - Boss Fight Handler Actions
////////////////////////////////////////////////////////////////////////////////

- (void)bossFightCreated:(NSNotification *)notification
{
    DebugLog(@"Boss Fight Created: %@", [notification object]);
    self.fightData = [notification object];
    [self presentFightView];
}

- (void)bossFightCreateFailed:(NSNotification *)notification
{
    DebugLog(@"Boss Fight Create Failed: %@", [notification object]);
    [[TMFight sharedInstance] getFights:kFightRunning];
}

#pragma mark - Fight Handler Actions
////////////////////////////////////////////////////////////////////////////////

- (void)fightsRetrieved:(NSNotification *)notification
{
    DebugLog(@"Fights Retrieved: %@", [notification object]);
    NSString *locationID = [self.locationData objectForKey:@"_id"];
    NSString *playerID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    NSString *opponentID = [self.selectedOpponent objectForKey:@"_id"];
    
    for (NSDictionary *fight in [notification object]) {
        if (self.bossFight) {
            if ([[fight objectForKey:@"_location"] isEqualToString:locationID]) {
                if (([[[fight objectForKey:@"attacker"] objectForKey:@"_user_character"] isEqualToString:playerID] &&
                     [[[fight objectForKey:@"defender"] objectForKey:@"_user_character"] isEqualToString:opponentID]) ||
                     ([[[fight objectForKey:@"attacker"] objectForKey:@"_user_character"] isEqualToString:opponentID] &&
                      [[[fight objectForKey:@"defender"] objectForKey:@"_user_character"] isEqualToString:playerID])) {
                         self.fightData = [fight copy];
                         break;
                }
            }
        } else {
            if (([[[fight objectForKey:@"attacker"] objectForKey:@"_user_character"] isEqualToString:playerID] &&
                 [[[fight objectForKey:@"defender"] objectForKey:@"_user_character"] isEqualToString:opponentID]) ||
                ([[[fight objectForKey:@"attacker"] objectForKey:@"_user_character"] isEqualToString:opponentID] &&
                 [[[fight objectForKey:@"defender"] objectForKey:@"_user_character"] isEqualToString:playerID])) {
                    self.fightData = [fight copy];
                    break;
                }
        }
    }
    
    if (self.fightData) {
        [self presentFightView];
    } else {
        [self.checkinsTable setUserInteractionEnabled:YES];
    }
}

- (void)fightsRetrieveFailed:(NSNotification *)notification
{
    
}

@end