#import "TMFightFriendsViewController.h"
#import "TMMainViewController.h"
#import "TMFriendListCell.h"
#import "TMUser.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface TMFightFriendsViewController ()

/** List of users */
@property (nonatomic, strong) NSMutableArray *matchedUserList;

@end

@implementation TMFightFriendsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.matchedUserList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self checkTextFieldEnabled];

    self.activityIndicator.alpha = 0.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userSearchCompleted:)
                                                 name:kUserSearchCompleted
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userSearchFailed:)
                                                 name:kUserSearchFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendAdded:)
                                                 name:kUserFriendAdded
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendAddFailed:)
                                                 name:kUserFriendAddFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendRetrieved:)
                                                 name:kUserFriendRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendRetrieveFailed:)
                                                 name:kUserFriendRetrieveFailed
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Friend Added Notirication Handlers
////////////////////////////////////////////////////////////////////////////////
- (void)friendAdded:(NSNotification *)notification
{
    [SVProgressHUD showSuccessWithStatus:@"Followed!"];
    [TMUser sharedInstance].currentUserDetails = [notification object];
}

- (void)friendAddFailed:(NSNotification *)notification
{
    [SVProgressHUD showErrorWithStatus:@"Follow failed.\n Please try again later."];
    DebugLog(@"Follow Error: %@", [notification object]);
}

#pragma mark - Friend Search Notification Handlers
////////////////////////////////////////////////////////////////////////////////
- (void)userSearchCompleted:(NSNotification *)notification
{
    self.matchedUserList = [[notification object] mutableCopy];
    [self.resultsTable reloadData];
    [self hideActivityIndicator];
    
    if (![self.matchedUserList count]) {
        self.noSearchResults.hidden = NO;
    }
}

- (void)userSearchFailed:(NSNotification *)notification
{
    [self hideActivityIndicator];
}


#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)searchForFriends:(id)sender
{
    [self showActivityIndicator];
    [self.friendSearchField resignFirstResponder];
    
    [self.matchedUserList removeAllObjects];
    [self.resultsTable reloadData];
    self.noSearchResults.hidden = YES;
    
    
    [[TMUser sharedInstance] searchForUser:self.friendSearchField.text];
}

- (void)showActivityIndicator
{
    self.activityIndicator.hidden = NO;
    self.activityIndicator.alpha = 1.0f;
    [self.activityIndicator startAnimating];
}

- (void)hideActivityIndicator
{
    self.activityIndicator.hidden = YES;
    self.activityIndicator.alpha = 1.0f;
    [self.activityIndicator stopAnimating];
}


#pragma mark - Textfield delegates & actions
////////////////////////////////////////////////////////////////////////////////

- (void)checkTextFieldEnabled
{
    if (![self.friendSearchField.text isEqualToString:@""]) {
        self.findMoreFriendsButton.enabled = YES;
    } else {
        self.findMoreFriendsButton.enabled = NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
                                                       replacementString:(NSString *)string
{
    if (![string isEqualToString:@""]) {
        self.findMoreFriendsButton.enabled = YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    self.findMoreFriendsButton.enabled = NO;
    [textField resignFirstResponder];
    [self hideActivityIndicator];
    
    textField.text = @"";
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchForFriends:nil];
    
    return YES;
}

#pragma mark - UITableView Data Source
////////////////////////////////////////////////////////////////////////////////

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TMFriendListCell *cell = (TMFriendListCell *)[self.resultsTable cellForRowAtIndexPath:indexPath];
    
    if ([cell isFriend]) {
        NSString *username = [cell.userData objectForKey:@"username"];
        [[TMUser sharedInstance] getFriend:username];
    }
}

#pragma mark - UITableView Delegates
////////////////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86.0f;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.matchedUserList != nil) {
        return [self.matchedUserList count];
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FriendCell";
    TMFriendListCell *cell = (TMFriendListCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FriendListCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell = (TMFriendListCell *)currentObject;
                break;
            }
        }
    }
    
    // Configure the cell.
    NSDictionary *friendCharacter = [self.matchedUserList objectAtIndex:indexPath.row];
    cell.name.text = [friendCharacter objectForKey:@"username"];
    cell.userData = friendCharacter;
    DebugLog(@"User: %@", friendCharacter);
    
    NSString *userID = [friendCharacter objectForKey:@"_id"];
    NSArray *currentFriends = [[TMUser sharedInstance].currentUserDetails objectForKey:@"friends"];
    
    if ([currentFriends containsObject:userID]) {
        [cell setFriend:YES];
    }
    
    return cell;
}

#pragma mark - User Retrieved Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)friendRetrieved:(NSNotification *)notification
{
    DebugLog(@"Friend Retrieved: %@", [notification object]);
    [[TMMainViewController sharedInstance] presentFriendProfileWithData:[notification object]];
}


- (void)friendRetrieveFailed:(NSNotification *)notification
{
    
}

@end
