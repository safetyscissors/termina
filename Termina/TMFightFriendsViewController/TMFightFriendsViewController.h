#import <UIKit/UIKit.h>

@interface TMFightFriendsViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDataSource>

/** Find more friends button */
@property (nonatomic, strong) IBOutlet UIButton *findMoreFriendsButton;

/** Friend search text field */
@property (nonatomic, strong) IBOutlet UITextField *friendSearchField;

/** Search results table */
@property (nonatomic, strong) IBOutlet UITableView *resultsTable;

/** Activity indicator */
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

/** No search results label */
@property (nonatomic, strong) IBOutlet UILabel *noSearchResults;


/** Search for friends action */
- (IBAction)searchForFriends:(id)sender;

@end
