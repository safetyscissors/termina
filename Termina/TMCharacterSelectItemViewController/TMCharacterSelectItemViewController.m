//
//  TMCharacterSelectViewController.m
//  Termina
//
//  Created by Jason Lagaac on 5/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMCharacterSelectItemViewController.h"
#import "TMUser.h"
#import "TMUserCharacter.h"
#import "TMViewHandlerController.h"



@interface TMCharacterSelectItemViewController ()
@property (nonatomic, strong) NSString *createdCharacterID;
@property (nonatomic) BOOL isCreatedCharacter;
@end

@implementation TMCharacterSelectItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.characterNameAlert = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCharacterCreated:)
                                                 name:kUserCharacterCreated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCharacterCreateFailed:)
                                                 name:kUserCharacterCreateFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userUpdated:)
                                                 name:kUserUpdated
                                               object:nil];
    
    self.isCreatedCharacter = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextArea Cell Actions
////////////////////////////////////////////////////////////////////////////////

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:) || action == @selector(copy:) || action == @selector(selectAll:))
        return NO;
    
    return [super canPerformAction:action withSender:sender];
}

#pragma mark - UI Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (void)presentCharacterAlertWithTitle:(NSString *)title
                               message:(NSString *)message
{
    self.characterNameAlert = [[UIAlertView alloc]initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
    self.characterNameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [self.characterNameAlert textFieldAtIndex:0].textAlignment = NSTextAlignmentCenter;

    [self.characterNameAlert show];
}

- (IBAction)createCharacter:(id)sender
{
    [self presentCharacterAlertWithTitle:@"Character Name"
                                 message:@"Please enter a name for your character"];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex) {
        if ([self.characterNameAlert textFieldAtIndex:0].text.length) {
            NSString *characterNameValue = [self.characterNameAlert textFieldAtIndex:0].text;
            NSString *characterID = [self.character objectForKey:@"_id"];
            
            [[TMUserCharacter sharedInstance] createUserCharacter:characterID
                                                         withName:characterNameValue];
        } else {
            [self presentCharacterAlertWithTitle:@"Character Name"
                                         message:@"Please enter a name for your character"];
        }
    } else {
        self.characterNameAlert = nil;
    }
}



#pragma mark - API notification handlers
////////////////////////////////////////////////////////////////////////////////

- (void)userCharacterCreated:(NSNotification *)notification
{

    self.createdCharacterID = [[notification object] objectForKey:@"_id"];
    NSString *createdCharacterID = [[notification object] objectForKey:@"_character"];
    NSString *characterType = [self.character objectForKey:@"_id"];
    
    if ([createdCharacterID isEqualToString:characterType]) {
        NSDictionary *userCharacter = [notification object];
        NSDictionary *currentUser = [TMUser sharedInstance].currentUserDetails;
        NSString *username = [currentUser objectForKey:@"username"];
        [TMUserCharacter sharedInstance].activeCharacter = [[notification object] copy];
        
        DebugLog(@"Setting Active Character: %@", username);

        [[TMUser sharedInstance] updateUser:username
                                 parameters:@{ @"_active_character" : [userCharacter objectForKey:@"_id"]}];
        self.isCreatedCharacter = YES;
    }
}

- (void)userCharacterCreateFailed:(NSNotification *)notification
{
    if (self.characterNameAlert) {
        [self presentCharacterAlertWithTitle:@"Name Already Reserved"
                                     message:@"Please enter a name for your character"];
    }
}

- (void)userUpdated:(NSNotification *)notification
{
    NSString *activeCharacterID = [[notification object] objectForKey:@"_active_character"];
    
    if (self.isCreatedCharacter) {
        [[TMUser sharedInstance] setCurrentUserDetails:[notification object]];
        [[TMViewHandlerController sharedInstance] presentMainView];
    }
}

@end