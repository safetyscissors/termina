#import <UIKit/UIKit.h>

@interface TMCharacterSelectItemViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

/** Selectable character information */
@property (nonatomic, strong) NSDictionary *character;

/** Character image */
@property (nonatomic, strong) IBOutlet UIImageView *characterImage;

/** Character description */
@property (nonatomic, strong) IBOutlet UITextView *characterDescription;

/** Character name label */
@property (nonatomic, strong) IBOutlet UILabel *characterName;

/** Create / select character button */
@property (nonatomic, strong) IBOutlet UIButton *createCharacterButton;

/** Character name alert */
@property (nonatomic) UIAlertView *characterNameAlert;

/** Create / select character interface action */
- (IBAction)createCharacter:(id)sender;


@end
