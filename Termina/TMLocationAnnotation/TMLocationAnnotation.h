#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "OCGrouping.h"


@interface TMLocationAnnotation : NSObject <MKAnnotation, OCGrouping>

/** Location coordinates */
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

/** Location title */
@property (nonatomic, copy) NSString *title;

/** Location subtitle */
@property (nonatomic, copy) NSString *subtitle;

/** Location Data */
@property (nonatomic, copy) NSDictionary *locationData;

- (id)initWithName:(NSString*)name
        coordinate:(CLLocationCoordinate2D)coordinate;

- (NSString *) title;

- (CLLocationCoordinate2D)coordinate;


@end
