#import "TMLocationAnnotation.h"

@interface TMLocationAnnotation ()

/** Location Name */
@property (nonatomic, copy) NSString *name;

/** Location Coordinate */
@property (nonatomic, assign) CLLocationCoordinate2D locationCoord;

@end

@implementation TMLocationAnnotation

- (id)initWithName:(NSString*)name
        coordinate:(CLLocationCoordinate2D)coordinate
{
    if ((self = [super init])) {
        if ([name isKindOfClass:[NSString class]]) {
            self.title = name;
        } else {
            self.title = @"Unknown charge";
        }
        
        self.locationCoord = coordinate;
    }
    
    return self;
}

- (CLLocationCoordinate2D)coordinate
{
    return self.locationCoord;
}

- (MKMapItem*)mapItem
{
    MKPlacemark *placemark = [[MKPlacemark alloc]
                              initWithCoordinate:self.coordinate
                              addressDictionary:nil];
    
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapItem.name = self.title;
    
    return mapItem;
}

- (BOOL)isEqual:(TMLocationAnnotation *)annotation;
{
    if (![annotation isKindOfClass:[TMLocationAnnotation class]]) {
        return NO;
    }
    
    return (self.coordinate.latitude == annotation.coordinate.latitude &&
            self.coordinate.longitude == annotation.coordinate.longitude &&
            [self.title isEqualToString:annotation.title] &&
            [self.groupTag isEqualToString:annotation.groupTag]);
}

@end
