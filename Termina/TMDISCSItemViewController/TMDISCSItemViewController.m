//
//  TMDISCSItemViewController.m
//  Termina
//
//  Created by Jason Lagaac on 27/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMDISCSItemViewController.h"
#import "TMDISCSItemDetailView.h"
#import "TMStoreFilterItem.h"
#import "TMUserCharacter.h"

NSMutableArray * retrieveItemsFromUserCharacter(tStoreItemFilter storeItemType, NSDictionary *userCharacter)
{
    
    NSString *itemCategory = nil;
    
    switch (storeItemType) {
        case kStoreItemFilterArm:
            itemCategory = @"arm";
            break;
        case kStoreItemFilterFeet:
            itemCategory = @"feet";
            break;
        case kStoreItemFilterTorso:
            itemCategory = @"torso";
            break;
        case kStoreItemFilterWaist:
            itemCategory = @"waist";
            break;
        default:
            break;
    }

    NSPredicate* p = [NSPredicate predicateWithFormat:@"type = %@", itemCategory];
    NSMutableArray *items = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"items"];
    NSMutableArray *filteredItems = [[items filteredArrayUsingPredicate:p] mutableCopy];
    
    return filteredItems;
}



@interface TMDISCSItemViewController ()

/** List of available items */
@property (nonatomic, strong) NSMutableArray *itemViews;

/** List of available items */
@property (nonatomic, strong) NSMutableArray *itemsInCategory;

@end

@implementation TMDISCSItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.itemViews = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self loadFilters];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Actions
////////////////////////////////////////////////////////////////////////////////

- (void)loadFilters
{
    // pagingView
    InfinitePagingView *pagingView = [[InfinitePagingView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 50.0f)];
    //pagingView.backgroundColor = [UIColor blackColor];
    pagingView.pageSize = CGSizeMake(84.5, self.view.frame.size.height);
    pagingView.delegate = self;
    [self.view addSubview:pagingView];
    
    
    for (int i = 0; i < kStoreItemFilterTotal * 3; i++) {
        TMStoreFilterItem *item = [[TMStoreFilterItem alloc] init];
        item.filterLabel.text = @"Test Item";
        
        DebugLog(@"FrameWidth: %f", item.center.y);
        
        CGRect frame;
        frame.origin.x = item.frame.size.width * i;
        frame.size = item.frame.size;
        [item setFrame:frame];
        item.center = CGPointMake(item.center.x * 1.05, self.filterScrollView.frame.size.height / 2);
        
        switch (i % kStoreItemFilterTotal) {
            case kStoreItemFilterArm:
                item.filterLabel.text = @"ARM";
                item.filterState = kStoreItemFilterArm;
                break;
            case kStoreItemFilterFeet:
                item.filterLabel.text = @"FEET";
                item.filterState = kStoreItemFilterFeet;
                break;
            case kStoreItemFilterTorso:
                item.filterLabel.text = @"TORSO";
                item.filterState = kStoreItemFilterTorso;
                break;
            case kStoreItemFilterWaist:
                item.filterLabel.text = @"WAIST";
                item.filterState = kStoreItemFilterWaist;
                break;
            default:
                break;
        }
        
        [pagingView addPageView:item];
    }
    
    DebugLog(@"Page Index Loaded: %d", pagingView.currentPageIndex);
    [[[pagingView pageViews] objectAtIndex:[pagingView centerPageIndex]] filterActive];
    [self loadItemsInCategory:pagingView];
}

- (void)loadItemsInCategory:(InfinitePagingView *)pagingView
{
    TMStoreFilterItem *item = [pagingView.pageViews objectAtIndex:[pagingView centerPageIndex]];
    self.itemsInCategory = retrieveItemsFromUserCharacter(item.filterState, [TMUserCharacter sharedInstance].activeCharacter);
    
    [self loadItemsIntoView:self.itemsInCategory];
}

- (void)loadItemsIntoView:(NSArray *)loadedItems
{
    self.itemScrollView.alpha = 0.0f;
    self.itemScrollView.contentSize = CGSizeMake(self.itemScrollView.frame.size.width * [loadedItems count], self.itemScrollView.frame.size.height);
    
    CGRect frame;
    frame.size = self.itemScrollView.frame.size;
    
    for (int i = 0; i < [loadedItems count]; i++) {
        TMDISCSItemDetailView *item = nil;
        
        if (IS_IPHONE_5) {
            item = [[TMDISCSItemDetailView alloc] initWithNibName:@"DISCSItemDetailView-568h"
                                                               bundle:nil];
        } else {
            item = [[TMDISCSItemDetailView alloc] initWithNibName:@"DISCSItemDetailView"
                                                               bundle:nil];
        }
        
        frame.origin.y = 0;
        frame.origin.x = self.itemScrollView.frame.size.width * i;
        [item.view setFrame:frame];
        [item setWithData:[loadedItems objectAtIndex:i]];
        
        [self.itemViews addObject:item];
        [self.itemScrollView addSubview:item.view];
    }
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.itemScrollView.alpha = 1.0f;
                     }];
}

- (void)unloadAllItemsViews {
    for (TMDISCSItemDetailView *item in self.itemViews) {
        [item.view removeFromSuperview];
    }
    
    [self.itemViews removeAllObjects];
}

#pragma mark - Infinite Paging View Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)pagingView:(InfinitePagingView *)pagingView
didEndDecelerating:(UIScrollView *)scrollView
       atPageIndex:(NSInteger)pageIndex
{
    [self unloadAllItemsViews];
    [[pagingView.pageViews objectAtIndex:[pagingView centerPageIndex]] filterActive];
    [self loadItemsInCategory:pagingView];
}

- (void)pagingView:(InfinitePagingView *)pagingView
 willBeginDragging:(UIScrollView *)scrollView
{
    for (id item in [pagingView pageViews]) {
        [item filterInactive];
    }
}

@end
