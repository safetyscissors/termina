//
//  TMDISCSItemViewController.h
//  Termina
//
//  Created by Jason Lagaac on 27/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfinitePagingView.h"

@interface TMDISCSItemViewController : UIViewController <InfinitePagingViewDelegate>

/** Item scroll view */
@property (nonatomic, strong) IBOutlet UIScrollView *itemScrollView;

/** Filter scroll view */
@property (nonatomic, strong) IBOutlet UIScrollView *filterScrollView;

@end
