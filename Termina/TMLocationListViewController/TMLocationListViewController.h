#import <UIKit/UIKit.h>

@interface TMLocationListViewController : UIViewController <UITableViewDataSource, UITabBarDelegate>

/** Heading label */
@property (nonatomic, strong) IBOutlet UILabel *heading;

/** Location list table */
@property (nonatomic, strong) IBOutlet UITableView *locationListTable;

/** Locations array */
@property (nonatomic, strong) NSArray *locations;

/** Back action to previous screen */
- (IBAction)backAction:(id)sender;

@end
