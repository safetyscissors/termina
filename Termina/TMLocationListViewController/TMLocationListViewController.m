#import "TMLocationListViewController.h"
#import "TMMainViewController.h"
#import "TMFightListLocationCell.h"
#import "TMLocationAnnotation.h"

@interface TMLocationListViewController ()

@end

@implementation TMLocationListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    DebugLog(@"Locations nearby: :%d", [self.locations count]);
    self.heading.text = [NSString stringWithFormat:@"%d LOCATIONS", [self.locations count]];
    [self.locationListTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Inteface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)backAction:(id)sender
{
    [[TMMainViewController sharedInstance] presentFightListFromLocation];
}

#pragma mark - Table Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TMLocationAnnotation *annotations = [self.locations objectAtIndex:indexPath.row];
    NSDictionary *location = annotations.locationData;
    [[TMMainViewController sharedInstance] presentLocationFromClusterWithData:location];
}

#pragma mark - Table Datasource
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.locations count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"LocationCell";
    TMFightListLocationCell *cell = (TMFightListLocationCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FightListLocationCell"
                                                                 owner:self
                                                               options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell = (TMFightListLocationCell *)currentObject;
                break;
            }
        }
    }
    
    // Configure the cell.
    TMLocationAnnotation *annotations = [self.locations objectAtIndex:indexPath.row];
    NSDictionary *location = annotations.locationData;
    cell.locationName.text = [location objectForKey:@"name"];
    
    if ([location objectForKey:@"boss_name"]) {
        cell.controlledByLabel.text = @"Controlled by";
        cell.bossName.hidden = NO;
        cell.bossName.text = [location objectForKey:@"boss_name"];
        cell.bossName.alpha = 1.0f;
    } else {
        cell.bossName.text = @"???";
        cell.bossName.alpha = 0.5f;
    }
    
    return cell;
}

@end
