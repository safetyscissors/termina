//
//  TMRentViewController.h
//  Termina
//
//  Created by Jason Lagaac on 15/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TMRentViewController : UIViewController <MKMapViewDelegate>

/** Map view for the rent view */
@property (nonatomic, strong) IBOutlet MKMapView *map;

/** Move back to the central view screen */
- (IBAction)backAction:(id)sender;

@end
