//
//  RentViewController.m
//  Termina
//
//  Created by Jason Lagaac on 7/21/13.
//  Copyright (c) 2013 mostlyserious. All rights reserved.
//

#import "TMRentViewController.h"
#import "TMMainViewController.h"

@interface TMRentViewController ()

@end

@implementation TMRentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    MKUserLocation *userLocation = self.map.userLocation;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 500, 500);
    [self.map setRegion:region animated:NO];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 500, 500);
    [self.map setRegion:region animated:YES];
    self.map.showsUserLocation = YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender
{
    [[TMMainViewController sharedInstance] presentCentralView];
}


@end
