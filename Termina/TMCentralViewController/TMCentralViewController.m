#import "TMCentralViewController.h"
#import "TMMainViewController.h"
#import "TMCharacterLevelView.h"
#import "TMUser.h"
#import "TMUserCharacter.h"
#import "TMFight.h"
#import "TMUserCharacter.h"

#define kFilteringFactor 0.1

@interface TMCentralViewController ()

/** Character center point */
@property (nonatomic) CGPoint characterCenter;

/** Location image center */
@property (nonatomic) CGPoint locationCenter;

/** Determine if the character is in motion from the accelerometer */
@property (nonatomic) BOOL characterMoving;

/** Fight update timer to retrieve running fights */
@property (nonatomic) NSTimer *fightUpdateTimer;

/** Accelerometer motion manager */
@property (nonatomic) CMMotionManager *motionManager;

/** Fight update timer to retrieve running fights */
@property (assign, nonatomic) CMAcceleration acceleration;

/** Character velocity movement in the X direction */
@property (assign, nonatomic) CGFloat characterXVelocity;

/** Character velocity movement in the Y direction */
@property (assign, nonatomic) CGFloat characterYVelocity;

/** Character Level View */
@property (nonatomic, strong) TMCharacterLevelView *characterLevelView;

@end

@implementation TMCentralViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(fightsRetrieved:)
                                                     name:kFightsRetrieved
                                                   object:nil];

    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.characterCenter = self.characterImage.center;
    self.locationCenter = self.locationBackground.center;
    self.characterMoving = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadParallax];
    [self loadCharacterLevel];
    [self loadCurrency];
    [self loadFights];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.motionManager stopAccelerometerUpdates];
    self.motionManager = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadParallax
{
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = 1/60;
    [self.motionManager startAccelerometerUpdatesToQueue:[[NSOperationQueue alloc] init]
                                             withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                                                 [(id) self setAcceleration:accelerometerData.acceleration];
                                                 [self performSelectorOnMainThread:@selector(updateCharacterPosition)
                                                                        withObject:nil
                                                                     waitUntilDone:NO];
                                             }];
}

- (void)loadCharacterLevel
{
    self.characterLevelView = [[TMCharacterLevelView alloc] init];
    self.characterLevelView.center = CGPointMake(54.0f, 66.0f);
    [self.characterLevelView.characterImage setImage:[UIImage imageNamed:@"MainCharacterIconSample.png"]];
    [self.view addSubview:self.characterLevelView];
    
    NSString *level = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"level"];
    NSString *levelText = [NSString stringWithFormat:@"Level %@", level];
    
    self.characterLevelView.levelLabel.text = levelText;
    [self.characterLevelView setProgress:0.0f];
    
}

- (void)loadCurrency
{
    // Set the available money label
    if ([[TMUser sharedInstance].currentUserDetails objectForKey:@"money"]) {
        self.gemsLabel.text = [[[TMUser sharedInstance].currentUserDetails objectForKey:@"money"] stringValue];
    } else {
        self.gemsLabel.text = @"0";
    }
    
    float gameCurrency = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"in_game_money"] floatValue];
    // Set the available money label
    if ([[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"in_game_money"]) {
        self.moneyLabel.text = [NSString stringWithFormat:@"%0.2f", gameCurrency];
    } else {
        self.moneyLabel.text = @"0";
    }
}

- (void)loadFights
{
    NSInteger activeFightCount = [[TMFight sharedInstance].currentActiveFights count];
    DebugLog(@"Active Fight Count %d", activeFightCount);
    
    if (activeFightCount) {
        self.fightNotificationLabel.hidden = NO;
        self.fightNotificationLabel.text = [NSString stringWithFormat:@"%d", activeFightCount];
        self.fightNotificationBackground.hidden = NO;
    } else {
        self.fightNotificationBackground.hidden = YES;
        self.fightNotificationLabel.hidden = YES;
    }
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)presentFightNotifications:(id)sender
{
    [[TMMainViewController sharedInstance] presentFightNotifications];
}

- (IBAction)presentStore:(id)sender
{
    [[TMMainViewController sharedInstance] presentStore];
}

- (IBAction)presentRent:(id)sender
{
    [[TMMainViewController sharedInstance] presentRent];
}

#pragma mark - Accelerometer Update Actions
////////////////////////////////////////////////////////////////////////////////

- (void)updateCharacterPosition
{
    // Move the character
    float character_new_x = self.characterImage.center.x - (self.acceleration.x * 0.5);
    if (character_new_x < self.characterCenter.x - 2)
        character_new_x = self.characterCenter.x - 2;
    
    if (character_new_x > self.characterCenter.x + 2)
        character_new_x = self.characterCenter.x + 2;
    
    float character_new_y = self.characterImage.center.y + (self.acceleration.y * - 0.5);
    if (character_new_y < self.characterCenter.y - 3)
        character_new_y = self.characterCenter.y - 3;
    
    if (character_new_y > self.characterCenter.y + 3)
        character_new_y = self.characterCenter.y + 3;
    
    self.characterImage.center = CGPointMake(character_new_x, character_new_y);
    
    float location_new_x = self.locationBackground.center.x - (self.acceleration.x * -0.5);
    if (location_new_x < self.locationCenter.x - 2)
        location_new_x = self.locationCenter.x - 2;
    
    if (location_new_x > self.locationCenter.x + 2)
        location_new_x = self.locationCenter.x + 2;
    
    float location_new_y = self.locationBackground.center.y + (self.acceleration.y * 0.5);
    if (location_new_y < self.locationCenter.y - 3)
        location_new_y = self.locationCenter.y - 3;
    
    if (location_new_y > self.locationCenter.y + 3)
        location_new_y = self.locationCenter.y + 3;
    
    self.locationBackground.center = CGPointMake(location_new_x, location_new_y);
}


#pragma mark - API Notification Actions
////////////////////////////////////////////////////////////////////////////////

- (void)fightsRetrieved:(NSNotification *)notification
{
    [self loadFights];
}



@end
