#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@interface TMCentralViewController : UIViewController

/** Location background for the current closest location type  */
@property (nonatomic, strong) IBOutlet UIImageView *locationBackground;

/** Character Image */
@property (nonatomic, strong) IBOutlet UIImageView *characterImage;

/** Fight notification indicator background image */
@property (nonatomic, strong) IBOutlet UIImageView *fightNotificationBackground;

/** Fight notification indicator number label */
@property (nonatomic, strong) IBOutlet UILabel *fightNotificationLabel;

/** Rent notification indicator background image */
@property (nonatomic, strong) IBOutlet UIImageView *rentNotificationBackground;

/** Rent notification indicator label */
@property (nonatomic, strong) IBOutlet UILabel *rentNotificationLabel;

/** Number of real world currency available to the user */
@property (nonatomic, strong) IBOutlet UILabel *moneyLabel;

/** Number of gems available to the user */
@property (nonatomic, strong) IBOutlet UILabel *gemsLabel;

/** Show the fight notifications view */
- (IBAction)presentFightNotifications:(id)sender;

/** Show the store view */
- (IBAction)presentStore:(id)sender;

/** Show the rent view */
- (IBAction)presentRent:(id)sender;

@end
