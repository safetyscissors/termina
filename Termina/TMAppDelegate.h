//
//  TMAppDelegate.h
//  Termina
//
//  Created by Jason Lagaac on 28/09/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@class TMViewHandlerController;

@interface TMAppDelegate : UIResponder <UIApplicationDelegate>

/** Application Window */
@property (strong, nonatomic) UIWindow *window;

/** View handler */
@property (strong, nonatomic) TMViewHandlerController *viewHandlerController;


@end
