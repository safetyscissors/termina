//
//  TMDISCSViewController.m
//  Termina
//
//  Created by Jason Lagaac on 15/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMDISCSViewController.h"
#import "TMViewHandlerController.h"
#import "TMMainViewController.h"
#import "TMDISCSCharacterViewController.h"
#import "TMDISCSItemViewController.h"
#import "TMDISCSDataViewController.h"
#import "TMDISCSSettingsViewController.h"


/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Toggle State Actions

BOOL toggleIsTheSame(tDiscsActiveView activeView, tDiscsActiveView activeVal)
{
    return activeView == activeVal;
}

/////////////////////////////////////////////////////////////////////////////////

@interface TMDISCSViewController ()

/** Active View Tab */
@property (nonatomic) tDiscsActiveView activeViewType;

/** Character Views Array */
@property (nonatomic) NSMutableArray *characterViewControllers;

/** Active View */
@property (nonatomic, strong) UIView *activeView;

/** Views are switching */
@property (nonatomic) BOOL viewsSwitching;

/** Settings View Controller */
@property (nonatomic, strong) TMDISCSSettingsViewController *settingsViewController;

/** Temporary Character View Controllers */
@property (nonatomic, strong) TMDISCSCharacterViewController *characterViewController;

/** Item View Controller */
@property (nonatomic, strong) TMDISCSItemViewController *itemViewController;

/** Data View Controller */
@property (nonatomic, strong) TMDISCSDataViewController *dataViewController;

@end

@implementation TMDISCSViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self loadCharacters];
    [self loadItems];
    [self loadData];
    [self loadSettings];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.characterViewController.view.alpha = 1.0f;
    [self.scrollView addSubview:self.characterViewController.view];
    self.activeView = self.characterViewController.view;
    
    self.activeViewType = kDiscsNone;
    self.viewsSwitching = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    self.viewsSwitching = NO;
    self.activeViewType = kDiscsNone;
    [self resetToggleButtons];

    for(UIView *subview in [self.scrollView subviews]) {
        [subview removeFromSuperview];
    }
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)resetToggleButtons
{
    self.activeViewType = kDiscsNone;
    [self.dataToggleButon setImage:[UIImage imageNamed:@"DISCSDataTab.png"]
                          forState:UIControlStateNormal];
    [self.itemsToggleButon setImage:[UIImage imageNamed:@"DISCSItemTab.png"]
                          forState:UIControlStateNormal];
    [self.settingsToggleButon setImage:[UIImage imageNamed:@"DISCSSettingsTab.png"]
                              forState:UIControlStateNormal];
}

- (void)loadCharacters
{
    if (IS_IPHONE_5) {
        self.characterViewController = [[TMDISCSCharacterViewController alloc] initWithNibName:@"DISCSCharacterView-568h"
                                                                                        bundle:nil];
    } else {
        self.characterViewController = [[TMDISCSCharacterViewController alloc] initWithNibName:@"DISCSCharacterView"
                                                                                        bundle:nil];
    }
}

- (void)loadItems
{
    if (IS_IPHONE_5) {
        self.itemViewController = [[TMDISCSItemViewController alloc] initWithNibName:@"DISCSItemView-568h"
                                                                              bundle:nil];

    } else {
        self.itemViewController = [[TMDISCSItemViewController alloc] initWithNibName:@"DISCSItemView"
                                                                                        bundle:nil];
    }
    
    self.itemViewController.view.alpha = 0.0f;
}

- (void)loadData
{
    if (IS_IPHONE_5) {
        self.dataViewController = [[TMDISCSDataViewController alloc] initWithNibName:@"DISCSDataView-568h"
                                                                              bundle:nil];
        
    } else {
        self.dataViewController = [[TMDISCSDataViewController alloc] initWithNibName:@"DISCSDataView"
                                                                              bundle:nil];
    }
    
    self.dataViewController.view.alpha = 0.0f;
}

- (void)loadSettings
{
    if (IS_IPHONE_5) {
        self.settingsViewController = [[TMDISCSSettingsViewController alloc] initWithNibName:@"DISCSSettingsView-568h"
                                                                                      bundle:nil];
    } else {
        self.settingsViewController = [[TMDISCSSettingsViewController alloc] initWithNibName:@"DISCSSettingsView"
                                                                                      bundle:nil];
    }
    
    self.settingsViewController.view.alpha = 0.0f;
}

- (void)switchToView:(UIView *)newView
{
    self.viewsSwitching = YES;
    
    for(UIView *subview in [self.scrollView subviews]) {
        [subview removeFromSuperview];
    }
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.scrollView addSubview:newView];
                         self.activeView.alpha = 0.0f;
                         newView.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                             [self.activeView removeFromSuperview];
                             self.activeView = newView;
                             self.viewsSwitching = NO;
                    
                     }];
}

#pragma mark - Interface Actions
/////////////////////////////////////////////////////////////////////////////////

/** Toggle between views */
- (IBAction)toggleView:(id)sender
{
    if (self.viewsSwitching == NO) {
        if (sender == self.dataToggleButon) {
            if (toggleIsTheSame(self.activeViewType, kDiscsData)) {
                // Dismiss current view
                [self resetToggleButtons];
                [self switchToView:self.characterViewController.view];
            } else {
                // Present data view
                [self resetToggleButtons];
                self.activeViewType = kDiscsData;
                [self.dataToggleButon setImage:[UIImage imageNamed:@"DISCSDataTabActive.png"]
                                      forState:UIControlStateNormal];
                [self switchToView:self.dataViewController.view];
            }
        }
        
        if (sender == self.itemsToggleButon) {
            if (toggleIsTheSame(self.activeViewType, kDiscsItems)) {
                // Dismiss current view
                [self resetToggleButtons];
                [self switchToView:self.characterViewController.view];
            } else {
                // Present items view
                [self resetToggleButtons];
                self.activeViewType = kDiscsItems;
                [self.itemsToggleButon setImage:[UIImage imageNamed:@"DISCSItemTabActive.png"]
                                      forState:UIControlStateNormal];
                [self switchToView:self.itemViewController.view];
            }
        }
        
        if (sender == self.settingsToggleButon) {
            if (toggleIsTheSame(self.activeViewType, kDiscsSettings)) {
                // Dismiss current view
                [self resetToggleButtons];
                [self switchToView:self.characterViewController.view];
            } else {
                // Present settings view
                [self resetToggleButtons];
                self.activeViewType = kDiscsSettings;
                [self.settingsToggleButon setImage:[UIImage imageNamed:@"DISCSSettingsTabActive.png"]
                                      forState:UIControlStateNormal];
                [self switchToView:self.settingsViewController.view];
            }
        }
    } else {
        DebugLog(@"Switching is ON");
    }
}

- (IBAction)backToMain:(id)sender
{
    self.viewsSwitching = NO;
    [self resetToggleButtons];
    
    [[TMViewHandlerController sharedInstance] presentMainView];
}

- (IBAction)presentStore:(id)sender
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         [[TMMainViewController sharedInstance] presentStoreFromDISCS];
                     } completion:^(BOOL finished) {
                         [[TMViewHandlerController sharedInstance] presentMainView];
                     }];
}

@end
