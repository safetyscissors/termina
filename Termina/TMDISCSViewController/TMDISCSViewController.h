//
//  TMDISCSViewController.h
//  Termina
//
//  Created by Jason Lagaac on 15/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum DISCSActiveView
{
    kDiscsNone,
    kDiscsData,
    kDiscsItems,
    kDiscsSettings,
} tDiscsActiveView;


@interface TMDISCSViewController : UIViewController <UIScrollViewDelegate>
////////////////////////////////////////////////////////////////////////////////
// Interface objects

/** Data Toggle Button */
@property (nonatomic, strong) IBOutlet UIButton *dataToggleButon;

/** Items Toggle Button */
@property (nonatomic, strong) IBOutlet UIButton *itemsToggleButon;

/** Settings Toggle Button */
@property (nonatomic, strong) IBOutlet UIButton *settingsToggleButon;

/** Settings Toggle Button */
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

////////////////////////////////////////////////////////////////////////////////
// Interface actions

/** Move back to main view */
- (IBAction)backToMain:(id)sender;

/** Present store */
- (IBAction)presentStore:(id)sender;

/** Toggle between views */
- (IBAction)toggleView:(id)sender;

@end
