#import "TMCharacterSelectViewController.h"
#import "TMUser.h"
#import "TMCharacter.h"
#import "TMCharacterSelectItemViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface TMCharacterSelectViewController ()

@end

@implementation TMCharacterSelectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerCharactersRetrieved:)
                                                 name:kPlayerCharactersRetrieved
                                               object:nil];
    
    [[TMCharacter sharedInstance] getAllPlayerCharacters];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - API Observer Actions
////////////////////////////////////////////////////////////////////////////////

- (void)playerCharactersRetrieved:(NSNotification *)notification
{
    self.playerCharacters = [[notification object] copy];
    [self drawCharacterSelectScroller];
    [SVProgressHUD dismiss];
    
    DebugLog(@"Player Characters: %@", self.playerCharacters);
}

#pragma mark - Draw Actions
////////////////////////////////////////////////////////////////////////////////

- (void)drawCharacterSelectScroller
{
    
    NSInteger numberOfCharacters = [self.playerCharacters count];
    self.characterViews = [[NSMutableArray alloc] init];
    
    self.characterSelectScrollView.contentSize = CGSizeMake(self.characterSelectScrollView.frame.size.width * numberOfCharacters,
                                                            self.characterSelectScrollView.frame.size.height);
    
    for (int i = 0; i < numberOfCharacters; i++) {
        
        TMCharacterSelectItemViewController *characterSelectItem = nil;
        NSDictionary *characterData = [self.playerCharacters objectAtIndex:i];
        
        
        if (IS_IPHONE_5) {
            characterSelectItem = [[TMCharacterSelectItemViewController alloc] initWithNibName:@"TMCharacterSelectItemView-568h"
                                                                                        bundle:nil];
        } else {
            characterSelectItem = [[TMCharacterSelectItemViewController alloc] initWithNibName:@"TMCharacterSelectItemView"
                                                                                        bundle:nil];
        }
        
        CGRect frame;
        frame.origin.x = self.characterSelectScrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.characterSelectScrollView.frame.size;
        
        [characterSelectItem.view setFrame:frame];
        [self.characterSelectScrollView addSubview:characterSelectItem.view];
        
        characterSelectItem.character = characterData;
        characterSelectItem.characterName.text = [[characterData objectForKey:@"name"] uppercaseString];
        NSString *characterImageName = [NSString stringWithFormat:@"Character%@Outline.png", [characterData objectForKey:@"name"]];
        
        [characterSelectItem.characterImage setImage:[UIImage imageNamed:characterImageName]];
        [characterSelectItem.characterDescription setText:[characterData objectForKey:@"bio"]];
        [characterSelectItem.createCharacterButton setTitle:[NSString stringWithFormat:@"CHOOSE %@", characterSelectItem.characterName.text]
                                                   forState:UIControlStateNormal];
        if (characterSelectItem.createCharacterButton.titleLabel.text.length > 7) {
            [characterSelectItem.createCharacterButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:9.0f]];
        }
        
        [self.characterViews addObject:characterSelectItem];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    for (TMCharacterSelectItemViewController *characterView in self.characterViews) {
        characterView.characterNameAlert = nil;
    }
}


@end
