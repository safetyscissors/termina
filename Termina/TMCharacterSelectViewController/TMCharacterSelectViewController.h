#import <UIKit/UIKit.h>

@interface TMCharacterSelectViewController : UIViewController <UIScrollViewDelegate>

/** Array to hold all character views */
@property (nonatomic) NSMutableArray *characterViews;

/** Array to hold all player character types */
@property (nonatomic) NSMutableArray *playerCharacters;

/** Scroll view to display the character views */
@property (nonatomic, strong) IBOutlet UIScrollView *characterSelectScrollView;

@end
