//
//  TBCircularSlider.m
//  TB_CircularSlider
//
//  Created by Yari Dareglia on 1/12/13.
//  Copyright (c) 2013 Yari Dareglia. All rights reserved.
//

#import "TMProgressIndicator.h"

#pragma mark - Progress -
@interface TMProgressIndicator ()
@property (nonatomic) CGFloat progress;
@property (nonatomic) CGFloat endProgress;
@property (nonatomic) TMProgressBarType type;
@end

#pragma mark - Implementation -

@implementation TMProgressIndicator

- (id)initWithFrame:(CGRect)frame
          progress:(CGFloat)progress
               type:(TMProgressBarType)type
{
    if (self = [super initWithFrame:frame]){
        self.opaque = NO;
        self.progress = progress;
        self.type = type;
    }
    
    return self;
}

#pragma mark - Drawing Functions - 

//Use the draw rect to draw the Background, the Circle and the Handle 
-(void)drawRect:(CGRect)rect
{    
    [super drawRect:rect];
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor* fillColor = nil;
    UIColor* strokeColor = nil;
    NSArray* gradientColors = nil;
    CGGradientRef gradient = nil;
    UIBezierPath* roundedRectanglePath = nil;
    
    CGFloat currentProgress = (self.progress / 100) * self.frame.size.width;
    
    //// Color Declarations
    if (self.type == TMProgressBarProgress) {
        fillColor = [UIColor colorWithRed: 0 green: 0.761 blue: 0.706 alpha: 1];
        strokeColor = [UIColor colorWithRed: 0.003 green: 0.722 blue: 0.732 alpha: 0.778];
        
        //// Gradient Declarations
        gradientColors = [NSArray arrayWithObjects:
                         (id)strokeColor.CGColor,
                         (id)[UIColor colorWithRed: 0.001 green: 0.741 blue: 0.719 alpha: 0.889].CGColor,
                         (id)fillColor.CGColor, nil];
        
        CGFloat gradientLocations[] = {0, 0.54, 1};
        gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
        
        roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, currentProgress, self.frame.size.height)
                                                          cornerRadius: 2];
    } else if (self.type == TMProgressBarQuestProgress) {
        //// Color Declarations
        fillColor = [UIColor colorWithRed: 0 green: 0.855 blue: 0.534 alpha: 0.86];
        strokeColor = [UIColor colorWithRed: 0.73 green: 1.0f blue: 0.0 alpha: 0.778];
        
        //// Gradient Declarations
        gradientColors = [NSArray arrayWithObjects:
                         (id)strokeColor.CGColor,
                         (id)[UIColor colorWithRed: 0.5 green: 0.888 blue: 0.267 alpha: 0.819].CGColor,
                         (id)fillColor.CGColor, nil];
        CGFloat gradientLocations[] = {0, 0.9, 1};
        gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
        
        //// Rounded Rectangle Drawing
        roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, currentProgress, self.frame.size.height)
                                                          cornerRadius: 4];
    } else if (self.type == TMProgressBarPlayerHealth) {
        //// Color Declarations
        fillColor = [UIColor colorWithRed: 0 green: 0.855 blue: 0.534 alpha: 0.86];
        strokeColor = [UIColor colorWithRed: 0.5 green: 0.91 blue: 0.60 alpha: 0.778];
        
        //// Gradient Declarations
        gradientColors = [NSArray arrayWithObjects:
                          (id)strokeColor.CGColor,
                          (id)[UIColor colorWithRed: 0.02 green: 0.82 blue: 0.71 alpha: 0.819].CGColor,
                          (id)fillColor.CGColor, nil];
        CGFloat gradientLocations[] = {0, 0.9, 1};
        gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
        
        //// Rounded Rectangle Drawing
        roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, currentProgress, self.frame.size.height)
                                                          cornerRadius: 4];
    } else {
        //// Color Declarations
        fillColor = [UIColor colorWithRed: 1.0f green: 0.26f blue: 0.27 alpha: 0.90];
        strokeColor = [UIColor colorWithRed: 0.92 green: 0.03 blue: 0.04 alpha: 0.86];
        
        //// Gradient Declarations
        gradientColors = [NSArray arrayWithObjects:
                          (id)strokeColor.CGColor,
                          (id)[UIColor colorWithRed: 1.0f green: 0.88 blue: 0 alpha: 0.819].CGColor,
                          (id)fillColor.CGColor, nil];
        CGFloat gradientLocations[] = {0, 0.9, 1};
        gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
        
        //// Rounded Rectangle Drawing
        roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, currentProgress, self.frame.size.height)
                                                          cornerRadius: 4];
    }
    
    CGContextSaveGState(context);
    [roundedRectanglePath addClip];
    
    CGContextDrawLinearGradient(context, gradient, CGPointMake(currentProgress, 14.5), CGPointMake(-1, 14.5), 0);
    CGContextRestoreGState(context);
    
    //// Cleanup
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}


- (void)reduceBarToProgressLevel:(CGFloat)newProgressLevel
{
    self.endProgress = newProgressLevel;
    [self resizeProgress];
}

- (void)resizeProgress
{
    if (self.progress > self.endProgress) {
        self.progress -= 0.02f;
        [self setNeedsDisplay];
        
        [NSTimer scheduledTimerWithTimeInterval:0.02f
                                         target:self
                                       selector:@selector(resizeProgress)
                                       userInfo:nil
                                        repeats:YES];
    }
}

@end


