//
//  TBCircularSlider.h
//  TB_CircularSlider
//
//  Created by Yari Dareglia on 1/12/13.
//  Copyright (c) 2013 Yari Dareglia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TMProgressBarType) {
    TMProgressBarProgress,
    TMProgressBarQuestProgress,
    TMProgressBarOpponentHealth,
    TMProgressBarPlayerHealth
};

@interface TMProgressIndicator : UIView

-(id)initWithFrame:(CGRect)frame
          progress:(CGFloat)progress
              type:(TMProgressBarType)type;

- (void)reduceBarToProgressLevel:(CGFloat)newProgressLevel;

@end
