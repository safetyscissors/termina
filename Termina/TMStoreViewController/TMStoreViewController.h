#import <UIKit/UIKit.h>
#import "InfinitePagingView.h"

@interface TMStoreViewController : UIViewController <InfinitePagingViewDelegate>

/** Item scroll view  */
@property (nonatomic, strong) IBOutlet UIScrollView *itemScrollView;

/** Filter scroll view  */
@property (nonatomic, strong) IBOutlet UIScrollView *filterScrollView;

/** Real money available label */
@property (nonatomic, strong) IBOutlet UILabel *realMoneyAvailable;

/** Gems available label */
@property (nonatomic, strong) IBOutlet UILabel *gemsAvailable;

/** No items available label */
@property (nonatomic, strong) IBOutlet UILabel *noItemsAvailable;

/** Presented from DISCS view */
@property (nonatomic) BOOL presentFromDISCS;

/** Back to main view action  */
- (IBAction)backAction:(id)sender;

/** Update available currency */
- (void)updateAvailableCurrencyValues;

@end
