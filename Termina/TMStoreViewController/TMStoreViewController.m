#import "TMStoreViewController.h"
#import "TMMainViewController.h"
#import "TMViewHandlerController.h"
#import "TMStoreItemViewController.h"
#import "TMStoreFilterItem.h"
#import "TMUser.h"
#import "TMUserCharacter.h"
#import "TMItem.h"
#import <SVProgressHUD/SVProgressHUD.h>

#define WIDTH_OF_SCROLL_PAGE 320
#define HEIGHT_OF_SCROLL_PAGE 352
#define WIDTH_OF_IMAGE 79.5
#define HEIGHT_OF_IMAGE 38.5
#define LEFT_EDGE_OFSET 5

NSString * itemToAPIString(tStoreItemFilter storeItem)
{
    NSString *itemCategory = nil;
    
    switch (storeItem) {
        case kStoreItemFilterArm:
            itemCategory = @"arm";
            break;
        case kStoreItemFilterFeet:
            itemCategory = @"feet";
            break;
        case kStoreItemFilterTorso:
            itemCategory = @"torso";
            break;
        case kStoreItemFilterWaist:
            itemCategory = @"waist";
            break;
        default:
            break;
    }
    
    return itemCategory;
}


@interface TMStoreViewController ()

/** List of available items */
@property (nonatomic) NSMutableArray *itemViews;

/** Success Alert Message */
@property (nonatomic, strong) UIAlertView *itemPurchaseSuccessAlert;

@end

@implementation TMStoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.itemViews = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Item purchase success alert
    self.itemPurchaseSuccessAlert = [[UIAlertView alloc] initWithTitle:@"Item Successfully Purchased"
                                                               message:@"Your item can be equipped on the D.I.S.C.S screen"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
    
    // pagingView
    InfinitePagingView *pagingView = [[InfinitePagingView alloc] initWithFrame:CGRectMake(0.0f, 58.0f, self.view.frame.size.width, 50.0f)];
    //pagingView.backgroundColor = [UIColor blackColor];
    pagingView.pageSize = CGSizeMake(84.5, self.view.frame.size.height);
    pagingView.delegate = self;
    
    [self.view addSubview:pagingView];
    
    self.realMoneyAvailable.text = @"0";
    self.gemsAvailable.text = @"0";
    
    for (int i = 0; i < kStoreItemFilterTotal * 3; i++) {
        TMStoreFilterItem *item = [[TMStoreFilterItem alloc] init];
        item.filterLabel.text = @"Test Item";
        
        CGRect frame;
        frame.origin.x = item.frame.size.width * i;
        frame.size = item.frame.size;
        [item setFrame:frame];
        item.center = CGPointMake(item.center.x * 1.05, self.filterScrollView.frame.size.height / 2);
        
        switch (i % kStoreItemFilterTotal) {
            case kStoreItemFilterArm:
                [item.filterLabel setText:@"ARM"];
                item.filterState = kStoreItemFilterArm;
                break;
            case kStoreItemFilterFeet:
                [item.filterLabel setText:@"FEET"];
                item.filterState = kStoreItemFilterFeet;
                break;
            case kStoreItemFilterTorso:
                [item.filterLabel setText:@"TORSO"];
                item.filterState = kStoreItemFilterTorso;
                break;
            case kStoreItemFilterWaist:
                [item.filterLabel setText:@"WAIST"];
                item.filterState = kStoreItemFilterWaist;
                break;
            default:
                break;
        }
        
        [pagingView addPageView:item];
    }
    
    DebugLog(@"Page Index Loaded: %d", pagingView.currentPageIndex);
    [[[pagingView pageViews] objectAtIndex:[pagingView centerPageIndex]] filterActive];
    
    [self updateAvailableCurrencyValues];
    
    
    TMStoreFilterItem *item = [pagingView.pageViews objectAtIndex:[pagingView centerPageIndex]];

    NSPredicate* p = [NSPredicate predicateWithFormat:@"type = %@", itemToAPIString(item.filterState)];

    NSArray *foundItems = [[TMItem sharedInstance].storeItems filteredArrayUsingPredicate:p];
    [self loadItemsForType:foundItems];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Add observers
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(itemAddedToUserCharacter:)
                                                 name:kItemAddedToUserCharacter
                                               object:nil];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)presentNoItemsAvailableLabel
{
    self.noItemsAvailable.hidden = NO;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.itemScrollView.alpha = 0.0f;
                         self.noItemsAvailable.alpha = 0.9f;
                     }];
}

- (void)dismissNoItemsAvailableLabel
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.itemScrollView.alpha = 1.0f;
                         self.noItemsAvailable.alpha = 0.0f;
                     } completion:^(BOOL finished) {
                         self.noItemsAvailable.hidden = YES;
                     }];
}

- (void)updateAvailableCurrencyValues
{
    self.gemsAvailable.text = [[[TMUser sharedInstance].currentUserDetails objectForKey:@"money"] stringValue];
    
    float gameCurrency = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"in_game_money"] floatValue];
    self.realMoneyAvailable.text = [NSString stringWithFormat:@"%0.2f", gameCurrency];
}


#pragma mark - User Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)backAction:(id)sender
{
    if (self.presentFromDISCS) {
        [[TMViewHandlerController sharedInstance] presentDISCS];
        [[TMMainViewController sharedInstance] presentCentralView];
    } else {
        [[TMMainViewController sharedInstance] presentCentralView];
    }
    
    self.presentFromDISCS = NO;
    
    [SVProgressHUD dismiss];
}

#pragma mark - Paging View Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)pagingView:(InfinitePagingView *)pagingView didEndDecelerating:(UIScrollView *)scrollView atPageIndex:(NSInteger)pageIndex
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    [[pagingView.pageViews objectAtIndex:[pagingView centerPageIndex]] filterActive];
    
    
    TMStoreFilterItem *item = [pagingView.pageViews objectAtIndex:[pagingView centerPageIndex]];
    [[TMItem sharedInstance] getItemsForType:itemToAPIString(item.filterState)
                                purchaseable:YES];
    
    NSPredicate* p = [NSPredicate predicateWithFormat:@"type = %@", itemToAPIString(item.filterState)];
    
    NSArray *foundItems = [[TMItem sharedInstance].storeItems filteredArrayUsingPredicate:p];
    [self loadItemsForType:foundItems];
    
    DebugLog(@"Found items: %@", foundItems);
}

- (void)pagingView:(InfinitePagingView *)pagingView willBeginDragging:(UIScrollView *)scrollView
{
    for (id item in [pagingView pageViews]) {
        [item filterInactive];
    }
 
    for (TMStoreItemViewController *item in self.itemViews) {
        [item.view removeFromSuperview];
    }
    
    [self.itemViews removeAllObjects];
}

////////////////////////////////////////////////////////////////////////////////
// Load items into scroll view

- (void)loadItemsIntoView:(NSArray *)loadedItems
{
    self.itemScrollView.alpha = 0.0f;
    self.itemScrollView.contentSize = CGSizeMake(self.itemScrollView.frame.size.width * [loadedItems count], self.itemScrollView.frame.size.height);
    
    CGRect frame;
    frame.size = self.itemScrollView.frame.size;
    
    for (int i = 0; i < [loadedItems count]; i++) {
        TMStoreItemViewController *item = nil;
        
        if (IS_IPHONE_5) {
            item = [[TMStoreItemViewController alloc] initWithNibName:@"StoreRegularItemView-568h"
                                                                    bundle:nil];
        } else {
            item = [[TMStoreItemViewController alloc] initWithNibName:@"StoreRegularItemView"
                                                                    bundle:nil];
        }
        
        frame.origin.y = 0;
        frame.origin.x = self.itemScrollView.frame.size.width * i;
        [item.view setFrame:frame];
        [item setWithData:[loadedItems objectAtIndex:i]];
        
        DebugLog(@"Frame x: %f y: %f ", item.view.frame.origin.x, item.view.frame.origin.y);
        
        [self.itemViews addObject:item];
        [self.itemScrollView addSubview:item.view];
        //[item displayItemData];
        
        NSString *itemID = [[loadedItems objectAtIndex:i] objectForKey:@"_id"];
        NSPredicate* p = [NSPredicate predicateWithFormat:@"_id = %@", itemID];

        DebugLog(@"Predicate: %@", [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"items"] filteredArrayUsingPredicate:p]);
        if ([[[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"items"] filteredArrayUsingPredicate:p] count]) {
            // User character already has the item
            [item setItemPurchased];
        }
    }
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.itemScrollView.alpha = 1.0f;
                     }];
    
}

#pragma mark - API call handlers
////////////////////////////////////////////////////////////////////////////////

- (void)loadItemsForType:(NSArray *)items
{
    DebugLog(@"Items Loaded: %@", items);
    [SVProgressHUD dismiss];
    
    if ([items count]) {
        [self dismissNoItemsAvailableLabel];
        [self loadItemsIntoView:items];
    } else {
        [self presentNoItemsAvailableLabel];
    }
}

- (void)itemAddedToUserCharacter:(NSNotification *)notification
{
    [self.itemPurchaseSuccessAlert show];
    self.gemsAvailable.text = [[[TMUser sharedInstance].currentUserDetails objectForKey:@"money"] stringValue];
    self.realMoneyAvailable.text = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"in_game_money"] stringValue];
    
    [[TMUserCharacter sharedInstance] loadActiveCharacter];
}


@end