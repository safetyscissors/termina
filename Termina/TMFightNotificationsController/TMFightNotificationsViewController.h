#import <UIKit/UIKit.h>

@interface TMFightNotificationsViewController : UIViewController

/** Temporary list of active fights */
@property (nonatomic, strong) NSArray *currentActiveFights;

/** Outstanding fights table */
@property (nonatomic, strong) IBOutlet UITableView *dataTable;

/** Header label */
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

- (IBAction)backAction:(id)sender;

@end

