#import "TMFightNotificationsViewController.h"
#import "TMFightListOpponentCell.h"
#import "TMMainViewController.h"
#import "TMViewHandlerController.h"
#import "TMUserCharacter.h"
#import "TMUser.h"
#import "TMFight.h"



@interface TMFightNotificationsViewController ()

@end

@implementation TMFightNotificationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    // Do any additional setup after loading the view from its nib.
    [super viewDidAppear:animated];
        
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(retrievedRunningFights:)
                                                 name:@"FightsRetrieved"
                                               object:nil];
    
    
    if (![self.currentActiveFights count]) {
        self.statusLabel.hidden = NO;
    } else {
        self.statusLabel.hidden = YES;
    }
    
    [self.dataTable reloadData];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[TMFight sharedInstance] retrieveActiveFights];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender
{
    [[TMFight sharedInstance] startMonitoringForActiveFights];
    [[TMMainViewController sharedInstance] presentCentralView];
}

- (void)retrievedRunningFights:(NSNotification *)notification
{
    [self.dataTable reloadData];
}

#pragma mark - Data Table Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - Data Table Data Source Actions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return [[[User sharedInstance] currentFights] count];
    return [self.currentActiveFights count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // A fight exists between the user and the selected opponent
    NSDictionary *fight = [[TMFight sharedInstance].currentActiveFights objectAtIndex:indexPath.row];
    [TMFight sharedInstance].currentActiveFight = [fight mutableCopy];
    [[TMViewHandlerController sharedInstance] presentExistingFight:fight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OpponentCell";
    TMFightListOpponentCell *cell = (TMFightListOpponentCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray* topLevelObjects = nil;
        topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FightListOpponentCell" owner:self options:nil];
        
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell = (TMFightListOpponentCell *)currentObject;
                break;
            }
        }
    }
    
    
    NSDictionary *fight = [self.currentActiveFights objectAtIndex:indexPath.row];
    NSDictionary *defender = [fight objectForKey:@"defender"];
    NSDictionary *attacker = [fight objectForKey:@"attacker"];
    NSString *userID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    //NSArray *friends = [[User sharedInstance].currentUser objectForKey:@"friends"];
    
    
    if (![[defender objectForKey:@"_user"] isEqualToString:userID]) {
        DebugLog(@"Defender: %@", defender);
        cell.name.text = [defender objectForKey:@"name"];
        cell.level.text = [NSString stringWithFormat:@"Level %@", [defender objectForKey:@"level"]];
    } else {
        DebugLog(@"Attacker: %@", attacker);
        cell.name.text = [attacker objectForKey:@"name"];
        cell.level.text = [NSString stringWithFormat:@"Level %@", [attacker objectForKey:@"level"]];
    }
    
    if ([[fight objectForKey:@"boss_fight"] boolValue]) {
        [cell setBossFight:YES];
    }
    
    if ([fight objectForKey:@"location_name"]) {
        cell.location.text = [fight objectForKey:@"location_name"];
    } else {
        cell.location.text = @"";
    }
    

    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}


@end
