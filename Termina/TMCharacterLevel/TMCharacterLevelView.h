

#import <UIKit/UIKit.h>

@class TMLevelIndicator;

@interface TMCharacterLevelView: UIView

/** Character image */
@property (nonatomic, strong) UIImageView *characterImage;

/** Character level */
@property (nonatomic, strong) UILabel *levelLabel;

/** Level indicator */
@property (nonatomic, strong) TMLevelIndicator *levelIndicator;


- (void)setProgress:(CGFloat)progress;

@end
