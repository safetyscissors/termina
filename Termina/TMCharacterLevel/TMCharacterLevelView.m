//
//  CharacterLevelView.m
//  Termina
//
//  Created by Jason Lagaac on 27/08/13.
//  Copyright (c) 2013 mostlyserious. All rights reserved.
//

#import "TMCharacterLevelView.h"
#import "TMLevelIndicator.h"

#define CHARACTER_LEVEL_WIDTH 93.0f
#define CHARACTER_LEVEL_HEIGHT 93.0f

@interface TMCharacterLevelView ()

@property (nonatomic, strong) UIImageView *background;

@end

@implementation TMCharacterLevelView

- (id)init
{
    if (self = [super initWithFrame:CGRectMake(0, 0, CHARACTER_LEVEL_WIDTH, CHARACTER_LEVEL_HEIGHT)]) {
        // Set the background image
        self.background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MainCharacterLevelBackground.png"]];
        self.background.frame = self.frame;
        [self addSubview:self.background];
        
        self.levelIndicator = [[TMLevelIndicator alloc]initWithFrame:CGRectMake(0, 0, 236, 236)];
        self.levelIndicator.center = CGPointMake(CHARACTER_LEVEL_WIDTH - 15.5, CHARACTER_LEVEL_HEIGHT / 4 - 8);
        [self addSubview:self.levelIndicator];
        
        // Set the character image
        self.characterImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
        self.characterImage.center = CGPointMake(CHARACTER_LEVEL_WIDTH / 2, CHARACTER_LEVEL_HEIGHT / 2);
        [self insertSubview:self.characterImage aboveSubview:self.background];
        
        // Set the level label dimensions and color
        self.levelLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50.0f, 10.0f)];
        self.levelLabel.center = CGPointMake(CHARACTER_LEVEL_WIDTH / 2, CHARACTER_LEVEL_HEIGHT - 20.0f);
        self.levelLabel.textColor = [UIColor whiteColor];
        self.levelLabel.backgroundColor = [UIColor clearColor];
        self.levelLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0f];
        self.levelLabel.textAlignment = NSTextAlignmentCenter;
        self.levelLabel.text = @"Level 100";
        [self addSubview:self.levelLabel];
        
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)setProgress:(CGFloat)progress {
    [self.levelIndicator updateProgress:progress];
}


@end
