#import "TMDISCSItemDetailView.h"
#import "TMUser.h"
#import "TMUserCharacter.h"

/** Check if an item has been equipped */
bool itemEquipped(NSDictionary *item, NSDictionary *userCharacter)
{
    NSArray *equipped_items = [userCharacter objectForKey:@"equipped_items"];
    NSPredicate *availableItemPredicate = [NSPredicate predicateWithFormat:@"(_id == %@)", [item objectForKey:@"_id"]];
    
    NSLog(@"Equipped Items: %d", [[equipped_items filteredArrayUsingPredicate:availableItemPredicate] count]);
    
    return [[equipped_items filteredArrayUsingPredicate:availableItemPredicate] count] ? YES : NO;
}

NSArray * equippedItemsInCategory(NSString *category, NSDictionary *userCharacter)
{
    NSArray *equipped_items = [userCharacter objectForKey:@"equipped_items"];
    NSPredicate *equippedItemPredicate = [NSPredicate predicateWithFormat:@"(type == %@)", category];
    
    return [equipped_items filteredArrayUsingPredicate:equippedItemPredicate];
}


void equipItem(NSDictionary *item, NSDictionary *userCharacter)
{
    NSString *itemID = [item objectForKey:@"_id"];
    NSString *userCharacterID = [userCharacter objectForKey:@"_id"];
    
    NSLog(@"Item to Equip: %@", itemID);
    NSLog(@"Item to User Character: %@", userCharacterID);
    
    [[TMUserCharacter sharedInstance] equipItem:itemID
                                toUserCharacter:userCharacterID];
}

void unequipItem(NSDictionary *item, NSDictionary *userCharacter)
{
    NSString *itemID = [item objectForKey:@"_id"];
    NSString *userCharacterID = [userCharacter objectForKey:@"_id"];
    
    [[TMUserCharacter sharedInstance] unequipItem:itemID
                                  toUserCharacter:userCharacterID];
}



@interface TMDISCSItemDetailView ()

/** Item data */
@property (nonatomic, strong) NSDictionary *itemData;

/** User character data */
@property (nonatomic, strong) NSMutableDictionary *userCharacterData;

/** Check if the item is being equipped */
@property (nonatomic) BOOL itemBeingEquipped;

@end

@implementation TMDISCSItemDetailView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Load user character and user data
    self.userCharacterData = [TMUserCharacter sharedInstance].activeCharacter;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCharacterUpdated:)
                                                 name:kItemEquippedToUserCharacter
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(itemEquipFailed:)
                                                 name:kItemEquipToUserCharacterFailed
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userCharacterUpdated:)
                                                 name:kItemUnequippedFromUserCharacter
                                               object:nil];
    
    if (itemEquipped(self.itemData, self.userCharacterData)) {
        [self.equipButton setBackgroundImage:[UIImage imageNamed:@"DISCSItemUnequipButton.png"]
                                    forState:UIControlStateNormal];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initialisation Actions
////////////////////////////////////////////////////////////////////////////////

- (void)setWithData:(NSDictionary *)itemData
{
    self.itemData = itemData;
    
    // Set the item's name
    [self.itemName setText:[[self.itemData objectForKey:@"name"] uppercaseString]];
        
    NSDictionary *itemAttributes = [self.itemData objectForKey:@"attributes"];
    NSString *attack = [NSString stringWithFormat:@"%@ ATTACK", [itemAttributes objectForKey:@"attack_radius"]];
    [self.attackValueLabel setText: attack];
    
    NSString *moves = [NSString stringWithFormat:@"%@ MOVES", [itemAttributes objectForKey:@"attack_points"]];
    [self.movesValueLabel setText:moves];
    
    NSString *defense = [NSString stringWithFormat:@"%@ DEFENSE", [itemAttributes objectForKey:@"defense_radius"]];
    [self.defenseValueLabel setText:defense];
    
    NSString *health = [NSString stringWithFormat:@"%@ HEALTH", [itemAttributes objectForKey:@"health"]];
    [self.healthValueLabel setText:health];
}

#pragma mark - User Character Update Handler
////////////////////////////////////////////////////////////////////////////////

- (void)userCharacterUpdated:(NSNotification *)notification
{
    DebugLog(@"User Character Updated: %@", [notification object]);
    self.userCharacterData = [notification object];
    [TMUserCharacter sharedInstance].activeCharacter = [notification object];
    self.itemBeingEquipped = NO;
}

- (void)itemEquipFailed:(NSNotification *)notification
{
    if (self.itemBeingEquipped) {
        NSString *item = [self.itemData objectForKey:@"type"];
        NSString *title = [NSString stringWithFormat:@"No Free %@ Slots", [item capitalizedString]];
        UIAlertView *itemSlotError = [[UIAlertView alloc] initWithTitle:title
                                                                message:@"Would you like to unequip items occupying the slots and equip this item?"
                                                               delegate:self
                                                      cancelButtonTitle:@"No"
                                                      otherButtonTitles:@"Yes", nil];
        [itemSlotError show];
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"Button Clicked: %d", buttonIndex);
    
    if (self.itemBeingEquipped) {
        self.itemBeingEquipped = NO;
        
        if (buttonIndex == 1) {

        }
    }
}


#pragma mark - Equip Item Action
////////////////////////////////////////////////////////////////////////////////

- (IBAction)equipItem:(id)sender
{
    self.itemBeingEquipped = YES;
    
    if (!itemEquipped(self.itemData, self.userCharacterData)) {
        equipItem(self.itemData, self.userCharacterData);
        [self.equipButton setBackgroundImage:[UIImage imageNamed:@"DISCSItemUnequipButton.png"]
                                    forState:UIControlStateNormal];
    } else {
        unequipItem(self.itemData, self.userCharacterData);
        [self.equipButton setBackgroundImage:[UIImage imageNamed:@"DISCSItemEquipButton.png"]
                                    forState:UIControlStateNormal];
    }
}



@end
