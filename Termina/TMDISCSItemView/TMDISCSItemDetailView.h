#import <UIKit/UIKit.h>

@interface TMDISCSItemDetailView : UIViewController <UIAlertViewDelegate>

////////////////////////////////////////////////////////////////////////////////
// Instance Properties

/** Item name */
@property (nonatomic, strong) IBOutlet UILabel *itemName;

/** Item description */
@property (nonatomic, strong) IBOutlet UILabel *itemDescription;

/** Health value label */
@property (nonatomic, strong) IBOutlet UILabel *healthValueLabel;

/** Attack value label */
@property (nonatomic, strong) IBOutlet UILabel *attackValueLabel;

/** Defense value label */
@property (nonatomic, strong) IBOutlet UILabel *defenseValueLabel;

/** Moves value label */
@property (nonatomic, strong) IBOutlet UILabel *movesValueLabel;

/** Item image */
@property (nonatomic, strong) IBOutlet UIImageView *itemImage;

/** Item image */
@property (nonatomic, strong) IBOutlet UIButton *equipButton;


////////////////////////////////////////////////////////////////////////////////
// Instance Functions

- (void)setWithData:(NSDictionary *)itemData;

- (IBAction)equipItem:(id)sender;

@end
