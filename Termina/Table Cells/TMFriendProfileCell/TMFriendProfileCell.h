#import <UIKit/UIKit.h>

@interface TMFriendProfileCell : UITableViewCell

/** Username Label */
@property (nonatomic, strong) IBOutlet UILabel *username;

/** User stats label */
@property (nonatomic, strong) IBOutlet UILabel *userStats;

@end
