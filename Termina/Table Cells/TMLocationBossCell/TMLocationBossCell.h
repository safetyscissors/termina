//
//  BossCell.h
//  Termina
//
//  Created by Jason Lagaac on 7/22/13.
//  Copyright (c) 2013 mostlyserious. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMLocationBossCell : UITableViewCell

/** Boss Image */
@property (nonatomic, strong) IBOutlet UIImageView *bossImage;

/** Boss Name Label */
@property (nonatomic, strong) IBOutlet UILabel *bossName;

/** Boss Level Label */
@property (nonatomic, strong) IBOutlet UILabel *bossLevel;

@end
