#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface TMFightListLocationCell : UITableViewCell

/** Location Name Label */
@property (nonatomic, strong) IBOutlet UILabel *locationName;

/** Boss Name Label */
@property (nonatomic, strong) IBOutlet UILabel *bossName;

/** Controlled By Label */
@property (nonatomic, strong) IBOutlet UILabel *controlledByLabel;

@end
