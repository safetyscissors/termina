//
//  TMFightListLocationCell.m
//  Termina
//
//  Created by Jason Lagaac on 15/10/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMFightListLocationCell.h"

@implementation TMFightListLocationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
