#import <UIKit/UIKit.h>

@interface TMFightListOpponentCell : UITableViewCell

/** Opponent name Label */
@property (nonatomic, strong) IBOutlet UILabel *name;

/** Opponent level Label */
@property (nonatomic, strong) IBOutlet UILabel *level;

/** Opponent level Label */
@property (nonatomic, strong) IBOutlet UILabel *location;

/** Boss / friend ribbon */
@property (nonatomic, strong) IBOutlet UIImageView *ribbon;



// Enable boss fight ribbon
- (void)setBossFight:(BOOL)isBossFight;

@end


