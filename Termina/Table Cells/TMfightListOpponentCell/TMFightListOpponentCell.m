#import "TMFightListOpponentCell.h"

@implementation TMFightListOpponentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBossFight:(BOOL)isBossFight
{
    if (isBossFight) {
        self.ribbon.hidden = NO;
    } else {
        self.ribbon.hidden = YES;
    }
}

@end
