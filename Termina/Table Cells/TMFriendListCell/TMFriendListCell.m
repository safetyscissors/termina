#import "TMFriendListCell.h"
#import "TMUser.h"
#import "TMURLEncoding.h"
#import <SVProgressHUD/SVProgressHUD.h>

@implementation TMFriendListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.ribbon.alpha = 0.0f;
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)friendAdded:(NSNotification *)notification
{
    DebugLog(@"Friend Added: %@", [notification object]);
    [self setFriend:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)addFriend:(id)sender
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendAdded:)
                                                 name:kUserFriendAdded
                                               object:nil];
    
    [[TMUser sharedInstance] addFriend:urlEncode(self.name.text)];

}

- (void)setFriend:(BOOL)isFriend
{
    self.ribbon.hidden = NO;
    self.addFriendButton.hidden = YES;
}

- (BOOL)isFriend
{
    return !self.ribbon.hidden;
}

@end
