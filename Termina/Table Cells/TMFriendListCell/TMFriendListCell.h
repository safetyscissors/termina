#import <UIKit/UIKit.h>

@interface TMFriendListCell : UITableViewCell

/** Opponent name Label */
@property (nonatomic, strong) IBOutlet UILabel *name;

/** Opponent level Label */
@property (nonatomic, strong) IBOutlet UILabel *location;

/** Boss / friend ribbon */
@property (nonatomic, strong) IBOutlet UIImageView *ribbon;

/** Boss / friend ribbon */
@property (nonatomic, strong) IBOutlet UIButton *addFriendButton;

@property (nonatomic, strong) NSDictionary *userData;

// Add friend action
- (IBAction)addFriend:(id)sender;

// Enable boss fight ribbon
- (void)setFriend:(BOOL)isFriend;

// Check if cell is a friend cell
- (BOOL)isFriend;

@end


