//
//  StoreItemFilterViewController.h
//  Termina
//
//  Created by Jason Lagaac on 7/24/13.
//  Copyright (c) 2013 mostlyserious. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum StoreItemFilters
{
    kStoreItemFilterTorso,
    kStoreItemFilterArm,
    kStoreItemFilterWaist,
    kStoreItemFilterFeet,
    kStoreItemFilterTotal
} tStoreItemFilter;

@interface TMStoreFilterItem : UIView

/** Activity state of the store filter item  */
@property BOOL active;

/** Filter type  */
@property tStoreItemFilter filterState;

/** Name of the selected item filter  */
@property (nonatomic, strong) UILabel *filterLabel;

/** Toggle the filter on */
- (void)filterActive;

/** Toggle the filter off */
- (void)filterInactive;

@end
