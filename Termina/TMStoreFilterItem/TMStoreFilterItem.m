#import "TMStoreFilterItem.h"

@interface TMStoreFilterItem ()

/** Active background image  */
@property (nonatomic) UIImageView *activeBackground;

/** Inactive background image  */
@property (nonatomic) UIImageView *inactiveBackground;

@end

@implementation TMStoreFilterItem

- (id)init
{
    if (self = [super initWithFrame:CGRectMake(0, 0, 79.5, 38.5)]) {
        // Custom initialization
        
        self.activeBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"StoreFilterButtonActive.png"]];
        self.inactiveBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"StoreFilterButton.png"]];
        
        [self addSubview:self.inactiveBackground];
        [self insertSubview:self.activeBackground
               belowSubview:self.inactiveBackground];
        self.activeBackground.hidden = YES;
        
        self.activeBackground.frame = self.frame;
        self.inactiveBackground.frame = self.frame;
        
        
        self.filterLabel = [[UILabel alloc] initWithFrame:self.frame];
        [self.filterLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:11.0f]];
        [self.filterLabel setTextColor:[UIColor whiteColor]];
        [self.filterLabel setBackgroundColor:[UIColor clearColor]];
        [self.filterLabel setTextAlignment:NSTextAlignmentCenter];
        [self insertSubview:self.filterLabel
               aboveSubview:self.inactiveBackground];
        
        self.active = NO;
    }
    
    return self;
}

- (void)filterActive
{
    DebugLog(@"Toggling Active");
    self.active = YES;
    [self.filterLabel setTextColor:[UIColor blackColor]];

    self.inactiveBackground.hidden = YES;
    self.activeBackground.hidden = NO;
}

- (void)filterInactive
{
    self.active = NO;
    [self.filterLabel setTextColor:[UIColor whiteColor]];
    self.inactiveBackground.hidden = NO;
    self.activeBackground.hidden = YES;
}


@end
