#import <UIKit/UIKit.h>

@interface TMFightResultViewController : UIViewController <UIScrollViewDelegate>

// Heading Label
@property (nonatomic, strong) IBOutlet UILabel *headingLabel;

// Share Label Heading
@property (nonatomic, strong) IBOutlet UILabel *shareLabel;

// Scroll view that presents each earned item/attribute/bonus
@property (nonatomic, strong) IBOutlet UIScrollView *resultScrollView;

// Page control which indicates which position the user is currently on the scroll view
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

// Load result views
- (void)loadWithResultsViews:(NSMutableArray *)resultViews;

// Load the fight result as a win
- (void)loadWin;

// Load the fight result as a lose
- (void)loadLose;

@end
