#import "TMFightResultViewController.h"
#import "TMCharacterLevelView.h"
#import "TMUserCharacter.h"
#import "TMViewHandlerController.h"

@interface TMFightResultViewController ()

/** Character Level View */
@property (nonatomic, strong) TMCharacterLevelView *characterLevelView;

/** Start touch point */
@property (nonatomic) CGPoint startTouchPoint;

/** End touch point */
@property (nonatomic) CGPoint endTouchPoint;

@end

@implementation TMFightResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self loadCharacterLevel];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadCharacterLevel
{
    self.characterLevelView = [[TMCharacterLevelView alloc] init];
    
    if (IS_IPHONE_5) {
        self.characterLevelView.center = CGPointMake(160.0f, 141.0f);
    } else {
        self.characterLevelView.center = CGPointMake(160.0f, 121.0f);
    }
    [self.characterLevelView.characterImage setImage:[UIImage imageNamed:@"MainCharacterIconSample.png"]];
    [self.view addSubview:self.characterLevelView];
    
    NSString *level = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"level"];
    NSString *levelText = [NSString stringWithFormat:@"Level %@", level];
    
    self.characterLevelView.levelLabel.text = levelText;
    [self.characterLevelView setProgress:0.0f];
}

- (void)loadWin
{
    self.headingLabel.text = @"YOU WON!";
    self.headingLabel.textColor = [UIColor whiteColor];
    self.shareLabel.text = @"SHARE YOUR VICTORY";
}

- (void)loadLose
{
    self.headingLabel.text = @"YOU LOST";
}

- (void)loadWithResultsViews:(NSMutableArray *)resultViews
{
    UIView *resultView = [resultViews objectAtIndex:0];
    float contentWidth = resultView.frame.size.width * [resultViews count];
    float contentHeight = resultView.frame.size.height;
    
    self.resultScrollView.contentSize = CGSizeMake(contentWidth, contentHeight);

    for (int i = 0; i < [resultViews count]; i++) {
        UIView *result = [resultViews objectAtIndex:i];
        CGRect resultItemFrame = [result frame];
        resultItemFrame.origin.x = i * resultItemFrame.size.width;
        result.frame = resultItemFrame;
        
        [self.resultScrollView addSubview:result];
    }
    
    if ([resultViews count] == 1) {
        [self.pageControl setHidden:YES];
    } else {
        [self.pageControl setHidden:NO];
        self.pageControl.numberOfPages = [resultViews count];
    }
}

#pragma mark - Touch handlers
////////////////////////////////////////////////////////////////////////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    self.startTouchPoint = [touch locationInView:touch.view];
    
    DebugLog(@"Start Touch Point: %f %f", self.startTouchPoint.x, self.startTouchPoint.y);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    self.endTouchPoint = [touch locationInView:touch.view];
    
    DebugLog(@"End Touch Point: %f %f", self.endTouchPoint.x, self.endTouchPoint.y);
    
    if (self.endTouchPoint.y > 420.0f && self.startTouchPoint.y > 420.0f) {
        if (self.endTouchPoint.x > self.startTouchPoint.x) {
            [[TMViewHandlerController sharedInstance] presentMainView];
        }
    }
}

#pragma mark - Scroll View Delegates
////////////////////////////////////////////////////////////////////////////////

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.resultScrollView.frame.size.width;
    int page = floor((self.resultScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}



@end
