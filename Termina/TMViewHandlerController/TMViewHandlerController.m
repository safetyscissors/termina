//
//  TMViewHandlerController.m
//  Termina
//
//  Created by Jason Lagaac on 30/09/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import "TMViewHandlerController.h"
#import "TMLoginViewController.h"
#import "TMMainViewController.h"
#import "TMForgotPasswordViewController.h"
#import "TMCreateAccountViewController.h"
#import "TMCharacterSelectViewController.h"
#import "TMQuestMainViewController.h"
#import "TMFightViewController.h"
#import "TMDISCSViewController.h"
#import "TMUserCharacter.h"
#import "TMItem.h"
#import "TMFightHelpers.h"
#import "TMUser.h"
#import "TMFight.h"
#import "TMDevice.h"

#import "KeychainItemWrapper.h"
#import <Security/Security.h>
#import <SVProgressHUD/SVProgressHUD.h>


@interface TMViewHandlerController ()

/** Login view controller */
@property (nonatomic, strong) TMLoginViewController *loginViewController;

/** Main view controller */
@property (nonatomic, strong) TMMainViewController *mainViewController;

/** Main view controller */
@property (nonatomic, strong) TMForgotPasswordViewController *forgotPasswordViewController;

/** Create account view controller */
@property (nonatomic, strong) TMCreateAccountViewController *createAccountViewController;

/** Create account view controller */
@property (nonatomic, strong) TMCharacterSelectViewController *characterSelectViewController;

/** Create account view controller */
@property (nonatomic, strong) TMQuestMainViewController *questMainViewController;

/** Create account view controller */
@property (nonatomic, strong) TMFightViewController *fightViewController;

/** DISCS view controller */
@property (nonatomic, strong) TMDISCSViewController *discsViewController;

@property (nonatomic, strong) UIImageView *terminaLoadImageView;

/** Active view controller */
@property (nonatomic, strong) UIView *activeView;

/** Previous view controller */
@property (nonatomic, strong) UIView *previousView;

@end

@implementation TMViewHandlerController

+ (TMViewHandlerController *)sharedInstance {
    
    static TMViewHandlerController *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        // Custom initialization
        if (IS_IPHONE_5) {
            self.loginViewController = [[TMLoginViewController alloc] initWithNibName:@"LoginView-568h"
                                                                               bundle:nil];
            
            self.forgotPasswordViewController = [[TMForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordView-568h"
                                                                                                 bundle:nil];
            
            self.createAccountViewController = [[TMCreateAccountViewController alloc] initWithNibName:@"CreateAccountView-568h"
                                                                                                 bundle:nil];
            
            self.characterSelectViewController = [[TMCharacterSelectViewController alloc] initWithNibName:@"CharacterSelectView-568h"
                                                                                                   bundle:nil];
            
            self.questMainViewController = [[TMQuestMainViewController alloc] initWithNibName:@"QuestMainView-568h"
                                                                                   bundle:nil];
            
            self.fightViewController = [[TMFightViewController alloc] initWithNibName:@"FightView-568h"
                                                                               bundle:nil];
            
            self.discsViewController = [[TMDISCSViewController alloc] initWithNibName:@"DISCSView-568h"
                                                                               bundle:nil];
        } else {
            self.loginViewController = [[TMLoginViewController alloc] initWithNibName:@"LoginView"
                                                                               bundle:nil];
            
            self.forgotPasswordViewController = [[TMForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordView"
                                                                                                 bundle:nil];
            
            self.createAccountViewController = [[TMCreateAccountViewController alloc] initWithNibName:@"CreateAccountView"
                                                                                               bundle:nil];
            
            self.characterSelectViewController = [[TMCharacterSelectViewController alloc] initWithNibName:@"CharacterSelectView"
                                                                                                   bundle:nil];
            
            self.questMainViewController = [[TMQuestMainViewController alloc] initWithNibName:@"QuestMainView"
                                                                                       bundle:nil];
            
            self.fightViewController = [[TMFightViewController alloc] initWithNibName:@"FightView"
                                                                               bundle:nil];
            
            self.discsViewController = [[TMDISCSViewController alloc] initWithNibName:@"DISCSView"
                                                                               bundle:nil];
        }
    }
    
    self.mainViewController = [TMMainViewController sharedInstance];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if (IS_IPHONE_5) {
        self.terminaLoadImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-568h@2x.png"]];
    } else {
        self.terminaLoadImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default@2x.png"]];
    }
    
    self.terminaLoadImageView.frame = self.view.frame;
    [self.view addSubview:self.terminaLoadImageView];
    
    self.previousView = nil;
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Termina"
                                                                            accessGroup:nil];
    
    NSData *passwordData = [keychainItem objectForKey:(__bridge id)kSecValueData];
    NSString *password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
    NSString *username = [keychainItem objectForKey:(__bridge id)kSecAttrAccount];
    
    DebugLog(@"Password: %d Username: %d", [password length], [username length]);
    if ([username length] == 0 && [password length] == 0) {
        // No saved credentials
        [self.terminaLoadImageView removeFromSuperview];
        self.terminaLoadImageView = nil;
        
        // Reset keychain
        [keychainItem resetKeychainItem];
        
        [self.view addSubview:self.loginViewController.view];
        self.activeView = self.loginViewController.view;
    } else {
        // Saved credentials present
        self.activeView = nil;
        
        [SVProgressHUD showWithStatus:@"Loading"
                             maskType:SVProgressHUDMaskTypeGradient];
        
        [self loadUserLoadObservers];
        [[TMUser sharedInstance] authenticateWithUsernameOrEmail:username
                                                        password:password];
    }
    
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)presentView:(UIView *)presentedView
{
    if (self.activeView != presentedView) {
        if (self.activeView) {
            presentedView.alpha = 0.0f;

            [self.view insertSubview:presentedView
                        aboveSubview:self.activeView];
            
            [UIView animateWithDuration:0.2
                             animations:^{
                                 presentedView.alpha = 1.0f;
                                 self.activeView.alpha = 0.0f;
                             } completion:^(BOOL finished) {
                                 if (finished) {
                                     [self.activeView removeFromSuperview];
                                     DebugLog(@"Finished: %@", self.activeView);
                                     self.activeView = presentedView;
                                 }
                             }];
        } else {
            [self.view addSubview:presentedView];
            self.activeView = presentedView;
        }
    }
}


#pragma mark - Present Main View Action
////////////////////////////////////////////////////////////////////////////////

- (void)presentMainView
{
    DebugLog(@"Present Main View");
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self presentView:self.mainViewController.view];
}

#pragma mark - Present Forgot Password
////////////////////////////////////////////////////////////////////////////////

- (void)presentLogin
{
    [self presentView:self.loginViewController.view];
}

- (void)presentForgotPassword
{
    [self presentView:self.forgotPasswordViewController.view];
}

- (void)presentLoginFromForgotPassword
{
    [self presentView:self.loginViewController.view];
}


#pragma mark - Present Forgot Password
////////////////////////////////////////////////////////////////////////////////
- (void)presentCreateAccount
{
    [self presentView:self.createAccountViewController.view];
}

- (void)presentLoginFromCreateAccount
{
    [self presentView:self.loginViewController.view];
}

#pragma mark - Present Character Select
////////////////////////////////////////////////////////////////////////////////

- (void)presentCharacterSelect
{
    [self presentView:self.characterSelectViewController.view];
}

#pragma mark - Present Quest
////////////////////////////////////////////////////////////////////////////////

- (void)presentQuest
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self presentView:self.questMainViewController.view];
    [[TMMainViewController sharedInstance] presentCentralView];
}

#pragma mark - Present Fight
////////////////////////////////////////////////////////////////////////////////

- (void)presentNewFight:(NSDictionary *)fightData
          playerCheckin:(NSDictionary *)playerCheckin
        opponentCheckin:(NSDictionary *)opponentCheckin
               location:(NSDictionary *)location
              bossFight:(BOOL)bossFight
             questFight:(BOOL)questFight
{
    NSString *playerCharacterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    self.fightViewController.opponentData = opponentForFight(playerCharacterID, fightData);
    NSString *opponentCharacterID = [self.fightViewController.opponentData objectForKey:@"_user_character"];
    
    self.fightViewController.fightData = fightData;
    self.fightViewController.playerData = opponentForFight(opponentCharacterID, fightData);
    self.fightViewController.playerCheckinData = playerCheckin;
    self.fightViewController.opponentCheckinData = opponentCheckin;
    self.fightViewController.locationData = [location copy];
    self.fightViewController.bossFight = bossFight;
    self.fightViewController.questFight = questFight;
    [self presentView:self.fightViewController.view];
}

- (void)presentExistingFight:(NSDictionary *)fight
{
    NSString *activeCharacterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    
    if (checkPlayerIsAttacker(activeCharacterID, fight)) {
        self.fightViewController.playerData = [fight objectForKey:@"attacker"];
        self.fightViewController.opponentData = [fight objectForKey:@"defender"];
    } else {
        self.fightViewController.playerData = [fight objectForKey:@"defender"];
        self.fightViewController.opponentData = [fight objectForKey:@"attacker"];
    }
    
    self.fightViewController.bossFight = [[fight objectForKey:@"boss_fight"] boolValue];
    self.fightViewController.questFight = [[fight objectForKey:@"quest_fight"] boolValue];
    self.fightViewController.fightData = [fight mutableCopy];
    
    [self presentView:self.fightViewController.view];
}

#pragma mark - Present DISCS
////////////////////////////////////////////////////////////////////////////////

- (void)presentDISCS
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self presentView:self.discsViewController.view];
}


#pragma mark - Authentication handlers if the user already has existing credentails
/////////////////////////////////////////////////////////////////////////////////

- (void)loadUserLoadObservers
{
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userAuthenticated:)
                                                 name:kAuthenticated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userAuthenticationFailed:)
                                                 name:kAuthenticationFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userRetrieved:)
                                                 name:kUserRetrieved
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activeCharacterLoaded:)
                                                 name:kActiveUserCharacterLoaded
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activeCharacterLoadFailed:)
                                                 name:kActiveUserCharacterLoadFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activeCharacterLoadFailed:)
                                                 name:kActiveUserCharacterLoadFailed
                                               object:nil];
}

#pragma mark - User Authentication Response Actions
////////////////////////////////////////////////////////////////////////////////

- (void)userAuthenticated:(NSNotification *)notification
{
    DebugLog(@"Authenticated: %@", [notification object]);
    [TMUser sharedInstance].authDetails = [[notification object] copy];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[TMUser sharedInstance]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"termina"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Termina" accessGroup:nil];
    NSString *username = [keychainItem objectForKey:(__bridge id)kSecAttrAccount];
    
    if ([[TMDevice sharedInstance] deviceToken]) {
        // Determine if there was a device token
        [[TMDevice sharedInstance] getDevices];
    }

    [[TMUser sharedInstance] getUser:username];
    [[TMFight sharedInstance] startMonitoringForActiveFights];
    [[TMItem sharedInstance] getItems];
}

- (void)userAuthenticationFailed:(NSNotification *)notification
{
    DebugLog(@"Error: %@", [notification object]);
    [SVProgressHUD showErrorWithStatus:@"There was a problem with your login.\n\nPlease try again."];
    
    
    // No saved credentials
    [self.terminaLoadImageView removeFromSuperview];
    self.terminaLoadImageView = nil;
    
    // Reset keychain
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Termina"
                                                                            accessGroup:nil];
    [keychainItem resetKeychainItem];
    
    [self.view addSubview:self.loginViewController.view];
    self.activeView = self.loginViewController.view;
}

#pragma mark - User Retrieval Response Actions
////////////////////////////////////////////////////////////////////////////////

- (void)userRetrieved:(NSNotification *)notification
{
    [TMUser sharedInstance].currentUserDetails = [[notification object] copy];
    NSDictionary *currentUser = [TMUser sharedInstance].currentUserDetails;
    
    if ([currentUser objectForKey:@"_active_character"] && ![[currentUser objectForKey:@"_active_character"] isEqual: [NSNull null]]) {
        DebugLog(@"Current User: %@", [currentUser objectForKey:@"_active_character"]);
        [[TMUserCharacter sharedInstance] loadActiveCharacter];
    } else {
        DebugLog(@"Current User: %@", currentUser);
        // Present the character select view
        [self.terminaLoadImageView removeFromSuperview];
        [[TMViewHandlerController sharedInstance] presentCharacterSelect];
        [SVProgressHUD dismiss];
    }
}

#pragma mark - User Character Response Actions
////////////////////////////////////////////////////////////////////////////////

- (void)activeCharacterLoaded:(NSNotification *)notification
{
    DebugLog(@"Retrieved user character: %@", [notification object]);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.terminaLoadImageView removeFromSuperview];
    self.terminaLoadImageView = nil;
    
    [self.view addSubview:self.mainViewController.view];
    self.activeView = self.mainViewController.view;
    [SVProgressHUD dismiss];
}

- (void)activeCharacterLoadFailed:(NSNotification *)notification
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Termina" accessGroup:nil];
    [keychainItem resetKeychainItem];
    
    [self presentLogin];
    self.previousView = nil;
}


#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////



@end
