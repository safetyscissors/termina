//
//  TMViewHandlerController.h
//  Termina
//
//  Created by Jason Lagaac on 30/09/13.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMViewHandlerController : UIViewController

+ (TMViewHandlerController *)sharedInstance;

/** Main view area which is presented to the user once they have logged in */
- (void)presentMainView;

/** Forgot password view */
- (void)presentForgotPassword;

/** Forgot password view back button action */
- (void)presentLoginFromForgotPassword;

/** Create account view */
- (void)presentCreateAccount;

/** Present login view back button action */
- (void)presentLoginFromCreateAccount;

/** Present character select view */
- (void)presentCharacterSelect;

/** Present quest view */
- (void)presentQuest;

/** Present DISCS view */
- (void)presentDISCS;

/** Present fight view */
- (void)presentNewFight:(NSDictionary *)fightData
          playerCheckin:(NSDictionary *)playerCheckin
        opponentCheckin:(NSDictionary *)opponentCheckin
               location:(NSDictionary *)location
              bossFight:(BOOL)bossFight
             questFight:(BOOL)questFight;

/** Present Existing Fight */
- (void)presentExistingFight:(NSDictionary *)fight;

@end
