#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>

@interface TMDISCSSettingsViewController : UIViewController

/** Facebook Connect Action Button */
@property (nonatomic, strong) IBOutlet UIButton *facebookConnectButton;

/** Twitter Connect Action Button */
@property (nonatomic, strong) IBOutlet UIButton *twitterConnectButton;

/** Main Settings View */
@property (nonatomic, strong) IBOutlet UIView *mainSettingsView;

////////////////////////////////////////////////////////////////////////////////
// Inteface actions

// Facebook Connect Button Action
- (IBAction)facebookConnnectButtonPressed:(id)sender;

// Twitter Connect Button Action
- (IBAction)twitterConnnectButtonPressed:(id)sender;

// Present Password Change Action
- (IBAction)presentPasswordChange:(id)sender;


@end
