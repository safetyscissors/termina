#import "TMDISCSSettingsViewController.h"
#import "TMDISCSResetPasswordViewController.h"
#import "TMTwitter.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface TMDISCSSettingsViewController ()

/** View to change password */
@property (nonatomic, strong) TMDISCSResetPasswordViewController *passwordResetViewController;

@end

@implementation TMDISCSSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self loadPasswordChangeController];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(noTwitterAccountsAvailable:)
                                                 name:kNoTwitterAccountsAvailable
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(singleTwitterAccountAvailable:)
                                                 name:kSingleTwitterAccountAvailable
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(multipleTwitterAccountsAvailable:)
                                                 name:kMultipleTwitterAccountsAvailable
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(twitterAccountAlreadyLinked:)
                                                 name:kTwitterAccountAlreadyLinked
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(presentMainSettingsView:)
                                                 name:@"backToDefaultSettingsView"
                                               object:nil];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadPasswordChangeController
{
    NSString *nibName = IS_IPHONE_5 ? @"TMDISCSResetPasswordViewController-568h" : @"TMDISCSResetPasswordViewController";
    self.passwordResetViewController = [[TMDISCSResetPasswordViewController alloc] initWithNibName:nibName
                                                                                            bundle:nil];
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

// Facebook Connect Button Action
- (IBAction)facebookConnnectButtonPressed:(id)sender
{
    // FBSample logic
    // if the session is open, then load the data for our view controller
    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          
                                          
                                      }];
        return;
    }
}

// Twitter Connect Button Action
- (IBAction)twitterConnnectButtonPressed:(id)sender
{
    [[TMTwitter sharedInstance] linkTwitterAccount];
    [SVProgressHUD showWithStatus:@"Linking Twitter Account"];
}

#pragma mark - Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)noTwitterAccountsAvailable:(NSNotification *)notification
{
    // Item purchase success alert
    UIAlertView *noTwitterAccountsAlert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts Available"
                                                               message:@"A Twitter account can be added or created via Settings > Twitter"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
    [noTwitterAccountsAlert show];
    [SVProgressHUD dismiss];
}


- (void)singleTwitterAccountAvailable:(NSNotification *)notification
{
    // Item purchase success alert
    [TMTwitter sharedInstance].selectedTwitterAccount = [[[TMTwitter sharedInstance] getTwitterAccounts] objectAtIndex:0];
    
    UIAlertView *singleTwitterAccountAlert = [[UIAlertView alloc] initWithTitle:@"Twitter Account Linked"
                                                                     message:@"Your Twitter account has been successfully linked to Termina"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
    
    [singleTwitterAccountAlert show];
    [SVProgressHUD dismiss];

}

- (void)multipleTwitterAccountsAvailable:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
}

- (void)twitterAccountAlreadyLinked:(NSNotification *)notification
{
    UIAlertView *twitterAccountAlreadyLinked = [[UIAlertView alloc] initWithTitle:@"Twitter Account Already Linked"
                                                                          message:@"A Twitter account has already been linked to Termina"
                                                                         delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
    [twitterAccountAlreadyLinked show];
    [SVProgressHUD dismiss];
}

- (void)presentMainSettingsView:(NSNotification *)notification
{
    self.mainSettingsView.hidden = NO;
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.mainSettingsView.alpha = 1.0f;
                         self.passwordResetViewController.view.alpha = 0.0f;
                     } completion:^(BOOL finished) {
                         [self.passwordResetViewController.view removeFromSuperview];
                     }];
}

- (IBAction)presentPasswordChange:(id)sender
{
    self.passwordResetViewController.view.alpha = 0.0f;
    [self.view insertSubview:self.passwordResetViewController.view
                aboveSubview:self.mainSettingsView];
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.mainSettingsView.alpha = 0.0f;
                         self.passwordResetViewController.view.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                         self.mainSettingsView.hidden = YES;
                     }];
}




@end
