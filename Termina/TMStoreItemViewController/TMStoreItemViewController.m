#import "TMStoreItemViewController.h"
#import "TMUser.h"
#import "TMUserCharacter.h"
#include <math.h>

@interface TMStoreItemViewController ()

/** Item data */
@property (nonatomic, strong) NSDictionary *itemData;

/** Insufficent funds alert message */
@property (nonatomic, strong) UIAlertView *insufficientFundsAlert;

/** Alternative purchase with gems */
@property (nonatomic, strong) UIAlertView *gemAlternativePurchase;

/** Alternative purchase with currency */
@property (nonatomic, strong) UIAlertView *currencyAlternativePurchase;

/** User data */
@property (nonatomic, strong) NSDictionary *userData;

/** Item currency cost */
@property (nonatomic) float itemCurrencyCost;

/** Item gem cost */
@property (nonatomic) int itemGemCost;

@end

@implementation TMStoreItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [self loadPurchaseActionMessages];
    
    // Load user character and user data
    self.userData = [TMUser sharedInstance].currentUserDetails;
    

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Item cost values
    self.itemCurrencyCost = (float)([[self.itemData objectForKey:@"cost"] floatValue] * 100) / 100;
    DebugLog(@"Item Currency Cost: %f", self.itemCurrencyCost);
    self.itemGemCost = (int)self.itemCurrencyCost * 1000;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadPurchaseActionMessages
{
    
    // Item purchase success alert
    self.insufficientFundsAlert = [[UIAlertView alloc] initWithTitle:@"Purchase Unsuccessful"
                                                             message:@"You have insufficent funds to buy this item.\n\
                                                                       Would you like to buy some currency?"
                                                            delegate:self
                                                   cancelButtonTitle:@"No"
                                                   otherButtonTitles:@"Buy", nil];
    
    // Currency alternative purchase
    self.currencyAlternativePurchase = [[UIAlertView alloc] initWithTitle:@"Purchase Unsuccessful"
                                                                  message:@"You have insufficent gems to buy this item.\n\
                                                                            Would you like to use money to buy this item?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"No"
                                                        otherButtonTitles:@"Use Gems", nil];
    
    // Gem alternative purchase
    self.gemAlternativePurchase = [[UIAlertView alloc] initWithTitle:@"Purchase Unsuccessful"
                                                             message:@"You have insufficent funds to buy this item.\n\
                                                                       Would you like to use gems to buy this item?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"No"
                                                        otherButtonTitles:@"Use Gems", nil];
    

}


#pragma mark - Data Set Actions
////////////////////////////////////////////////////////////////////////////////

- (void)setItemPurchased
{
    self.itemImage.alpha = 0.5f;
    self.attackValueLabel.alpha = 0.5f;
    self.healthValueLabel.alpha = 0.5f;
    self.defenseValueLabel.alpha = 0.5f;
    self.movesValueLabel.alpha = 0.5f;
    self.itemName.alpha = 0.5f;
    
    self.gemPurchase.userInteractionEnabled = NO;
    self.realMoneyPurchase.userInteractionEnabled = NO;
    self.gemPurchase.alpha = 0.5f;
    self.realMoneyPurchase.alpha = 0.5f;
    
    self.itemPurchasedBanner.hidden = NO;
}


- (void)setWithData:(NSDictionary *)itemData
{
    self.itemData = itemData;
    
    // Set the item's name
    [self.itemName setText:[[self.itemData objectForKey:@"name"] uppercaseString]];
    
    // Set the currency purchaseable value
    float realCost = [[self.itemData objectForKey:@"cost"] floatValue];
    [self.realMoneyPurchase setTitle:[NSString stringWithFormat:@"%0.2f", realCost]
                            forState:UIControlStateNormal];


    int gemValue = ([[self.itemData objectForKey:@"cost"] floatValue] * 1000);
    NSString *gemCost = [NSString stringWithFormat:@"%d", gemValue];
    [self.gemPurchase setTitle:gemCost
                      forState:UIControlStateNormal];
    
    NSDictionary *itemAttributes = [self.itemData objectForKey:@"attributes"];
    NSString *attack = [NSString stringWithFormat:@"%@ ATTACK", [itemAttributes objectForKey:@"attack_radius"]];
    [self.attackValueLabel setText: attack];
    
    NSString *moves = [NSString stringWithFormat:@"%@ MOVES", [itemAttributes objectForKey:@"attack_points"]];
    [self.movesValueLabel setText:moves];
    
    NSString *defense = [NSString stringWithFormat:@"%@ DEFENSE", [itemAttributes objectForKey:@"defense_radius"]];
    [self.defenseValueLabel setText:defense];
    
    NSString *health = [NSString stringWithFormat:@"%@ HEALTH", [itemAttributes objectForKey:@"health"]];
    [self.healthValueLabel setText:health];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"Button Clicked: %d", buttonIndex);
    
    if (buttonIndex == 1) {
        if (alertView == self.gemAlternativePurchase) {
            [self itemPurchasedWithGems];
        } else if (alertView == self.currencyAlternativePurchase) {
            [self itemPurchasedWithCurrency];
        }
    }
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)purchaseWithCurrency:(id)sender
{
    float gameCurrency = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"in_game_money"] floatValue];
    int userGems = [[self.userData objectForKey:@"money"] integerValue];
    
    if (gameCurrency > self.itemCurrencyCost) {
        [self itemPurchasedWithCurrency];
        [self setItemPurchased];
    } else {
        if (userGems > self.itemGemCost) {
            // Purchase with gems
            [self.gemAlternativePurchase show];
        } else {
            [self.insufficientFundsAlert show];
        }
    }
}

- (IBAction)purchaseWithGems:(id)sender
{
    float gameCurrency = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"in_game_money"] floatValue];
    int userGems = [[self.userData objectForKey:@"money"] integerValue];

    if (userGems > self.itemGemCost) {
        [self itemPurchasedWithGems];
        [self setItemPurchased];
    } else {
        if (gameCurrency > self.itemCurrencyCost) {
            // Purchase with gems
            [self.currencyAlternativePurchase show];
        } else {
            [self.insufficientFundsAlert show];
        }
    }
}

#pragma mark - Item Purchased Actions
////////////////////////////////////////////////////////////////////////////////

- (void)itemPurchasedWithCurrency
{
    float gameCurrency = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"in_game_money"] floatValue];
    NSString *userCharacterID = [[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"];
    gameCurrency -= self.itemCurrencyCost;
    

    // Set the in-game money values
    [[TMUserCharacter sharedInstance].activeCharacter setValue:[NSNumber numberWithFloat:gameCurrency]
                                                        forKey:@"in_game_money"];
    
    // Add items to user character
    [[TMUserCharacter sharedInstance] addItem:self.itemData
                              toUserCharacter:[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"]];
    
    // Update user character details
    [[TMUserCharacter sharedInstance] updateCharacter:userCharacterID
                                           withParams:@{@"in_game_money":[NSNumber numberWithFloat:gameCurrency]}];
}

- (void)itemPurchasedWithGems
{
    int gems = [[self.userData objectForKey:@"money"] floatValue];
    NSString *userID = [[TMUser sharedInstance].currentUserDetails objectForKey:@"_id"];
    gems -= self.itemGemCost;
    
    
    [[TMUserCharacter sharedInstance] addItem:self.itemData
                              toUserCharacter:[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"_id"]];
    [[TMUser sharedInstance] updateUser:userID
                             parameters:@{@"money":[NSNumber numberWithInt:gems]}];
}



@end
