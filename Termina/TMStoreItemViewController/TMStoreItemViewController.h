#import <UIKit/UIKit.h>

@interface TMStoreItemViewController : UIViewController <UIAlertViewDelegate>

////////////////////////////////////////////////////////////////////////////////
// Instance Properties

/** Item name */
@property (nonatomic, strong) IBOutlet UILabel *itemName;

/** Item description */
@property (nonatomic, strong) IBOutlet UILabel *itemDescription;

/** Health value label */
@property (nonatomic, strong) IBOutlet UILabel *healthValueLabel;

/** Attack value label */
@property (nonatomic, strong) IBOutlet UILabel *attackValueLabel;

/** Defense value label */
@property (nonatomic, strong) IBOutlet UILabel *defenseValueLabel;

/** Moves value label */
@property (nonatomic, strong) IBOutlet UILabel *movesValueLabel;

/** Gem purchase button */
@property (nonatomic, strong) IBOutlet UIButton *gemPurchase;

/** Real money purchase button */
@property (nonatomic, strong) IBOutlet UIButton *realMoneyPurchase;

/** Item image */
@property (nonatomic, strong) IBOutlet UIImageView *itemImage;

/** Item Purchased Banner */
@property (nonatomic, strong) IBOutlet UIImageView *itemPurchasedBanner;

////////////////////////////////////////////////////////////////////////////////
// Instance Functions

- (void)setItemPurchased;

- (void)setWithData:(NSDictionary *)itemData;

@end
