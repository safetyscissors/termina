//
//  MainViewController.m
//  Termina
//
//  Created by Jason Lagaac on 7/20/13.
//  Copyright (c) 2013 mostlyserious. All rights reserved.
//

#import "TMMainViewController.h"
#import "TMViewHandlerController.h"
#import "TMCentralViewController.h"
#import "TMFightListViewController.h"
#import "TMFightNotificationsViewController.h"
#import "TMStoreViewController.h"
#import "TMRentViewController.h"
#import "TMLocationViewController.h"
#import "TMLocationListViewController.h"
#import "TMEnergyLevelView.h"
#import "TMUser.h"
#import "TMFight.h"
#import "TMUserCharacter.h"
#import "TMFriendViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>


@interface TMMainViewController ()

/** Instance of the CentralViewController */
@property (nonatomic, strong) TMCentralViewController *centralViewController;

/** Instance of the FightViewController */
@property (nonatomic) TMFightNotificationsViewController *fightNotificationsViewController;

/** Instance of the StoreViewController */
@property (nonatomic) TMStoreViewController *storeViewController;

/** Instance of the RentViewController */
@property (nonatomic) TMRentViewController *rentViewController;

/** Instance of the FightListViewController */
@property (nonatomic) TMFightListViewController *fightListController;

/** Instance of the LocationViewController */
@property (nonatomic) TMLocationViewController *locationViewController;

/** Location list from location map cluster */
@property (nonatomic) TMLocationListViewController *locationListViewController;

/** Friend profile view */
@property (nonatomic) TMFriendViewController *friendViewController;

/** Holds the instance of the activeView */
@property (nonatomic) UIView *activeView;

/** Center point for views */
@property (nonatomic) CGPoint centerPoint;

/** Energy Level Indicator */
@property (nonatomic) TMEnergyLevelView *energyLevel;


@end

@implementation TMMainViewController

+ (TMMainViewController *)sharedInstance {
    
    static TMMainViewController *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        if (IS_IPHONE_5) {
            sharedInstance = [[self alloc] initWithNibName:@"MainView-568h"
                                                    bundle:nil];
        } else {
            sharedInstance = [[self alloc] initWithNibName:@"MainView"
                                                    bundle:nil];
        }
    });
    
    return sharedInstance;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
        
        if (IS_IPHONE_5) {
           self.centralViewController = [[TMCentralViewController alloc] initWithNibName:@"CentralView-568h"
                                                                                  bundle:nil];
            
            self.fightListController = [[TMFightListViewController alloc] initWithNibName:@"FightListViewController-568h"
                                                                                 bundle:nil];

            self.fightNotificationsViewController = [[TMFightNotificationsViewController alloc] initWithNibName:@"FightNotifications-568h"
                                                                                                       bundle:nil];
            self.storeViewController = [[TMStoreViewController alloc] initWithNibName:@"StoreView-568h"
                                                                               bundle:nil];
            
            self.rentViewController = [[TMRentViewController alloc] initWithNibName:@"RentView-568h"
                                                                           bundle:nil];
            
            self.locationViewController = [[TMLocationViewController alloc] initWithNibName:@"LocationView-568h"
                                                                                     bundle:nil];
            
            self.locationListViewController = [[TMLocationListViewController alloc] initWithNibName:@"LocationListView-568h"
                                                                                             bundle:nil];
            
            self.friendViewController = [[TMFriendViewController alloc] initWithNibName:@"FriendView-568h"
                                                                                 bundle:nil];

        } else {
            self.centralViewController = [[TMCentralViewController alloc] initWithNibName:@"CentralView"
                                                                                   bundle:nil];
            
            self.fightListController = [[TMFightListViewController alloc] initWithNibName:@"FightListViewController"
                                                                                 bundle:nil];
            
            self.fightNotificationsViewController = [[TMFightNotificationsViewController alloc] initWithNibName:@"FightNotifications"
                                                                                                       bundle:nil];
            
            self.storeViewController = [[TMStoreViewController alloc] initWithNibName:@"StoreView"
                                                                               bundle:nil];
            
            self.rentViewController = [[TMRentViewController alloc] initWithNibName:@"RentView"
                                                                             bundle:nil];
            
            self.locationViewController = [[TMLocationViewController alloc] initWithNibName:@"LocationView"
                                                                                     bundle:nil];
            
            self.locationListViewController = [[TMLocationListViewController alloc] initWithNibName:@"LocationListView"
                                                                                             bundle:nil];
            
            self.friendViewController = [[TMFriendViewController alloc] initWithNibName:@"FriendView"
                                                                                 bundle:nil];
        }
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    DebugLog(@"Parent View: %@", self.view.superview);
    
    self.centerPoint = self.centralViewController.view.center;
    self.activeView = self.centralViewController.view;
    [self.view insertSubview:self.centralViewController.view belowSubview:self.bottomBar];
    
    CGFloat currentEnergyLevel = [[[TMUserCharacter sharedInstance].activeCharacter objectForKey:@"energy"] floatValue];
    DebugLog(@"Current Energy Level: %f", currentEnergyLevel);
    
    if (IS_IPHONE_5) {
        DebugLog(@"iPhone 5");
        self.energyLevel = [[TMEnergyLevelView alloc]initWithFrame:CGRectMake(85, 455.5, 150.0, 150.0f)];
        [self.view insertSubview:self.energyLevel belowSubview:self.fightListButton];
        [self.energyLevel updateEnergyLevel:(currentEnergyLevel / 100)];
    } else {
        DebugLog(@"iPhone 4");
        self.energyLevel = [[TMEnergyLevelView alloc]initWithFrame:CGRectMake(85, 364.0, 150.0f, 150.0f)];
        [self.view insertSubview:self.energyLevel belowSubview:self.fightListButton];
        [self.energyLevel updateEnergyLevel:(currentEnergyLevel / 100)];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidDisppear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)presentView:(UIView *)presentedView
      fromDirection:(PresentViewDirection)direction
{
    if (self.activeView != presentedView) {
        if (direction == kDirectionLeft) {
            presentedView.center = VIEW_OUTER_LEFT;
            [self.view insertSubview:presentedView
                        aboveSubview:self.activeView];
        } else {
            presentedView.center = VIEW_OUTER_RIGHT;
            [self.view insertSubview:presentedView
                        aboveSubview:self.activeView];
        }
        
        [UIView animateWithDuration:0.2f
                         animations:^{
                             if (direction == kDirectionLeft) {
                                 presentedView.center = VIEW_CENTER;
                                 self.activeView.center = VIEW_OUTER_RIGHT;
                             } else {
                                 presentedView.center = VIEW_CENTER;
                                 self.activeView.center = VIEW_OUTER_LEFT;
                             }
                         } completion:^(BOOL finished) {
                             [self.activeView removeFromSuperview];
                             self.activeView = presentedView;
                             
                         }];
    }
}

- (IBAction)presentQuest:(id)sender
{
    [[TMViewHandlerController sharedInstance] presentQuest];
}


- (IBAction)presentFightList:(id)sender
{
    [self presentView:self.fightListController.view
        fromDirection:kDirectionRight];
    self.fightListButton.userInteractionEnabled = NO;
}

- (IBAction)presentDISCSView:(id)sender
{
    [[TMViewHandlerController sharedInstance] presentDISCS];
}


- (void)presentCentralView
{
    [SVProgressHUD dismiss];
    [self presentView:self.centralViewController.view
        fromDirection:kDirectionLeft];
    self.fightListButton.userInteractionEnabled = YES;
}


- (void)presentFightNotifications
{
    self.fightNotificationsViewController.currentActiveFights = [TMFight sharedInstance].currentActiveFights;
    [self presentView:self.fightNotificationsViewController.view
        fromDirection:kDirectionRight];
}

- (void)presentStore
{
    [self presentView:self.storeViewController.view
        fromDirection:kDirectionRight];
}

- (void)presentStoreFromDISCS
{
    self.storeViewController.presentFromDISCS = YES;
    [self presentView:self.storeViewController.view
        fromDirection:kDirectionRight];
}


- (void)presentRent
{
    [self presentView:self.rentViewController.view
        fromDirection:kDirectionRight];
}

- (void)presentLocationWithData:(NSDictionary *)locationData;
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.locationViewController.locationData = locationData;
    [self presentView:self.locationViewController.view
        fromDirection:kDirectionRight];
}

- (void)presentLocationFromClusterWithData:(NSDictionary *)locationData;
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.locationViewController.locationData = locationData;
    self.locationViewController.fromLocationListView = YES;
    [self presentView:self.locationViewController.view
        fromDirection:kDirectionRight];
}

- (void)presentFightListFromLocation
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self presentView:self.fightListController.view
        fromDirection:kDirectionLeft];
}

- (void)presentLocationList:(NSArray *)locations
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.locationListViewController.locations = [locations copy];
    [self presentView:self.locationListViewController.view
        fromDirection:kDirectionRight];
}


- (void)presentLocationListFromLocation
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self presentView:self.locationListViewController.view
        fromDirection:kDirectionLeft];
}

/** Present Friend View */
- (void)presentFriendProfileWithData:(NSDictionary *)friendData
{
    self.friendViewController.friendData = friendData;
    [self presentView:self.friendViewController.view
        fromDirection:kDirectionRight];
}

@end
