#import <UIKit/UIKit.h>

enum
{
    kDirectionLeft,
    kDirectionRight,
} typedef PresentViewDirection;


@interface TMMainViewController : UIViewController

/** Bottom bar background */
@property (nonatomic, strong) IBOutlet UIImageView *bottomBar;

/** Fight list presentation button */
@property (nonatomic, strong) IBOutlet UIButton *fightListButton;

/** Singleton instance of the MainViewController */
+ (TMMainViewController *)sharedInstance;

/** Present the fight notifications list view */
- (void)presentFightNotifications;

/** Present the central screen view */
- (void)presentCentralView;

/** Present the store view */
- (void)presentStore;

/** Present the store from DISCS */
- (void)presentStoreFromDISCS;

/** Present the rent view */
- (void)presentRent;

/** Present the location view with checkins */
- (void)presentLocationWithData:(NSDictionary *)locationData;

/** Present the location view from cluster */
- (void)presentLocationFromClusterWithData:(NSDictionary *)locationData;

/** Present the fight list back from the location view */
- (void)presentFightListFromLocation;

/** Present a list of locations from a location cluster */
- (void)presentLocationList:(NSArray *)locations;

/** Present the location list back from a location view */
- (void)presentLocationListFromLocation;

/** Present Friend View */
- (void)presentFriendProfileWithData:(NSDictionary *)friendData;

@end
