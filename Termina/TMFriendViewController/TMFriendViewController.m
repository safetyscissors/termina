#import "TMFriendViewController.h"
#import "TMFriendProfileCell.h"
#import "TMFightHelpers.h"
#import "TMFightListOpponentCell.h"
#import "TMMainViewController.h"

@interface TMFriendViewController ()

/** Array of friend characters */
@property (nonatomic, strong) NSArray *friendCharacters;

@end

@implementation TMFriendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.friendCharacters = [[self.friendData objectForKey:@"user_characters"] copy];
    [self.characterTable reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Actions
////////////////////////////////////////////////////////////////////////////////

- (IBAction)backToFriendList:(id)sender
{
    [[TMMainViewController sharedInstance] presentFightListFromLocation];
}

#pragma mark - Table Delegate
////////////////////////////////////////////////////////////////////////////////

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark - Table Datasource
////////////////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 100.0F;
    } else {
        return 86.0f;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.friendCharacters count] + 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {

        static NSString *CellIdentifier = @"FriendProfileCell";
        TMFriendProfileCell *cell = (TMFriendProfileCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FriendProfileCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell = (TMFriendProfileCell *)currentObject;
                    break;
                }
            }
        }
        
        cell.username.text = [[self.friendData objectForKey:@"username"] uppercaseString];
        
        // Calculate number of days played
        NSString *dateString = [self.friendData objectForKey:@"created"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        DebugLog(@"Create Date: %@", dateString);
        
        NSDate *createDate = [dateFormat dateFromString:dateString];
        
        NSString *daysPlayed = @"";
        int daysBetweenResult = daysBetween(createDate, [NSDate date]);
        if (daysBetweenResult == 1) {
            daysPlayed = [NSString stringWithFormat:@"%d DAY PLAYED", 1];
        } else {
            daysPlayed = [NSString stringWithFormat:@"%d DAYS PLAYED", daysBetweenResult];
        }
        
        NSString *totalCharacters = @"";
        
        if ([self.friendCharacters count] == 1) {
            totalCharacters = @"1 CHARACTER";
        } else {
            totalCharacters = [NSString stringWithFormat:@"%d CHARACTERS", [self.friendCharacters count]];
        }
        
        cell.userStats.text = [NSString stringWithFormat:@"%@ • %@", totalCharacters, daysPlayed];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        return cell;
        
    } else {
        static NSString *CellIdentifier = @"CharacterCell";
        TMFightListOpponentCell *cell = (TMFightListOpponentCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FightListOpponentCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell = (TMFightListOpponentCell *)currentObject;
                    break;
                }
            }
        }
        
        cell.name.text = [[self.friendCharacters objectAtIndex:indexPath.row - 1] objectForKey:@"name"];
        NSInteger level = [[[self.friendCharacters objectAtIndex:indexPath.row - 1] objectForKey:@"level"] integerValue];
        cell.level.text = [NSString stringWithFormat:@"Level %d", level];
        cell.name.textColor = [UIColor whiteColor];
        cell.level.textColor = [UIColor whiteColor];
        cell.location.hidden = YES;
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
};

@end
