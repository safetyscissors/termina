//
//  TMFriendViewController.h
//  Termina
//
//  Created by Jason Lagaac on 19/12/2013.
//  Copyright (c) 2013 MostlySerious. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMFriendViewController : UIViewController <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>

/** Location Data */
@property (nonatomic, strong) NSDictionary *friendData;

/** Active checkins table */
@property (nonatomic, strong) IBOutlet UITableView *characterTable;

/** Current location name */
@property (nonatomic, strong) IBOutlet UILabel *friendName;

/** Background image */
@property (nonatomic, strong) IBOutlet UIImageView *backgroundImage;

/** Back to fight list action */
- (IBAction)backToFriendList:(id)sender;

@end
