#import <Foundation/Foundation.h>

NSMutableURLRequest * customRequest(NSString *url, NSString *action, NSDictionary *params, NSDictionary *headers);

NSMutableURLRequest * customJSONRequest(NSString *url, NSString *action, NSDictionary *params, NSDictionary *headers);

NSMutableURLRequest * customRequestWithQueryString(NSString *url, NSString *action, NSDictionary *params, NSDictionary *headers);