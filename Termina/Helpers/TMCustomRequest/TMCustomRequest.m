#import "TMCustomRequest.h"
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

void addHeadersToRequest(NSMutableURLRequest *request, NSDictionary *headers)
{
    if (headers != nil) {
        for (NSString *key in [headers allKeys]) {
            [request addValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
}

NSMutableURLRequest * customRequest(NSString *url, NSString *action, NSDictionary *params, NSDictionary *headers)
{
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:action
                                                                                 URLString:url
                                                                                parameters:params];
    addHeadersToRequest(request, headers);
    
    return request;
}

NSMutableURLRequest * customJSONRequest(NSString *url, NSString *action, NSDictionary *params, NSDictionary *headers)
{
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:action
                                                                                 URLString:url
                                                                                parameters:params];
    addHeadersToRequest(request, headers);
    
    return request;
}

NSMutableURLRequest * customRequestWithQueryString(NSString *url, NSString *action, NSDictionary *params, NSDictionary *headers)
{
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:action
                                                                                 URLString:url
                                                                                parameters:params];
    addHeadersToRequest(request, headers);

    return request;
}


