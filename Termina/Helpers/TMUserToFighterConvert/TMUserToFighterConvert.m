#include "TMUserToFighterConvert.h"

NSDictionary * userToFighter(NSDictionary *userCharacter, NSString *checkinID)
{
    if (checkinID) {
        return @{ @"_user" : [userCharacter objectForKey:@"_user"],
                  @"_user_character" : [userCharacter objectForKey:@"_id"],
                  @"type" : [userCharacter objectForKey:@"type"],
                  @"name" : [userCharacter objectForKey:@"name"],
                  @"_checkin" : checkinID,
                  @"attributes" : [userCharacter objectForKey:@"attributes"]};
    } else {
        return @{ @"_user" : [userCharacter objectForKey:@"_user"],
                  @"_user_character" : [userCharacter objectForKey:@"_id"],
                  @"type" : [userCharacter objectForKey:@"type"],
                  @"name" : [userCharacter objectForKey:@"name"],
                  @"attributes" : [userCharacter objectForKey:@"attributes"]};
    }
}