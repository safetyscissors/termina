#import <Foundation/Foundation.h>

typedef enum {
    kFightModeWaiting,
    kFightModeAttack,
    kFightModeDefend
} FightMode;

// Scale touch to fight screen
CGPoint scaleTouchToFightScreen(CGPoint point);

// Scale touch to API
CGPoint scaleTouchToAPI(CGPoint point);

// Check if the player is an attacker
bool checkPlayerIsAttacker(NSString *playerID, NSDictionary *fight);

// Convert hash to CGPoint
CGPoint convertToPoint(NSMutableDictionary *point);

// Distance Between Points
double distanceBetweenPoints(CGPoint p1, CGPoint p2);

// Accuracy of defense to attack points
float accuracyBetweenAttackAndDefendPoints(NSDictionary *attack, NSDictionary *defend);

// Health calculation for character
float healthForCharacterFromSequences(NSDictionary *character, NSDictionary *opponent, NSArray *sequences);

// Check if opponent is an NPC
bool checkOpponentIsNPC(NSDictionary *fight);

// Retrieve the opponent data for a fight
NSDictionary *opponentForFight(NSString *playerID, NSDictionary *fight);

// Check the current state
FightMode checkPlayerState(NSString *playerID, NSDictionary *fight, NSArray *fightSequences);

// Get the character's attributes for the fight
NSDictionary *characterAttributesForFight(NSString *playerID, NSDictionary *fight);

// Convert API sequence point to touch image
UIImageView * sequencePointToTouchPoint(NSDictionary *point, FightMode fightMode);

// Calculate days between date
int daysBetween(NSDate *dt1, NSDate *dt2);