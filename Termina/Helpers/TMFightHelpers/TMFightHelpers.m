#include "TMFightHelpers.h"

////////////////////////////////////////////////////////////////////////////////
// Scale touches to fight screen or API

CGPoint scaleTouchToFightScreen(CGPoint point)
{
    float pointX = point.x / 2;
    float pointY = point.y / 2;
    
    if (pointX < 0) {
        pointX = 0;
    } else if (pointX >= 320) {
        pointX = 320;
    }
    
    if (pointY < 65.0f) {
        pointY = 65.0f;
    } else if (pointY >= 325) {
        pointY = 325;
    }
    
    return CGPointMake(pointX, pointY);
}

CGPoint scaleTouchToAPI(CGPoint point)
{
    float pointX = point.x;
    float pointY = point.y;
    
    if (pointX < 0) {
        pointX = 0;
    } else if (pointX >= 320) {
        pointX = 320;
    }
    
    if (pointY < 65.0f) {
        pointY = 65.0f;
    } else if (pointY >= 325) {
        pointY = 325;
    }
    
    return CGPointMake(pointX * 2, pointY * 2);
}


////////////////////////////////////////////////////////////////////////////////
// Check if the player is an attacker

bool checkPlayerIsAttacker(NSString *playerID, NSDictionary *fight)
{
    NSString *attackerID = [[fight objectForKey:@"attacker"] objectForKey:@"_user_character"];
    return [playerID isEqualToString:attackerID];
}

////////////////////////////////////////////////////////////////////////////////
// Convert hash to CGPoint

CGPoint convertToPoint(NSMutableDictionary *point)
{
    CGFloat pointX = [[point objectForKey:@"x"] floatValue];
    CGFloat pointY = [[point objectForKey:@"y"] floatValue];
    CGPoint convertedPoint = CGPointMake(pointX, pointY);
    
    return convertedPoint;
}

////////////////////////////////////////////////////////////////////////////////
// Distance Between Points

double distanceBetweenPoints(CGPoint p1, CGPoint p2)
{
    return sqrt(pow((p2.x - p1.x), 2.0) + pow((p2.y - p1.y), 2.0));
}

// Accuracy of defense to attack points
float accuracyBetweenAttackAndDefendPoints(NSDictionary *attack, NSDictionary *defend)
{
    float distance = 0;
    int totalPoints = [[attack objectForKey:@"points"] count];
    
    for (int i = 0; i < totalPoints; i++) {
        CGPoint attackPoint = convertToPoint([[attack objectForKey:@"points"] objectAtIndex:i]);
        CGPoint defendPoint = convertToPoint([[defend objectForKey:@"points"] objectAtIndex:i]);
        
        distance += distanceBetweenPoints(attackPoint, defendPoint);
    }
    
    return fabs(distance) / totalPoints;
}

////////////////////////////////////////////////////////////////////////////////
// Health calculation for character

float healthForCharacterFromSequences(NSDictionary *character, NSDictionary *opponent, NSArray *sequences)
{
    NSString *userCharacterID = [character objectForKey:@"_user_character"];
    NSDictionary *characterAttributes = [character objectForKey:@"attributes"];
    
    float characterHealth = [[characterAttributes objectForKey:@"health"] floatValue];
    float characterDefense = [[characterAttributes objectForKey:@"defense_radius"] floatValue];
    
    NSDictionary *opponentAttributes = [opponent objectForKey:@"attributes"];
    float opponentStrength = [[opponentAttributes objectForKey:@"strength"] floatValue];
    
    float blockedAverage = 0.0f;
    
    for (int i = 2; i < [sequences count] + 1; i += 2) {
        NSDictionary *attackSequence = [sequences objectAtIndex:i - 2];
        NSDictionary *defendSequence = [sequences objectAtIndex:i - 1];
        
        if ([[defendSequence objectForKey:@"_user_character"] isEqualToString:userCharacterID]) {
            // Determine the average distance between user character blocks
            blockedAverage = accuracyBetweenAttackAndDefendPoints(attackSequence, defendSequence);
            NSLog(@"Character Blocked Average %@ %f", userCharacterID, blockedAverage);

            characterHealth -=  (characterDefense - (characterDefense * ((100 - blockedAverage) / 100))) / characterDefense * opponentStrength;
            
            NSLog(@"Character Health %@ %f", userCharacterID, characterHealth);
        }
    }
    
    return characterHealth;
}

////////////////////////////////////////////////////////////////////////////////
// Check opponent is an NPC

bool checkOpponentIsNPC(NSDictionary *fight)
{
    NSString *attacker = [[fight objectForKey:@"attacker"] objectForKey:@"type"];
    NSString *defender = [[fight objectForKey:@"defender"] objectForKey:@"type"];
    
    return ([attacker isEqualToString:@"npc"] || [defender isEqualToString:@"npc"]);
}

////////////////////////////////////////////////////////////////////////////////
// Retrieve the opponent data for a fight

NSDictionary *opponentForFight(NSString *playerID, NSDictionary *fight)
{
    NSDictionary *attacker = [fight objectForKey:@"attacker"];
    NSDictionary *defender = [fight objectForKey:@"defender"];
    
    if ([[attacker objectForKey:@"_user_character"] isEqualToString:playerID]) {
        return defender;
    }
    
    return attacker;
}

////////////////////////////////////////////////////////////////////////////////
// Check the current state

FightMode checkPlayerState(NSString *playerID, NSDictionary *fight, NSArray *fightSequences)
{
    if (![fightSequences count] && checkPlayerIsAttacker(playerID, fight)) {
        return kFightModeAttack;
    } else if (![fightSequences count] && !checkPlayerIsAttacker(playerID, fight)) {
        return kFightModeWaiting;
    } else if ([fightSequences count]) {
        NSDictionary *lastItem = [fightSequences lastObject];
        NSString *lastItemType = [lastItem objectForKey:@"type"];
        NSString *sequenceCharacterID  = [lastItem objectForKey:@"_user_character"];
        
        if ([lastItemType isEqual:@"attack"] && [sequenceCharacterID isEqual:playerID]) {
            return kFightModeWaiting;
        } else if ([lastItemType isEqual:@"defend"] && ![sequenceCharacterID isEqual:playerID]) {
            return kFightModeWaiting;
        } else if ([lastItemType isEqual:@"attack"] && ![sequenceCharacterID isEqual:playerID]) {
            return kFightModeDefend;
        } else if ([lastItemType isEqual:@"defend"] && [sequenceCharacterID isEqual:playerID]) {
            return kFightModeAttack;
        }
    }
    
    
    return kFightModeWaiting;
}

////////////////////////////////////////////////////////////////////////////////
// Retrieve the character attributes for the fight

NSDictionary *characterAttributesForFight(NSString *playerID, NSDictionary *fight) {
    if (checkPlayerIsAttacker(playerID, fight)) {
        return [[fight objectForKey:@"attacker"] objectForKey:@"attributes"];
    }
    
    return [[fight objectForKey:@"defender"] objectForKey:@"attributes"];
}

////////////////////////////////////////////////////////////////////////////////
// API attack sequence to touch point

UIImageView * sequencePointToTouchPoint(NSDictionary *point, FightMode fightMode) {
    
    UIImage *pointImage = nil;
    
    if (fightMode == kFightModeAttack) {
        pointImage = [UIImage imageNamed:@"AttackPoint.png"];
    } else if (fightMode == kFightModeDefend) {
        pointImage = [UIImage imageNamed:@"DefendPoint.png"];
    }
    
    UIImageView *pointImageView = [[UIImageView alloc] initWithImage:pointImage];
    CGPoint touchPoint = convertToPoint([point mutableCopy]);
    pointImageView.center = scaleTouchToFightScreen(touchPoint);
    
    return pointImageView;
}

////////////////////////////////////////////////////////////////////////////////
// Calculate days between dates

int daysBetween(NSDate *dt1, NSDate *dt2) {
    NSUInteger unitFlags = NSDayCalendarUnit;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return [components day]+1;
}

